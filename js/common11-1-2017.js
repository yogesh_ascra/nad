
jQuery(document).ready(function($) {
	$(window).load(function(){
		$('#preloader').fadeOut('slow',function(){$(this).remove();});
	});
});
$(document).ready(function(){
	$('#xmlFile').val("");
	$("#uploadMasterReset").click(function(e){
		$(".field-error").html("");
		$("#uploadMasterDataForm")[0].reset();
		$(".xml_file_value").html("&nbsp;");
		$("#master-data-form").addClass("showHideMasterData");
	});

	$('[data-toggle="popover"]').popover();   
    $("input[type=password]").mobilePassword();
    $('[data-toggle="tooltip"]').tooltip({'trigger':'focus'});
	
	// $('.dp').datepick({dateFormat: 'dd-mm-yyyy'});
    $('.dp').datepicker();
    $('.dp').on('changeDate', function(ev){    
        $(this).datepicker('hide');
        $(".dob").addClass("focused");
	});
	
    
});

function uploadMasterData(){
	var scroll = [];
	$(".field-error").html("");
	var flag = true;
	if($('#xmlFile').val().trim()==""){
		flag = false;
		$("#xmlFile_error").show().css("display","block");
		$("#xmlFile_error").html("Please select .xml file.");
		scroll.push('xmlFile');
	}
	else{
		var extension = $('#xmlFile').val().split('.').pop().toLowerCase();
		if($.inArray(extension, ['xml']) == -1) {
			flag = false;
			$("#xmlFile_error").show().css("display","block");
			$("#xmlFile_error").html("File extension should be .xml."); 
			scroll.push('xmlFile'); 
		}
	}
	scroll_error(scroll);
	if(flag){
		$("#master-data-form").removeClass("showHideMasterData");
	}
}

function sideMenu(){
	var dashboardContentHeight = $(".dashboard-content").height();
	if(dashboardContentHeight > 700)
	{
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	}else{
		$(".dashboard_side_menu").css("height","800px");
	}
}

$(window).load(function(){
	sideMenu();
});

$(document).ready(function() {
	/*vaibhav js*/
	/*10/01/2017*/
	$(document).on("click",".multiselect",function(e){
		$(this).next(".multiselect-container").find("li a label.checkbox").each(function(){
			var a = $(this).text();
			$(this).find("span").remove();
			$(this).contents().filter(function(){ return this.nodeType != 1; }).remove();
			$(this).append("<span>"+a+"</span>");
			$(".multiselect-container").addClass("mCustomScrollbar");
			$(".multiselect-container").mCustomScrollbar();
		});
	});
	
	$(".knowing-nad-owl-carousel").owlCarousel({
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		mouseDrag:false,
		nav: true,
		autoplay:false,
		smartSpeed : 1000,
		dots: false,
		items:1,
		loop: true,
		margin:20,
		rewindNav : false,  
		pagination : false, 
		
	});
	$(".knowing_nad_tab li a").click(function(e){
		if(!$(this).hasClass("active"))
		{
			var for_value = parseInt($(this).attr("for"));
			$(".knowing_nad_tab li a").removeClass("active");
			$(this).addClass("active");
			$(".knd").slideUp(200);
			$(".knowing_nad_tabs"+for_value).slideDown(200);
		}
	});
	
	/*6-1-2017*/
	$(".checkbox").find("input").trigger("click");
	$(".checkbox").find("input").trigger("click");
	$(".manadatory_star_wrap").find(".filter-option").addClass("active");
	
	
	
	/* 22 / 12 / 2016*/
	
	
	var $window = $(window), previousScrollTop = 0, scrollLock = false;
    
    $window.scroll(function(event) {     
        if(scrollLock) {
            $window.scrollTop(previousScrollTop); 
		}
		
        previousScrollTop = $window.scrollTop();
	});
    
	
	
	$(".dashboard-header").click(function(e){
		if($(".nad_main_menu_wrap").css("left") == "0px")
		{
			 var container = $(".nad_main_menu_wrap");

			if (!container.is(e.target) // if the target of the click isn't the container...
				&& container.has(e.target).length === 0) // ... nor a descendant of the container
			{
				scrollLock = !scrollLock; $(".dropdown_menu").slideUp(); 
				$(".nad_main_menu_wrap").css({"left":"-82%"}); 
				$(".menu_icon_ctrl").removeClass("active"); 
				$(".lower-menu ul li.dropdown a").removeClass("active");
				$(".black_overlay").removeClass("running"); $("body").removeClass("noscroll");
			}
			
		}
	});
	
	$(".menu_icon_ctrl").click(function(e){
		scrollLock = !scrollLock;
		$("body").toggleClass("noscroll");
		$(this).toggleClass("active");
		$(".black_overlay").toggleClass("running");
		if($(this).hasClass("active"))
		{
			$(".nad_main_menu_wrap").css({"left":"0%"});
		}
		else
		{
			$(".nad_main_menu_wrap").css({"left":"-82%"});
		}
	});
	
	$(".lower-menu ul li.dropdown ul.dropdown_menu li a, .black_overlay, .lower-menu ul li a.no_dropdown ").click(function(e){  scrollLock = !scrollLock; $(".dropdown_menu").slideUp(); $(".nad_main_menu_wrap").css({"left":"-82%"}); $(".menu_icon_ctrl").removeClass("active"); $(".lower-menu ul li.dropdown a").removeClass("active");
	$(".black_overlay").removeClass("running"); $("body").removeClass("noscroll"); });
	
	$(".lower-menu ul li.dropdown a").click(function(e){
		if($(window).width() < 768)
		{
			$(this).toggleClass("active");
			if($(this).hasClass("active"))
			{
			    $(".dropdown_menu").slideUp();
				$(this).next(".dropdown_menu").slideDown();	
			}
			else
			{
				
				$(this).next(".dropdown_menu").slideUp();		
			}
		}
	});
	
	
	$(".lower-menu ul li.dropdown").hover(function(e){
		//$(this).find(".dropdown_menu").stop(true,true).fadeIn(400);
		},function(e){
		//$(this).find(".dropdown_menu").stop(true,true).hide(0);
	});
	
	
	/*21/12/2016*/
	$(".header-search").on("click",function(e){
		e.preventDefault();
		$(this).next(".searchBox").toggle(".searchBox");
	});
	$('body').on('click', function (e) {
		
		var container = $(this).find(".search_link");
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			$(".searchBox").hide(400);
		}
	});
	
	/*19/12/2016*/
	$(".xmlLabel input").change(function(e){
		$(".xml_file_value").html($(this).val());
	});
	/*16/12/2016*/
	$('#yesUpdate, #noUpdate').on('hidden.bs.modal', function (e) {
	   $("body").removeClass("modal-open");
	   $("body").css("paddingRight","0px");
	   $(this).css("paddingRight","0px");
	})
	var flag_pop = 1;
	$('#myModal').on('hidden.bs.modal', function (e) {
		if(flag_pop == 1)
		{
		$("body").addClass("modal-open");
		if($(window).width() > 768)
		{
			$(this).css("paddingRight","17px");
			$("body").css("paddingRight","17px");
		}
		else
		{
			$(this).css("paddingRight","0px");
			$("body").css("paddingRight","0px");
		}
	   
	   }
	  
	});
	$('#myModal').on('click', function (e) {

		var container = $(this).find(".modal-content");
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			flag_pop = 0;
			setTimeout(function(){ 
				$("body").css("paddingRight","0px");
		   $("body").removeClass("modal-open");
		   $(this).css("paddingRight","0px"); }, 2000);
			
		}
	});

	$('#myModal, #yesUpdate, #noUpdate').on('shown.bs.modal', function (e) {
		
	   $("body").addClass("modal-open");
	   if($(window).width() > 768)
	   {
		  $("body").css("paddingRight","17px");
		  $(this).css("paddingRight","17px");
	   }
	   else
	   {
		   $(this).css("paddingRight","0px");
		   $("body").css("paddingRight","0px");
	   }
	})
	
	$(".masterDataUpdateLink-main").on("click",function(e){
		$('#yesUpdate, #noUpdate').modal('hide');
	});
	$(".masterDataUpdateLink-yes").on("click",function(e){
		flag_pop = 1;
		$('#myModal, #noUpdate').modal('hide');
		$(this).hasClass("active");
	});
	$(".masterDataUpdateLink-no").on("click",function(e){
		flag_pop = 1;
		$('#myModal, #yesUpdate').modal('hide');
		$(this).hasClass("active");
		$("body").css("overflow-y","scroll");
	});
	
	/*14/13/216*/
	/* var dashboardContentHeight = $(".dashboard-content").innerHeight();
		var dashboardMenuHeight = $("ul.dashboard_menu").innerHeight();
		if(dashboardContentHeight > dashboardMenuHeight)
		{
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 150);
		
		}else{
		$(".dashboard_side_menu").css("height",dashboardMenuHeight + 150);
	} */
	
	
	
	
	/*12/13/2016*/
	$(".student_ai_link").hover(function(e){
		if($(window).width() > 992){ $(this).find(".ai_desc").fadeIn();}
		},function(){
		if($(window).width() > 992){ $(this).find(".ai_desc").fadeOut();}
	});
	$(".student_ai_link").click(function(e){ 
		if($(window).width() < 993){ $(this).find(".ai_desc").fadeToggle(); }
	});
	
	$("body").click(function(e){
		var container = $(".student_ai_link");
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			$(".ai_desc").fadeOut(0);
		}
	});
	
	$(".ad_hover").hover(function(e){
		$(this).find(".ms_default").fadeOut(100);
		$(this).find(".ms_active ").fadeIn(100);
		},function(e){
		$(this).find(".ms_default").fadeIn(100);
		$(this).find(".ms_active ").fadeOut(100);
	});
	
	
	$(".student_submit_button1").click(function(e){
		var flag = true;
		//$(".field-error").html("");
		var attr = $(this).attr('for');

		if (typeof attr !== typeof undefined && attr !== false) {
			if(attr=="other_email_id"){
				if($("#other_email_id").val()==""){
					$("#other_email_id_error").show().css("display","block");
					$("#other_email_id_error").html("Email ID information is not filled.");
					flag = false;
					$("#mobile_number").addClass("error-focused");
				}
				else if((!isEmail($("#other_email_id").val()) || $("#other_email_id").val().length<9)){
					$("#other_email_id_error").show().css("display","block");
					$("#other_email_id_error").html("Please check the email ID provided, it looks incorrect.");
					flag = false;
					$("#other_email_id").addClass("error-focused");
				}
				else{
					$("#other_email_id").removeClass("error-focused");
					$("#other_email_id_error").html("");
				}
			}
		}

		if(flag){
			$(this).fadeOut(200);
			$(".student_account_details_edit_mail_btn").removeClass("active").fadeIn(200);
			$(".student_account_details_edit_mobile_btn").removeClass("active");
			$(".students-acoounts-details-point12").addClass("student-edit");
		}
	});
	
	// otp jquery
	$(".student_account_details_submit_btn").click(function(e){

		var flag = true;
		//$(".field-error").html("");
		var attr = $(this).attr('for');

		if (typeof attr !== typeof undefined && attr !== false) {
			if(attr=="mobile_number"){
			    var mobile_regx=/[0-9]/;
				if($("#mobile_number").val().length=="4"){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Mobile number information is not filled.");
					flag = false;
					$("#mobile_number").addClass("error-focused");
				}
				else if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15"){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
					flag = false;
					$("#mobile_number").addClass("error-focused");
				}
				else if($("#mobile_number").val()!="" &&  !mobile_regx.test($("#mobile_number").val())){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Please check the mobile number entered.");
					flag = false;
					$("#mobile_number").addClass("error-focused");
				}
				else{
					$("#mobile_number").removeClass("error-focused");
					$("#mobile_number_error").html("");
				}
			}
		}

		if(flag){
			$(".students_accounts_details_otp").slideDown(400);
			$(".students-acoounts-details-point12, .students-acoounts-details-point11").addClass("student-edit");
			$(this).fadeOut(200);
			$(".otp-box input").focus();
			$(".student_account_details_edit_mobile_btn").removeClass("active").fadeIn(200);
			$(".student_account_details_edit_mail_btn").removeClass("active");

			/* height*/
			$("#master-data-form").removeClass("showHideMasterData");
			var dashboardContentHeight = $(".dashboard-content").height();
			$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
			$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		}
		
	});
	
	$('#reason_for_sharing').on('change', function(){
		var str = $(this).val();
		if (str.search(/other/i) >= 0)
		{
			if($(window).width() < 768)
			{
				$(".rfs_field").slideDown();
			}
			else
			{
				$(".rfs_field").fadeIn();
			}
		}
		else
		{
			if($(window).width() < 768)
			{
				$(".rfs_field").slideUp();
			}
			{
				$(".rfs_field").fadeOut();
			}
		}
	});
	/* end of 12/13/2016*/
	
	// account details
	/*$(".ad_hover").hover(function(e){
		$(this).find(".ms_default").fadeOut(200);
		$(this).find(".ms_active ").fadeIn(200);
		},function(e){
		$(this).find(".ms_default").fadeIn(200);
		$(this).find(".ms_active ").fadeOut(200);
	});*/
	//put cursor at end
	jQuery.fn.putCursorAtEnd = function() {
		
		return this.each(function() {
			
			// Cache references
			var $el = $(this),
			el = this;
			
			// Only focus if input isn't already
			if (!$el.is(":focus")) {
				$el.focus();
			}
			
			// If this function exists... (IE 9+)
			if (el.setSelectionRange) {
				
				// Double the length because Opera is inconsistent about whether a carriage return is one character or two.
				var len = $el.val().length * 2;
				
				// Timeout seems to be required for Blink
				setTimeout(function() {
					el.setSelectionRange(len, len);
				}, 1);
				
				} else {
				
				// As a fallback, replace the contents with itself
				// Doesn't work in Chrome, but Chrome supports setSelectionRange
				$el.val($el.val());
				
			}
			
			// Scroll to the bottom, in case we're in a tall textarea
			// (Necessary for Firefox and Chrome)
			this.scrollTop = 999999;
			
		});
		
	};
	// students accounts details
	$(".student_cancel_button1").click(function(e){
		$(this).fadeOut(200);
		$(this).siblings(".student_submit_button1").fadeOut(200);
		$(".student_account_details_edit_mail_btn").fadeIn(200);
		$(".students-acoounts-details-point12").addClass("student-edit");
	});
	$(".student_cancel_button").click(function(e){
		$(this).fadeOut(200);
		$(".students-acoounts-details-otp-txt").fadeOut(200);
		$(this).siblings(".student_submit_button").fadeOut(200);
		$(".student_account_details_edit_mobile_btn").fadeIn(200);
		$(".students-acoounts-details-point11").addClass("student-edit");
	});
	$(".student_submit_button1").click(function(e){
		$(".student_cancel_button1").fadeOut(200);
	});
	$(".student_submit_button").click(function(e){
		$(".student_cancel_button").fadeOut(200);
	});
	
	$(".student_account_details_edit_mail_btn").click(function(e){
		if($(".students_accounts_details_otp").css("display") == "none")
		{
			$(".students-acoounts-details-otp-txt").fadeOut(200);
		}
		$(this).fadeOut(200);
		$(this).prev(".student_submit_button1").fadeIn(200);
		$(this).next(".student_cancel_button1").fadeIn(200);
		var searchInput = $(".students-acoounts-details-point12 .form-group.info-field > input");
		searchInput.focus();
		searchInput.caret(searchInput.val().length);
		$(".students-acoounts-details-point12").removeClass("student-edit");
		$(this).addClass("active");
		$(this).find(".ms_default").fadeOut(200);
		$(this).find(".ms_active ").fadeIn(200);
		
		$(".students-acoounts-details-point11").addClass("student-edit");
		$(".student_account_details_edit_mobile_btn").fadeIn(200);
		$(".student_account_details_edit_mobile_btn").prev(".student_submit_button").fadeOut(200);
		$(".student_account_details_edit_mobile_btn").next(".student_cancel_button").fadeOut(200);
		
	});
	
	$(".student_account_details_edit_mobile_btn").click(function(e){
		$(".students-acoounts-details-otp-txt").fadeIn(200);
		$(this).fadeOut(200);
		$(".students_accounts_details_otp").slideUp(200);
		$(this).prev(".student_submit_button").fadeIn(200);
		$(this).next(".student_cancel_button").fadeIn(200);
		var searchInput = $(".students-acoounts-details-point11 .form-group.info-field > input");
		searchInput.focus();
		searchInput.caret(searchInput.val().length);
		$(".students-acoounts-details-point11").removeClass("student-edit");
		$(".student_account_details_mobile").attr("id","mobile_number");
		$(this).addClass("active");
		$(".students-acoounts-details-point12").addClass("student-edit");
		$(".student_account_details_edit_mail_btn").fadeIn(200);
		$(".student_account_details_edit_mail_btn").prev(".student_submit_button1").fadeOut(200);
		$(".student_account_details_edit_mail_btn").next(".student_cancel_button1").fadeOut(200);
		
		invokeMobileValidation();
		
		
		
	});
	
	// my certificates
	$(".dashboard_responsive_table  table tr td.university_box_trigger").hover(function(e){
		var a22 = $(".dashboard_responsive_table table tbody tr td:nth-child(1)").innerWidth();
		var a33 = $(".dashboard_responsive_table table tbody tr td:nth-child(2)").width();
		var t44 =  $(".dashboard_responsive_table table tbody tr td:nth-child(2)").innerHeight()/2;
		var t55 =  $(".dashboard_responsive_table table tbody tr td:nth-child(2)").innerHeight();
		var thh =  $(".dashboard_responsive_table table thead tr th").innerHeight();
		var len = $(".dashboard_responsive_table  table tbody tr").length;
		var ind = $(this).parent("tr").index();
		/*if(len == ind+1)
		{	*/
		
		
		$(".university_box").css({"left":a22+a33,"top":20+thh+t44+(t55*(ind))});
		/*}
			else
			{
			$(this).parent("tr").css("position","relative");
			$(".university_box").css({"left":a22+a33,"top":'20px'});
		}*/
		
		$(this).find(".university_box").fadeIn(300);
		}, function(e){
		$(this).find(".university_box").fadeOut(0);
	});
	
	var position_box, position_box_value, a33 , p44;
	
	$("body").click(function(e){
		var container = $(".dashboard_responsive_table  table tr td.university_box_trigger");
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			container.removeClass("active1");
			$(".university_box").removeClass("active").fadeOut(0);
		}
	});
	
	$(".dashboard_responsive_table  table tr td.university_box_trigger").click(function(e){
		if($(this).hasClass("active1"))
		{
			$(this).find(".university_box").removeClass("active").fadeOut(0);
			$(this).removeClass("active1");
		}
		else
		{
			position_box = $(this);
			position_box_value = position_box.position();
			//alert( "left: " + position.left + ", top: " + position.top );
			p44 = $(".dashboard_responsive_table").scrollLeft();
			var a22 = $(".dashboard_responsive_table table tbody tr td:nth-child(1)").innerWidth();
			a33  = $(".dashboard_responsive_table table tbody tr td:nth-child(2)").width();
			var t44 =  $(".dashboard_responsive_table table tbody tr td:nth-child(2)").innerHeight()/2;
			var t55 =  $(".dashboard_responsive_table table tbody tr td:nth-child(2)").innerHeight();
			var thh =  $(".dashboard_responsive_table table thead tr th").innerHeight();
			var ind = $(this).parent("tr").index();
			$(".university_box").css({"left":position_box_value.left+a33,"top":10+thh+t44+(t55*(ind))});
			$(".dashboard_responsive_table  table tr td.university_box_trigger").removeClass("active1");
			if($(".university_box").hasClass("active"))
			{
				$(".university_box").fadeOut(0);
				$(".university_box").removeClass("active");
			}
			$(this).addClass("active1");
			$(this).find(".university_box").addClass("active").fadeIn(200);
		}
		
	});
	$(".dashboard_responsive_table").scroll(function() {
		var p33 = $(this).scrollLeft();
	    $(".university_box").css({"left":position_box_value.left+a33+p44-p33});
	});
	
	
	
	$(".steptabOne").css("display","block");
	if($(window).width() < 768)
	{
		$(".dashboard_side_menu .link_title").css("display","none");
	}
	
	
	
	function invokeMobileValidation()
	{
		var mobile_regx=/[0-9]/;
		if($("#mobile_number").val().length=="4" && selected=="Indian"){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Mobile number information is not filled.");
			flag = false;
		}
		if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15" && selected=="Indian"){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			flag = false;
		}
		if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15"){
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			student_reg.push('mobile_number');
			flag = false;
		}
		if(!mobile_regx.test($("#mobile_number").val())){
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number entered.");
			student_reg.push('mobile_number');
			flag = false;
		}
		$("#mobile_number").keydown(function(e) {
			var selected = $(".nationality:checked").val();
			if(selected=="Foreigner"){
			}
			else{
				var oldvalue=$(this).val();
				var field=this;
				/*setTimeout(function () {
					if(field.value.indexOf('+91 ') !== 0) {
						$(field).val(oldvalue);
					} 
				}, 1);*/
				if(field.value.indexOf('+91 ') !== 0) {
					if(oldvalue=="+91")
						oldvalue="+91 ";
					$("#mobile_number").val(oldvalue);
				}
			}
		});
		$("#mobile_number").keyup(function(e) {
			
			var get_focus =f1(document.getElementById('mobile_number'));
			var oldvalue = $(this).val().replace(/\s/g, '') ;
			var len = $(this).val().length;
			
			var selected = $(".nationality:checked").val();
			if(selected=="Foreigner"){
				/*if(get_focus==11 && e.keyCode=="8"){
					var str1 = oldvalue.substring(0, 5);
					var str2 = oldvalue.substring(5, 9)+oldvalue.substring(10,11);
					var str3 = oldvalue.substring(11);
					}
					else*/ if(get_focus==5 && e.keyCode=="8"){
					get_focus--;
					var str1 = oldvalue.substring(0, 4)+oldvalue.substring(5,6);
					var str2 = oldvalue.substring(6, 11);
					var str3 = oldvalue.substring(11);
				}
				else{
					var str1 = oldvalue.substring(0, 5);
					var str2 = oldvalue.substring(5, 10);
					var str3 = oldvalue.substring(10);
				}
				/*if(str3!=""){
					if(get_focus!=12)
					get_focus++;
					$(this).val(str1+" "+str2+" "+str3);
					}
					else*/ if(str2!=""){
					/*get_focus++;
						if(get_focus==5){
						get_focus++;
						get_focus++;
					}*/
					$(this).val(str1+" "+str2);
				}
				if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
					setCaretPosition('mobile_number', get_focus)
				}
			}
			else{     
				if(get_focus==9 && e.keyCode=="8"){
					get_focus--;
					var str1 = oldvalue.substring(0, 3);
					var str2 = oldvalue.substring(3, 7)+oldvalue.substring(8,9);
					var str3 = oldvalue.substring(9);
				}
				else{
					var str1 = oldvalue.substring(0, 3);
					var str2 = oldvalue.substring(3, 8);
					var str3 = oldvalue.substring(8);
				}
				
				if(str3!=""){
					$(this).val(str1+" "+str2+" "+str3);
				}
				else if(str2!=""){
					$(this).val(str1+" "+str2);
				}
				if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
					setCaretPosition('mobile_number', get_focus)
				}
			}
		});
	}
	
	
	// dashboard menu 24/12/2016
	$(".dashboard_hamburger").click(function(e){
		if($("body").hasClass("big-dash-m"))
		{
			$("body").removeClass("big-dash-m").addClass("small-dash-m");
			if($(window).width() > 767){ $(".dashboard_side_menu .link_title").fadeOut(0);}
			else { var a = $(".dashboard_side_menu").innerWidth(); $(".dashboard-content").fadeOut(200); $(".dashboard-content").css("position","absolute"); $(".dashboard_side_menu .link_title, .dashboard_btn_close").fadeIn(200); $(".black_overlay1").addClass("running");} 
		}
		else{
			
			$("body").removeClass("small-dash-m").addClass("big-dash-m");
			if($(window).width() > 767){ $(".dashboard_side_menu .link_title").fadeIn(400);}
			else {$(".dashboard-content").fadeIn(200);  $(".dashboard-content").css("position","static");   $(".dashboard_side_menu .link_title, .dashboard_btn_close").fadeOut(0);  $(".black_overlay1").removeClass("running");} 
		}
	});  
	 
	
	//resize
	$(window).resize(function(e){
		if($(window).width() > 767)
		{
			$(".dashboard-content").css("paddingTop","40px")
		}
		else
		{
			$(".dashboard-content").css("paddingTop","30px");
			$(".student-landing-banner-menu").css("position","relative");
		}
		dashboard_paddTop = parseInt($(".dashboard-content").css("paddingTop"),10);
	});
	
	
	
	
	
	//nano scroller
	/* $(".nano").nanoScroller({ sliderMaxHeight: 200 }); */
	
	$(".dashboard_menu li a.dashboard-dropdown").on("click",function(e){
		if(!($(this).hasClass("turn")))
		{
			$(".dashboard_menu li ul.dashboard-dropdown-menu").slideUp();
			$(".dashboard_menu li a.dashboard-dropdown").removeClass("turn");
		}
		$(this).toggleClass("turn");
		$(this).next(".dashboard_menu li ul.dashboard-dropdown-menu").slideToggle(200);
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").removeClass("active");
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").closest("li").removeClass("super_active");
		$(".dashboard_menu li ul.dashboard-dropdown-menu li:first-child a").addClass("active");
		$(".dashboard_menu li ul.dashboard-dropdown-menu li:first-child a").closest("li").addClass("super_active");
	});
	$(".dashboard_menu li a.no-dashboard-dropdown").click(function(e){
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").removeClass("active");
		$(this).addClass("active");
		$(".dashboard_menu li ul.dashboard-dropdown-menu").slideUp(200);
		$(".dashboard_menu li a.dashboard-dropdown").removeClass("turn");
		if($(window).width() < 768)
		{
			$("body").removeClass("small-dash-m").addClass("big-dash-m");
			$(".dashboard_side_menu .link_title").fadeOut(0);
			$(".dashboard-content").fadeIn(200);  $(".dashboard-content").css("position","static"); 
		}
		$(".black_overlay, .black_overlay1 ").removeClass("running");
	});

	
	
	$(".dashboard_menu li ul.dashboard-dropdown-menu li a").click(function(e){
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").removeClass("active");
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").closest("li").removeClass("super_active");
		$(this).addClass("active");
		$(this).closest("li").addClass("super_active");
		if($(window).width() < 768)
		{
			$("body").removeClass("small-dash-m").addClass("big-dash-m");
			$(".dashboard_side_menu .link_title").fadeOut(0);
			$(".dashboard-content").fadeIn(200);  $(".dashboard-content").css("position","static"); 
		}
		$(".black_overlay, .black_overlay1").removeClass("running");
	});
	/*24/12/2016*/
	$(".dashboard_btn_close, .black_overlay1, .dashboard-navbar, .dashboard_wrapper, .dashboard-footer, .student-landing-banner-menu-wrap ").click(function(e){
		if($(window).width() < 768)
		{
			$(".dashboard-content").css("position","static"); 
			$(".dashboard-content").fadeIn(200);
			$("body").removeClass("small-dash-m").addClass("big-dash-m");
			$(".black_overlay1").removeClass("running");
			$(".dashboard_side_menu .link_title").fadeOut(0);
		}
	});
	
	
	
	function  dashboardMenuTopValue()
	{
		var dashboard_navbar = parseInt($(".dashboard-navbar").innerHeight(),10);;
		var dashboard_wrapper = parseInt($(".dashboard_wrapper").innerHeight(),10);
		var dashboard_banner_menu = parseInt($(".student-landing-banner-menu").innerHeight(),10);
		var dashboard_menu_top_value = dashboard_navbar + dashboard_wrapper + dashboard_banner_menu;
		return dashboard_menu_top_value;
	}
	function dashboarBannerHieght()
	{
		var dashboard_banner_menu = parseInt($(".student-landing-banner-menu").innerHeight(),10);
		return dashboard_banner_menu;
	}
	function studentImageTop()
	{
		var a = dashboardMenuTopValue() -  parseInt($(".student-landing-banner-menu").innerHeight(),10);;
		return a;
	}
	function mobilestudentImageTop()
	{
		var a = dashboardMenuTopValue();
		return a;
	}
	
	function scrollFixed(a)
	{
		$('html, body').stop().animate({
			scrollTop: $(a).offset().top+2
		},500);
	}
	
	/*end vaibhav js*/
});




function dateDisplay() {
	$('input[name="datefilter"]').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true
	}, 
	function(start, end, label) {
		var years = moment().diff(start, 'years');
		/* alert("You are " + years + " years old."); */
	});
};

function dateMasterDisplay(inputName) {
	$("#"+inputName).daterangepicker({
		singleDatePicker: true,
		showDropdowns: true
	}, 
	function(start, end, label) {
		var years = moment().diff(start, 'years');
		/* alert("You are " + years + " years old."); */
	});
};


$(document).ready(function(){
	$(".share_certificate_selection").click(function(e){
		$(".field-error").hide();
		var flag = true;
		var scroll = [];

		var check_flag = false;
		var values = $('input:checkbox:checked.certificate_checkbox').map(function () {
		  check_flag = true;
		}).get();
		if(!check_flag){
			flag = false;
			scroll.push('certificate_checkbox_table');
			$("#certificate_checkbox_error").html("Please select atleast one certificate.").show();
		}

		if($("#type_of_verifier").val()==""){
			scroll.push('type_of_verifier');
			$("#type_of_verifier_error").html("Type of verifier information is not filled.").show();
			flag = false;
		}

		if($("#verifier_name").val()==""){
			scroll.push('type_of_verifier');
			$("#verifier_name_error").html("Verifier name information is not filled.").show();
			flag = false;
		}

		if($("#reason_for_sharing").val()==""){
			scroll.push('reason_for_sharing');
			$("#reason_for_sharing_error").html("Reason for sharing information is not filled.").show();
			flag = false;
		}

		if($("#reason").val()=="" && $("#reason_for_sharing").val()=="Other Reasons"){
			scroll.push('reason');
			$("#reason_error").html("Reason for sharing information is not filled.").show();
			flag = false;
		}

		if($("#days").val()==""){
			scroll.push('days');
			$("#days_error").html("Period for allowing access (In Days) information is not filled.").show();
			flag = false;
		}
		scroll_error(scroll);
		return flag;
	});


	$(".auth_transaction_order").click(function(e){
		$(".field-error").hide();
		var flag = true;
		var scroll = [];
		if($("#address1").val()==""){
			scroll.push('address1');
			$("#address1_error").show();
			$("#address1_error").html("Address 1 information is not filled.");
			flag = false;
		}
		if($("#address2").val()==""){
			scroll.push('address2');
			$("#address2_error").show();
			$("#address2_error").html("Address 2 information is not filled.");
			flag = false;
		}
		if($("#city").val()==""){
			scroll.push('city');
			$("#city_error").show();
			$("#city_error").html("City /Town /Village information is not filled.");
			flag = false;
		}
		if($("#state").val()==""){
			scroll.push('state');
			$("#state_error").show();
			$("#state_error").html("State information is not filled.");
			flag = false;
		}
		if($("#auth_pincode").val()==""){
			scroll.push('auth_pincode');
			$("#pincode_error").show();
			$("#pincode_error").html("Pincode information is not filled.");
			flag = false;
		}
		else if($("#auth_pincode").val().length!=6){
			scroll.push('auth_pincode');
			$("#pincode_error").show();
			$("#pincode_error").html("Please enter valid pincode.");
			flag = false;
		}
		if($("#cont").val()==""){
			scroll.push('cont');
			$("#cont_error").show();
			$("#cont_error").html("Country information is not filled.");
			flag = false;
		}
		scroll_error(scroll);
		if(!flag){
			$("#master-data-form").removeClass("showHideMasterData");
			var dashboardContentHeight = $(".dashboard-content").height();
			$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
			$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		}
		return flag;
	});

	$(".auth-selection-certficate").click(function(e){
		$(".field-error").hide();
		var flag = false;
		var scroll = [];
		var values = $('input:checkbox:checked.certificate_checkbox').map(function () {
		  flag = true;
		}).get();
		if(!flag){
			$("#certificate_checkbox_error").html("Please select atleast one certificate.").show();
			scroll.push('certificate_checkbox_table');
		}
		scroll_error(scroll);
		return flag;
	});

	$(".DOB").focus(function(){
		dateDisplay();
		$(this).addClass("focusedField");
	}).blur(function(){
		$(".DOB").removeClass("focusedField");
	});
	
	$("#to_date").focus(function(){
		dateMasterDisplay('to_date');
	});
	$("#from_date").focus(function(){
		dateMasterDisplay('from_date');
	});
	
	$(".date_img").click(function(event){
		dateMasterDisplay($(this).attr("date_id"));
		$('#'+$(this).attr("date_id")).data('daterangepicker').toggle();
		$(".DOB").removeClass("focusedField");
		$(this).siblings(".DOB").addClass("focusedField");
	});
	
	
	/*$("#forgotUserID").change(function(){
		$("#forgot_user_id").val("");
		if($(this).val()=="addharId"){
		$("#forgot_user_id").attr("onkeypress","return IsNumeric(event);");
		$("#forgot_user_id").attr("text","tel");
		$("#forgot_user_id").removeClass("text-capitalize");
		$("#forgot_user_id").focus();
		}
		else{
		$("#forgot_user_id").attr("onkeypress","return nadIDValidation(event);");
		$("#forgot_user_id").attr("text","text");
		$("#forgot_user_id").addClass("text-capitalize");
		$("#forgot_user_id").focus();
		}
		});
	$("#forgotUserID").change();*/
	
	var selected = $(".nationality:checked").val();
	if(selected=="Foreigner"){
		//$('#country').attr('readonly', false);
		var state_html = '<label class="control-label">State </label>';
		state_html+='<input id="state" maxlength="30" type="text" class="text-capitalize form-control maxlenghtValidation" data-toggle="tooltip" data-placement="bottom" title="Enter State.">';
		state_html+='<span id="state_error" class="field-error">Error goes here!</span>';
		$("#stateDiv").html(state_html);
		$("#country").removeClass("non-editable");
		$("#mobile_number").val("");
		$("#mobile_number").attr("maxlength","11");
		$("#pincode").removeAttr("maxlength");
		$("#pincode").removeAttr("onkeypress");
		$("#city").removeAttr("onkeypress");
		$("#pincode").removeAttr("title");
		$("#pincode").removeAttr("data-original-title");
		//$("#pincode").attr("onkeypress","return isAlphaNumeric(event);");
	}
	else{
		///$('#country').attr('readonly', true);
		var state_html = '<select id="state" class="selectpicker custom-select state-select" >';
		state_html+='<option value="">State <i class="mandatory">*</i></option>';
		state_html+='<option value="Maharashtra">Maharashtra</option>';
		state_html+='<option value="Goa">Goa</option>';
		state_html+='</select>';
		state_html+='<span id="state_error" class="field-error">Error goes here!</span>';
		
		$("#stateDiv").html(state_html);
		$("#country").addClass("non-editable");
		$("#mobile_number").val("+91 ");
		$("#mobile_number").attr("maxlength","15");
		$("#pincode").attr("maxlength","6");
		$("#pincode").attr("title","Enter your City/Town/Village Name.");
		$("#pincode").attr("data-original-title","Enter your City/Town/Village Name.");
		$("#pincode").attr("onkeypress","return IsNumeric(event);");
		$("#city").attr("onkeypress","return isAlphaSpace(event);");
		$('.selectpicker').selectpicker();
	}
	
	$(".nationality").change(function(e){
		$("#pincode").val("");
		if($(this).val()=="Indian"){
			//$('#country').attr('readonly', true).removeClass("form-control");
			var state_html = '<select id="state" class="selectpicker custom-select state-select" >';
			state_html+='<option value="">State <i class="mandatory">*</i></option>';
			state_html+='<option value="Maharashtra">Maharashtra</option>';
			state_html+='<option value="Goa">Goa</option>';
			state_html+='</select>';
			state_html+='<span id="state_error" class="field-error">Error goes here!</span>';
			
			$("#stateDiv").html(state_html);
			$("#country").val("India").focus();
			$("#country").addClass("non-editable");
			$("#mobile_number").val("+91 ").focus();
			$("#mobile_number").attr("maxlength","15");
			$("#pincode").attr("maxlength","6");
			$("#pincode").attr("onkeypress","return IsNumeric(event);");
			$("#city").attr("onkeypress","return isAlphaSpace(event);");
			$('.selectpicker').selectpicker();
			$("#pincode").attr("title","Enter your City/Town/Village Name.");
			$("#pincode").attr("data-original-title","Enter your City/Town/Village Name.");
		}
		else{
			var state_html = '<label class="control-label">State</label>';
			state_html+='<input id="state" maxlength="30" type="text" class="text-capitalize form-control maxlenghtValidation" data-toggle="tooltip" data-placement="bottom" title="Enter State.">';
			state_html+='<span id="state_error" class="field-error">Error goes here!</span>';
			$("#stateDiv").html(state_html);
			
			$("#country").removeClass("non-editable");
			//$('#country').attr('readonly', false);
			$("#country").val("").blur();
			$("#mobile_number").val("").blur();
			$("#mobile_number").attr("maxlength","11");
			//$("#pincode").attr("maxlength","7");
			//$("#pincode").attr("onkeypress","return isAlphaNumeric(event);");
			$("#pincode").removeAttr("maxlength");
			$("#pincode").removeAttr("onkeypress");
			$("#city").removeAttr("onkeypress");
			$("#pincode").removeAttr("title");
			$("#pincode").removeAttr("data-original-title");
		}
	});
	
	$(".stud_details_submit").click(function(e){
		var regx=/[0-9]/;
		$("#otp_error").html("");
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().show();
			$("#otpClone0").addClass("error-focused");
			return false;
		}
		else if($("#otp").val().length!="6" || !regx.test($("#otp").val())){
			$("#otp_error").html("Please enter valid OTP.").fadeIn().show();
			$("#otpClone0").addClass("error-focused");
			return false;
		}
		else{
			$("#otp").val("");
			$("#otpClone0").removeClass("error-focused");
			$(".students_accounts_details_otp").hide();
			$(".students-acoounts-details-otp-txt").hide();
			return true;
		}
	});
	
	$(".subs-submit-btn").click(function(e){
		var	flag = true;
		$(".field-error").hide();
		if($("#subs-email").val()==""){
			$("#subs-email-error").show().css("display","block");
			$("#subs-email-error").html("Email ID information is not filled.");
			flag = false;
		}
		else if(!isEmail($("#subs-email").val()) || $("#subs-email").val().length<9){
			$("#subs-email-error").show().css("display","block");
			$("#subs-email-error").html("Please check the email ID provided, it looks incorrect.");
			flag = false;
		}
		else{
			
		}
		return flag;
	});
	
	$(".validate-btn").click(function(e){
		$(".field-error").hide();
		var flag = true;
		if($("#nad_id").val()==""){
			flag = false;
			$("#nad_id_error").html("NAD ID information is not filled.").show();
		}
		else if($("#nad_id").val().length!="14"){
			flag = false;
			$("#nad_id_error").html("Please enter valid NAD ID.").show();
		}
		else{
			var str = $("#nad_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag = false;
					$("#nad_id_error").html("Please enter valid NAD ID1.").show();
				}
			}
			else{
				flag = false;
				$("#nad_id_error").html("Please enter valid NAD ID2.").show();
			}
		}
		if(flag){
			$("#nad_id").addClass("non-editable");
			$("#id-based-otp").show();
		}
		return false;
	});
	
	$(document).on('keydown','.disbleInput',function(e){
		//$(".disbleInput").keydown(function(){
		return false;
	});
	
	$(".verify-nad-id").click(function(e){
		var regx=/[0-9]/;
		$(".field-error").hide();
		if($("#nad_otp").val()==""){
			$("#nad_otp_error").html("Please enter OTP.").fadeIn().show();
			return false;
		}
		else if($("#nad_otp").val().length!="6" || !regx.test($("#nad_otp").val())){
			$("#nad_otp_error").html("Please enter valid OTP.").fadeIn().show();
			return false;
		}
		return true;
	});
	
	$("#email_id").blur(function(){
		if($("#email_id").val()!=""){ 
			if(isEmail($("#email_id").val())){
				if($("#user_id").val()==""){
					$("#user_id").val($("#email_id").val());
					$("#user_id").parent('div').addClass('focused');
				}
			}
		}
	});

	$(".nad-reg-submit").click(function(e){
		$(".field-error").hide();
		var flag = true;
		var regx = /^[A-Za-z]+$/;
		var mobile_regx=/[0-9]/;
		var scroll = [];
		if($("#first_name").val()==""){
			scroll.push('first_name');
			$("#first_name_error").show();
			$("#first_name_error").html("First Name information is not filled.");
			flag = false;
		}
		else if($("#first_name").val().length<3){
			scroll.push('first_name');
			$("#first_name_error").show();
			$("#first_name_error").html("First Name length should be minimum 3.");
			flag = false;
		}
		else if($("#first_name").val().length>25){
			scroll.push('first_name');
			$("#first_name_error").show();
			$("#first_name_error").html("First Name length should be maximum 25");
			flag = false;
		}
		
		/*if($("#middle_name").val()==""){
			$("#middle_name_error").show();
			$("#middle_name_error").html("Middle Name information is not filled.");
			flag = false;
			}
			else if($("#middle_name").val().length>25){
			$("#middle_name_error").show();
			$("#middle_name_error").html("Middle Name length should be maximum 25");
			flag = false;
		}*/
		
		if($("#last_name").val()==""){
			scroll.push('last_name');
			$("#last_name_error").show();
			$("#last_name_error").html("Last Name information is not filled.");
			flag = false;
		}
		else if($("#last_name").val().length>25){
			scroll.push('last_name');
			$("#last_name_error").show();
			$("#last_name_error").html("Last Name length should be maximum 25");
			flag = false;
		}
		
		if($("#father_name").val()==""){
			scroll.push('father_name');
			$("#father_name_error").show();
			$("#father_name_error").html("Father’s name information is not filled.");
			flag = false;
			}else if(!regx.test($("#father_name").val())){
			$("#father_name_error").show();
			$("#father_name_error").html("Please enter only alphabets.");
			flag = false;
		}
		if($("#dob").val()==""){
			scroll.push('dob');
			$("#dob_error").show();
			$("#dob_error").html("Date of Birth information is not filled.");
			flag = false;
		}
		else{
			var age = getAge($("#dob").val());
			if(parseInt(age)<parseInt(10) || parseInt(age)>parseInt(75)){
				$("#dob_error").show();
				$("#dob_error").html("Please check the date provided. Registration open for age between 10 years and 75 years.");
				flag = false;
				scroll.push('dob');
			}
		}
		if($("#gender").val()==""){
			$("#gender_error").show().css("display","block");
			$("#gender_error").html("Gender information is not filled.");
			flag = false;
			scroll.push('gender');
			
		}
		if($("#address1").val()==""){
			scroll.push('address1');
			$("#address1_error").show();
			$("#address1_error").html("Address 1 information is not filled.");
			flag = false;
		}
		if($("#address2").val()==""){
			scroll.push('address2');
			$("#address2_error").show();
			$("#address2_error").html("Address 2 information is not filled.");
			flag = false;
		}
		if($("#city").val()==""){
			scroll.push('city');
			$("#city_error").show();
			$("#city_error").html("City /Town /Village information is not filled.");
			flag = false;
		}
		if($("#state").val()=="" && $("#country").val()=="India"){
			scroll.push('state');
			$("#state_error").show();
			$("#state_error").html("State information is not filled.");
			flag = false;
		}
		if($("#pincode").val()=="" /*&& $("#country").val()=="India"*/){
			scroll.push('pincode');
			$("#pincode_error").show();
			$("#pincode_error").html("Pincode information is not filled.");
			flag = false;
		}
		else{
			if($("#pincode").val()!="" && $("#country").val()=="India"){
				
				if($("#pincode").val().length!="6"){
					$("#pincode_error").show();
					$("#pincode_error").html("Please enter valid Pincode.");
					scroll.push('pincode');
					flag = false;
				}
			}
		}
		if($("#country").val()==""){
			scroll.push('country');
			$("#country_error").show();
			$("#country_error").html("Country information is not filled.");
			flag = false;
		}
		if($("#browsImg").val()==""){
			scroll.push('browsImg_error');
			$("#browsImg_error").show().css("display","block");
			$("#browsImg_error").html("Please upload photo.");
			flag = false;
		}
		else{
			var image_size = $("#browsImg")[0].files[0].size;
			var image_extension = $('#browsImg').val().split('.').pop().toLowerCase();
			if($.inArray(image_extension, ['jpeg', 'tiff', 'jpg']) == -1) {
				flag = false;
				scroll.push('browsImg_error');
				$("#browsImg_error").show().css("display","block");
				$("#browsImg_error").html("Only TIFF,JPG,JPEG allowed!");  
			}
			else{
				if(image_size>15360){
					validflag = false;
					scroll.push('browsImg_error');
					$("#browsImg_error").show().css("display","block");
					$("#browsImg_error").html('File size should be less than 15kb');  
				}
			}
		}
		if($("#browsSig").val()==""){
			$("#browsSig_error").show();
			$("#browsSig_error").html("Please upload signature.");
			flag = false;
			scroll.push('browsSig');
		}
		else{
			var image_size = $("#browsSig")[0].files[0].size;
			var image_extension = $('#browsSig').val().split('.').pop().toLowerCase();
			if($.inArray(image_extension, ['jpeg', 'tiff', 'jpg']) == -1) {
				flag = false;
				scroll.push('browsSig');
				$("#browsSig_error").show();
				$("#browsSig_error").html("Only TIFF,JPG,JPEG allowed!");  
			}
			else{
				if(image_size>10240){
					validflag = false;
					scroll.push('browsSig');
					$("#browsSig_error").show();
					$("#browsSig_error").html('File size should be less than 10kb');  
				}
			}
		}
		
		var selected = $(".nationality:checked").val();
		if($("#mobile_number").val().length=="4" && selected=="Indian"){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Mobile number information is not filled.");
			flag = false;
		}
		if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15" && selected=="Indian"){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			flag = false;
		}
		if($("#mobile_number").val()!="" &&  !mobile_regx.test($("#mobile_number").val())){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number entered.");
			flag = false;
		}
		if($("#name_of_university").val()==""){
			$("#name_of_university_error").show();
			$("#name_of_university_error").html("Name of University information is not filled.");
			flag = false;
			scroll.push('name_of_university');
		}
		if($("#course").val()==""){
			$("#course_error").show();
			$("#course_error").html("Course information is not filled.");
			flag = false;
			scroll.push('course');
		}
		if($("#year_of_passing").val()==""){
			$("#year_of_passing_error").show();
			$("#year_of_passing_error").html("Year of Passing information is not filled.");
			flag = false;
			scroll.push('year_of_passing');
		}
		if($("#unique_ref_no").val()==""){
			$("#unique_ref_no_error").show();
			$("#unique_ref_no_error").html("Unique Ref No. information is not filled.");
			flag = false;
			scroll.push('unique_ref_no');
		}
		
		if($("#user_id").val()==""){
			$("#user_id_error").show();
			$("#user_id_error").html("User ID information is not filled.");
			flag = false;
			scroll.push('user_id');
		}
		else if($("#user_id").val().length<6 || $("#user_id").val().length>30){
			$("#user_id_error").show();
			$("#user_id_error").html("Please enter valid user ID.");
			flag = false;
			scroll.push('user_id');
		}
		else{
			var regx = /^[A-Za-z0-9]+$/;
			if(!isEmail($("#user_id").val())){
				if(!regx.test($("#user_id").val())){
					$("#user_id_error").show();
					$("#user_id_error").html("User ID should contain only alphanumeric.");
					flag = false;
					scroll.push('user_id');
				}
			}
		}
		
		if($("#user_password").val()==""){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password information is not filled.");
			flag = false;
			scroll.push('user_password');
		}
		else {
			var regx = /^[A-Za-z0-9]+$/;
			if($("#user_password").val().length<8){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be minimum 8.");
				flag = false;
				scroll.push('user_password');
			}
			else if($("#user_password").val().length>20){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be maximum 20");
				flag = false;
				scroll.push('user_password');
			}
			else if(!regx.test($("#user_password").val())){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password should contain only alphanumeric.");
				flag = false;
				scroll.push('user_password');
			}
		}
		
		if($("#confirm_password").val()==""){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Confirm Password information is not filled.");
			flag = false;
			scroll.push('confirm_password');
		}
		
		if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
			if($("#user_password").val()!=$("#confirm_password").val()){
				$("#confirm_password_error").show();
				$("#confirm_password_error").html("Hey the confirm password value does not match.. Enter the same password and yes do remember it");
				flag = false;
				scroll.push('confirm_password');
			}
		}
		
		if($("#captch_code").val()==""){
			$("#captch_code_error").html("Please enter captcha code.").fadeIn().css("display","block");
			scroll.push('captch_code');
			flag = false;
		}
		else if(!regx.test($("#captch_code").val())){
			$("#captch_code_error").html("Invalid captcha code.").fadeIn().css("display","block");
			scroll.push('captch_code');
			flag = false;
		}
		
		if($("#email_id").val()==""){
			$("#email_id_error").show().css("display","block");
			$("#email_id_error").html("Email ID information is not filled.");
			flag = false;
			scroll.push('email_id');
		}
		else if(!isEmail($("#email_id").val()) || $("#email_id").val().length<9){
			$("#email_id_error").show().css("display","block");
			$("#email_id_error").html("Please check the email ID provided, it looks incorrect.");
			flag = false;
			scroll.push('email_id');
		}
		
		if(!document.getElementById('checkboxInput').checked){
			$("#term_error").show().css({"display":"block","clear":"both"});
			$("#term_error").html("You must agree terms and conditions before registration.");
			flag = false;
		}
		scroll_error(scroll);
		return flag;
	});
	
	$(".maxlenghtValidation").keyup(function(e) {
		var max = $(this).attr("maxlength");
		if ($(this).val().length > max) {
			$(this).val($(this).val().substr(0, max));
		}
	});
	
	$(".firstLatterCappital").keyup(function(e) {
		var string = $(this).val();
		$(this).val(string.charAt(0).toUpperCase() + string.slice(1));
	});
	
	$(".mob-verify-btn").click(function(e){
		var mobile_regx=/[0-9]/;
		$(".field-error").hide();
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
			return false;
		}
		else if($("#otp").val().length!="6" || !mobile_regx.test($("#otp").val())){
			$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
			return false;
		}
		else{
			$(".reg-success").show();
			return true;
		}
		
	});
	
	$("#mob-verify-btn-bill").click(function(e){
		var mobile_regx=/[0-9]/;
		$(".field-error").hide();
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
			return false;
		}
		else if($("#otp").val().length!="6" || !mobile_regx.test($("#otp").val())){
			$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
			return false;
		}
		else{
			$(".reg-success").show();
			return false;
		}
		
	});
	
	$(".student-reg-submit-bill").click(function(){
		$(".field-error").hide();
		var flag = true;
		var student_reg = [];
		if($("#bill_mobile_number").val().length=="4"){
			$("#bill_mobile_number_error").show().css("display","block");
			$("#bill_mobile_number_error").html("Mobile number information is not filled.");
			flag = false;
		}
		else if($("#bill_mobile_number").val().length!="4" && $("#bill_mobile_number").val().length!="15"){
			$("#bill_mobile_number_error").show().css("display","block");
			$("#bill_mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			flag = false;
		}
		if($("#email_id").val()==""){
			$("#email_id_error").show().css("display","block");
			$("#email_id_error").html("Email ID information is not filled.");
			flag = false;
		}
		else if(!isEmail($("#email_id").val()) || $("#email_id").val().length<9){
			$("#email_id_error").show().css("display","block");
			$("#email_id_error").html("Please check the email ID provided, it looks incorrect.");
			flag = false;
		}
		if($("#user_id").val()==""){
			$("#user_id_error").show();
			$("#user_id_error").html("User ID information is not filled.");
			flag = false;
			student_reg.push('user_id');
		}
		else if($("#user_id").val().length<6 || $("#user_id").val().length>30){
			$("#user_id_error").show();
			$("#user_id_error").html("Please enter valid user ID.");
			flag = false;
			student_reg.push('user_id');
		}
		else if(!isEmail($("#user_id").val())){
			var regx = /^[A-Za-z0-9]+$/;
			if(!regx.test($("#user_id").val())){
				$("#user_id_error").show();
				$("#user_id_error").html("User ID should contain only alphanumeric.");
				flag = false;
				student_reg.push('user_id');
			}
		}
		if($("#user_password").val()==""){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password information is not filled.");
			student_reg.push('user_password');
			flag = false;
		}
		else {
			var regx = /^[A-Za-z0-9]+$/;
			if($("#user_password").val().length<8){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be minimum 8.");
				flag = false;
				student_reg.push('user_password');
			}
			else if($("#user_password").val().length>20){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be maximum 20");
				flag = false;
				student_reg.push('user_password');
			}
			else if(!regx.test($("#user_password").val())){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password should contain only alphanumeric.");
				flag = false;
				student_reg.push('user_password');
			}
		}
		
		if($("#confirm_password").val()==""){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Confirm Password information is not filled.");
			flag = false;
			student_reg.push('confirm_password');
		}
		
		if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
			if($("#user_password").val()!=$("#confirm_password").val()){
				$("#confirm_password_error").show();
				$("#confirm_password_error").html("Hey the confirm password value does not match.. Enter the same password and yes do remember it");
				flag = false;
				student_reg.push('confirm_password');
			}
		}
		
		if($("#address1").val()==""){
			$("#address1_error").show();
			$("#address1_error").html("Address 1 information is not filled.");
			flag = false;
		}
		if($("#address2").val()==""){
			$("#address2_error").show();
			$("#address2_error").html("Address 2 information is not filled.");
			flag = false;
		}
		if($("#city").val()==""){
			$("#city_error").show();
			$("#city_error").html("City /Town /Village information is not filled.");
			flag = false;
		}
		if($("#state").val()==""){
			$("#state_error").show();
			$("#state_error").html("State information is not filled.");
			flag = false;
		}
		if($("#bill_pincode").val()=="" && $("#country").val()=="India"){
			$("#bill_pincode_error").show();
			$("#bill_pincode_error").html("Pincode information is not filled.");
			flag = false;
		}
		else{
			if($("#bill_pincode").val()!=""){
				if($("#bill_pincode").val().length!="7"){
					$("#bill_pincode_error").show();
					$("#bill_pincode_error").html("Please enter valid Pincode.");
					flag = false;
				}
			}
		}
		if($("#country").val()==""){
			$("#country_error").show();
			$("#country_error").html("Country information is not filled.");
			flag = false;
		}
		
		if($("#browsSig").val()!=""){
			var image_size = $("#browsSig")[0].files[0].size;
			var image_extension = $('#browsSig').val().split('.').pop().toLowerCase();
			if($.inArray(image_extension, ['jpeg', 'tiff', 'jpg']) == -1) {
				flag = false;
				$("#browsSig_error").show();
				$("#browsSig_error").html("Only TIFF,JPG,JPEG allowed!");  
			}
			else{
				if(image_size>10240){
					validflag = false;
					$("#browsSig_error").show();
					$("#browsSig_error").html('file size should be less than 10kb');  
				}
			}
		}
		if(!document.getElementById('checkboxInput').checked){
			$("#term_error").show().css({"display":"block","clear":"both"});
			$("#term_error").html("You must agree terms and conditions before registration.");
			student_reg.push('checkboxInput');
			flag = false;
		}
		scroll_error(student_reg)
		return flag;
	});
	
	$(".student-reg-submit").click(function(e){
		//e.preventDefault(); 		
		var regx = /^[A-Za-z]+$/;
		var mobile_regx = /[0-9]/;
		var flag = true;
		var student_reg = [];
		$("#father_name_error").html("");
		$("#user_id_error").html("");
		$("#user_password_error").html("");
		$("#confirm_password_error").html("");
		$("#term_error").html("");
		$("#other_email_id_error").html("");
		$("#mobile_number_error").html("");
		if($("#father_name").val()==""){
			$("#father_name_error").show();
			$("#father_name_error").html("Father’s name information is not filled.");
			student_reg.push('father_name');
			flag = false;
			}else if(!regx.test($("#father_name").val())){
			$("#father_name_error").show();
			$("#father_name_error").html("Please enter only alphabets.");
			student_reg.push('father_name');
			flag = false;
		}
		
		if($("#user_id").val()==""){
			student_reg.push('user_id');
			$("#user_id_error").show();
			$("#user_id_error").html("User ID information is not filled.");
			flag = false;
		}
		else if($("#user_id").val().length<6 || $("#user_id").val().length>30){
			$("#user_id_error").show();
			$("#user_id_error").html("Please enter valid user ID.");
			student_reg.push('user_id');
			flag = false;
		}
		else if(!isEmail($("#user_id").val())){
			var regx = /^[A-Za-z0-9]+$/;
			if(!regx.test($("#user_id").val())){
				$("#user_id_error").show();
				$("#user_id_error").html("User ID should contain only alphanumeric.");
				student_reg.push('user_id');
				flag = false;
			}
		}
		
		if($("#user_password").val()==""){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password information is not filled.");
			student_reg.push('user_password');
			flag = false;
		}
		else {
			var regx = /^[A-Za-z0-9]+$/;
			if($("#user_password").val().length<8){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be minimum 8.");
				student_reg.push('user_password');
				flag = false;
			}
			else if($("#user_password").val().length>20){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be maximum 20");
				student_reg.push('user_password');
				flag = false;
			}
			else if(!regx.test($("#user_password").val())){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password should contain only alphanumeric.");
				student_reg.push('user_password');
				flag = false;
			}
		}
		
		if($("#confirm_password").val()==""){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Confirm Password information is not filled.");
			student_reg.push('confirm_password');
			flag = false;
		}
		
		if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
			if($("#user_password").val()!=$("#confirm_password").val()){
				$("#confirm_password_error").show();
				$("#confirm_password_error").html("Hey the confirm password value does not match.. Enter the same password and yes do remember it");
				student_reg.push('confirm_password');
				flag = false;
			}
		}
		
		if($("#other_email_id").val()!="" && (!isEmail($("#other_email_id").val()) || $("#other_email_id").val().length<9)){
			$("#other_email_id_error").show().css("display","block");
			$("#other_email_id_error").html("Please check the email ID provided, it looks incorrect.");
			student_reg.push('other_email_id');
			flag = false;
		}
		
		if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15"){
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			student_reg.push('mobile_number');
			flag = false;
		}
		if($("#mobile_number").val()!="" && !mobile_regx.test($("#mobile_number").val()))
		{
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number entered.");
			student_reg.push('mobile_number');
			flag = false;
		}
		if(!document.getElementById('checkboxInput').checked){
			$("#term_error").show().css({"display":"block","clear":"both"});
			$("#term_error").html("You must agree terms and conditions before registration.");
			student_reg.push('term');
			flag = false;
		}
		if(!flag){
			scroll_error(student_reg)
			return false;
		}
		else{
			return flag;
		}
	});
	$(".submit-aadhaar-number").click(function(e){
		e.preventDefault();
		$(".adhar-number-validation").hide();
		$(".field-error").hide();
		var mobile_regx=/[0-9]/;
		var regx = /^[A-Za-z0-9]+$/;
		var sum = 0;
		$( ".inputs" ).each(function( index ) {
			sum = sum + parseInt($( this ).val().length);
		});
		if(($("#adharNumber1").val()!="" && !mobile_regx.test($("#adharNumber1").val())) || ($("#adharNumber2").val()!="" && !mobile_regx.test($("#adharNumber2").val())) || ($("#adharNumber3").val()!="" && !mobile_regx.test($("#adharNumber3").val())))
		{
			$(".adhar-number-validation").html("Please enter valid Aadhaar Number.").fadeIn().css("display","block");
		}
		if(sum=='12' && document.getElementById('checkboxInput').checked && $("#captch_code").val().length=="5"){
			$(this).hide();
			$("#verify-btn").show();
			$("#captcha-section").css("display","none");
			$("#checkboxInput").attr('disabled', true);
			$("#otp-section").fadeIn();
			$(".inputs").addClass('non-editable');
		}
		else{
			if($("#captch_code").val()=="")
			$("#captch_code_error").html("Please enter captcha code.").fadeIn().css("display","block");
			else if($("#captch_code").val().length!=5 || !regx.test($("#captch_code").val()))
			$("#captch_code_error").html("Please enter valid captcha code.").fadeIn().css("display","block");
			if(!document.getElementById('checkboxInput').checked)
			$("#checkboxInput_error").html("Please check checkbox for authentication.").fadeIn().css("display","block");
			if(sum=='0')
			$(".adhar-number-validation:eq(0)").fadeIn();
			else if(sum!='12')
			$(".adhar-number-validation:eq(1)").fadeIn();
		}
		
		return false;
	});
	
	$("#verify-btn").click(function(){
		var mobile_regx=/[0-9]/;
		$(".field-error").hide();
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
			return false;
		}
		else if($("#otp").val().length!="6" || !mobile_regx.test($("#otp").val())){
			$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
			return false;
		}
	});
	/* Js By Anjali*/
	$(".inputs").keyup(function (e) {
		if(e.keyCode=='8' || e.keyCode=='46' || e.keyCode=='229'){
			if (this.value.length == '0' ) {
				$(this).prev('.inputs').focus();
			}
		}
		else{
			if (this.value.length == '4' ) {
				$(this).next('.inputs').focus();
			}
		}
	});
});

var specialKeys = new Array();
specialKeys.push(8); //Backspace
specialKeys.push(9); //Tab
specialKeys.push(13); //Enter
specialKeys.push(46); //Delete
specialKeys.push(35); //End
specialKeys.push(36); //Home
specialKeys.push(37); //Left Arrow
specialKeys.push(38); //Up Arrow
specialKeys.push(39); //Right Arrow
specialKeys.push(40); //Down Arrow
function IsNumeric(event) {
	/*var keyCode = e.which ? e.which : e.keyCode
		if (e.shiftKey)
		return false;
		var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
	return ret;*/
	var keyCode = event.keyCode || event.which
	// alert(keyCode)
	if($.isNumeric( String.fromCharCode(event.which) ))
	return true;
	var keyCode = event.keyCode || event.which
	if(String.fromCharCode(event.which)=="'" || String.fromCharCode(event.which)==".")
	return false;
	
	if (event.shiftKey)
	return false;
	
	//console.log(keyCode+"=="+event.keyCode+"=="+event.which)
	if(keyCode==40)
	return false;
	/*if(keyCode==46)
	return false;*/
	if(keyCode==8 || keyCode==46 || keyCode==229 || specialKeys.indexOf(keyCode) != -1)
	return true;
	var regex = new RegExp("^[0-9]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	
	if (!regex.test(key) || keyCode==0) {
		return false;
	}
}

$('input[readonly]').focus(function(){
	this.blur();
});

$(document).on('focus blur','.form-control',function(e){
	e.preventDefault();
	if(e.type === 'focusin' || this.value.length > 0){
		$(this).parents('.form-group').addClass('focused');
	}
	else{
		$(this).parents('.form-group').removeClass('focused');
	}
});
$(document).ready(function(){
	$('.form-control').on('focus blur', function (e) {
		$(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
	}).trigger('blur');
});

if($(window).width() > 768){
	$(".dropdown").hover(
	function() {
		$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
		$(this).toggleClass('open');        
	},
	function() {
		$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
		$(this).toggleClass('open');       
	}
	);
};

function printContent(el){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}

function isAlphaSpace(event) {
	var inputValue = event.charCode;
	if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
		event.preventDefault();
	}
}

function isAlphaNumeric(event) {
	var keyCode = event.keyCode || event.which
	
	if (event.shiftKey)
	return false;
	
	// Don't validate the input if below arrow, delete and backspace keys were pressed 
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}    
	
	var regex = new RegExp("^[a-zA-Z0-9-]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	
	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function addressValidation(event) {
	var keyCode = event.keyCode || event.which
	
	if (event.shiftKey)
	return false;
	
	// Don't validate the input if below arrow, delete and backspace keys were pressed 
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}    
	
	var regex = new RegExp("^[a-zA-Z0-9-.,/ ]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	
	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function nadIDValidation(event) {
	var str= $('#nad_id').val();
	if(typeof str == 'undefined'){
		var str= $('#nad_certificate_id').val();
		if(typeof str == 'undefined'){
			var str= $('#forgot_user_id').val();
		}
	}
	var keyCode = event.keyCode || event.which
	//console.log(keyCode)
	if(str.length == 0 && keyCode>=48 && keyCode<=57 ){
		return false;
	}
	if(keyCode == 46 || keyCode == 39){
		return false;
	}
	if (keyCode!="78" && keyCode!="67" && event.shiftKey)
	return false;
	// Don't validate the input if below arrow, delete and backspace keys were pressed 
	if (keyCode == 8 || keyCode==229 || keyCode==46 || (keyCode >= 35 && keyCode <= 40) || keyCode==99 || keyCode==110 || keyCode==78 || keyCode==67) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		if(str.length>0 && (keyCode==99 || keyCode==110 || keyCode==78 || keyCode==67)){
			return false;
		}
		return;
	}    
	
	var regex = new RegExp("^[NC0-9]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	
	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function onlyAlphaNumeric(event) {
	var keyCode = event.keyCode || event.which
	if (event.shiftKey || keyCode=="39")
	return false;
	
	// Don't validate the input if below arrow, delete and backspace keys were pressed 
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}    
	
	var regex = new RegExp("^[a-zA-Z0-9]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	
	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function uniquerefnovalidation(event) {
	var keyCode = event.keyCode || event.which
	
	if (event.shiftKey)
	return false;
	
	// Don't validate the input if below arrow, delete and backspace keys were pressed 
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}    
	
	var regex = new RegExp("^[a-zA-Z0-9-/]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	
	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function isEmail(email) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(email);
	/*var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);*/
}



$("#mobile_number").keydown(function(e) {
	var selected = $(".nationality:checked").val();
	if(selected=="Foreigner"){
	}
	else{
		var oldvalue=$(this).val();
		var field=this;
		if(field.value.indexOf('+91 ') !== 0) {
			if(oldvalue=="+91")
				oldvalue="+91 ";
			$("#mobile_number").val(oldvalue);
		}
		else{
		}
		/*var oldvalue=$(this).val();
		var field=this;
		setTimeout(function () {
			if(field.value.indexOf('+91 ') !== 0) {
				$(field).val(oldvalue);
			} 
		}, 1);*/
	}
});

$("#bill_mobile_number").keydown(function(e) {
	/*var oldvalue=$(this).val();
	var field=this;
	setTimeout(function () {
		if(field.value.indexOf('+91 ') !== 0) {
			$(field).val(oldvalue);
		} 
	}, 1);*/
	var oldvalue=$(this).val();
	var field=this;
	setTimeout(function () {
		if(field.value.indexOf('+91 ') !== 0) {
			$(field).val(oldvalue);
		} 
	}, 1);
});

$("#bill_mobile_number").keyup(function(e) {
	var get_focus =f1(document.getElementById('bill_mobile_number'));
	var oldvalue = $(this).val().replace(/\s/g, '') ;
	var len = $(this).val().length;
	
	if(get_focus==9 && e.keyCode=="8"){
		get_focus--;
		var str1 = oldvalue.substring(0, 3);
		var str2 = oldvalue.substring(3, 7)+oldvalue.substring(8,9);
		var str3 = oldvalue.substring(9);
	}
	else{
		var str1 = oldvalue.substring(0, 3);
		var str2 = oldvalue.substring(3, 8);
		var str3 = oldvalue.substring(8);
	}
	
	if(str3!=""){
		$(this).val(str1+" "+str2+" "+str3);
	}
	else if(str2!=""){
		$(this).val(str1+" "+str2);
	}
	if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
		setCaretPosition('bill_mobile_number', get_focus)
	}
});

$("#bill_pincode").keyup(function(e) {
	var get_focus =f1(document.getElementById('bill_pincode'));
	var oldvalue = $(this).val().replace(/\s/g, '') ;
	var len = $(this).val().length;
	
	if(get_focus==3 && e.keyCode=="8"){
		get_focus--;
		var str1 = oldvalue.substring(0, 2)+oldvalue.substring(3, 4);
		var str2 = oldvalue.substring(4);
	}
	else{
		var str1 = oldvalue.substring(0, 3);
		var str2 = oldvalue.substring(3);
	}
	if(str2!=""){
		$(this).val(str1+" "+str2);
	}
	if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
		setCaretPosition('bill_pincode', get_focus)
	}
});

$("#mobile_number").keyup(function(e) {
	
	var get_focus =f1(document.getElementById('mobile_number'));
	var oldvalue = $(this).val().replace(/\s/g, '') ;
	var len = $(this).val().length;
	
	var selected = $(".nationality:checked").val();
	if(selected=="Foreigner"){
		/*if(get_focus==11 && e.keyCode=="8"){
			var str1 = oldvalue.substring(0, 5);
			var str2 = oldvalue.substring(5, 9)+oldvalue.substring(10,11);
			var str3 = oldvalue.substring(11);
			}
			else*/ if(get_focus==5 && e.keyCode=="8"){
			get_focus--;
			var str1 = oldvalue.substring(0, 4)+oldvalue.substring(5,6);
			var str2 = oldvalue.substring(6, 11);
			var str3 = oldvalue.substring(11);
		}
		else{
			var str1 = oldvalue.substring(0, 5);
			var str2 = oldvalue.substring(5, 10);
			var str3 = oldvalue.substring(10);
		}
		/*if(str3!=""){
			if(get_focus!=12)
			get_focus++;
			$(this).val(str1+" "+str2+" "+str3);
			}
			else*/ if(str2!=""){
			/*get_focus++;
				if(get_focus==5){
				get_focus++;
				get_focus++;
			}*/
			$(this).val(str1+" "+str2);
		}
		if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
			setCaretPosition('mobile_number', get_focus)
		}
	}
	else{     
		if(get_focus==9 && e.keyCode=="8"){
			get_focus--;
			var str1 = oldvalue.substring(0, 3);
			var str2 = oldvalue.substring(3, 7)+oldvalue.substring(8,9);
			var str3 = oldvalue.substring(9);
		}
		else{
			var str1 = oldvalue.substring(0, 3);
			var str2 = oldvalue.substring(3, 8);
			var str3 = oldvalue.substring(8);
		}
		
		if(str3!=""){
			$(this).val(str1+" "+str2+" "+str3);
		}
		else if(str2!=""){
			$(this).val(str1+" "+str2);
		}
		if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
			setCaretPosition('mobile_number', get_focus)
		}
	}
});




function setCaretPosition(elemId, caretPos) {
	var el = document.getElementById(elemId);
	
	if (el !== null) {
		
		if (el.createTextRange) {
			var range = el.createTextRange();
			range.move('character', caretPos);
			range.select();
			return true;
		}
		else {
			if (el.selectionStart || el.selectionStart === 0) {
				el.focus();
				el.setSelectionRange(caretPos, caretPos);
				return true;
			}
			
			else  { // fail city, fortunately this never happens (as far as I've tested) :)
				el.focus();
				return false;
			}
		}
	}
}

function f1(el) {
	var val = el.value;
	return val.slice(0, el.selectionStart).length;
}



function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#imgView').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
}

$("#browsImg").change(function(){
	readURL(this);
});




function readURL1(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#signatureView').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
}

$("#browsSig").change(function(){
	readURL1(this);
});



/*$(".mob-verify-btn").click(function(e){
	e.preventDefault();
	$(".reg-success").fadeIn();
	setInterval(function(){ $(".reg-success").fadeOut(); }, 3000);
});*/

function getAge(birth) {
	var today = new Date();
	var curr_date = today.getDate();
	var curr_month = today.getMonth() + 1;
	var curr_year = today.getFullYear();
	
	var pieces = birth.split('/');
	var birth_date = pieces[1];
	var birth_month = pieces[0];
	var birth_year = pieces[2];
	
	if (curr_month == birth_month && curr_date >= birth_date) return parseInt(curr_year-birth_year);
	if (curr_month == birth_month && curr_date < birth_date) return parseInt(curr_year-birth_year-1);
	if (curr_month > birth_month) return parseInt(curr_year-birth_year);
	if (curr_month < birth_month) return parseInt(curr_year-birth_year-1);
}




$(document).ready(function(){
	$('input[type="radio"]').change(function(){
		if($(this).attr("value")=="yes-content"){            
			$(".yes-content").show();
			$(".no-content").hide();
			// $("#no-text").html("Have you ever registered earlier with NAD (through NDML or CVL)?");
			}else{
			$(".yes-content").hide();
			$(".no-content").show();
			// $("#no-text").html("To get a consolidated view of all your certificates, it is important to use your existing NAD ID if already registered.");
		}      
	});
});


/************** JS by Deepti start *************/
$(document).ready(function(){
	//owl slider		
	$("#owl-demo").owlCarousel({
		nav: false,
		autoplay:true,
		dots: true,
		items:1,
		loop: true,
		animateOut: 'fadeOut'
	});	
	
	var winWidth = $(window).width();
	var winHeight = $(window).height();
	if (winWidth > 768){
		var headerHeight = $(".custom-nav").height() + 30;
		var bannerImgHeight = winHeight - headerHeight;
		$(".banner-img").css("height", bannerImgHeight);
		$("#owl-demo li").css("height", bannerImgHeight);
		/* var bannerImg = $("#owl-demo .owl-item img").attr("src");
		$("#owl-demo .owl-item li").css({"backgroundImage": "url(" + bannerImg + ")","background-size":"cover"}); */
	}
	
	$("#owl-demo1").owlCarousel({
		nav: true,
		autoplay:true,
		smartSpeed : 1000,
		dots: false,
		items:1,
		loop: true,
		margin:20,
		rewindNav : false,  
		pagination : false, 
	});
	
	$("#owl-demo2").owlCarousel({
		nav: true,
		autoplay:true,
		smartSpeed : 1000,
		dots: false,
		items:3,
		loop: true,
		margin:20,
		rewindNav : false,  
		pagination : false, 
		responsive: {
			0: {
				items: 1
			},
			480: {
				items: 2
			},
			1024: {
				items: 3
			}
		},
	});
	
	
	$(".stepTabList li a").click(function(){
		$(".stepTabList li a").removeClass("activeTab");
		$(this).addClass("activeTab");
		var tabName = $(this).attr("for");
		$(".step-div").css("display","none");
		$("." + tabName).fadeIn();
	});
	
	/* JS BY Anjali*/
	
	
	$(".submit-forgot-password1").click(function(){
		var flag = true;
		var regx = /^[A-Za-z0-9]+$/;
		$("#forgot_user_id_error").html("");
		$(".field-error").html("");
		if($("#forgot_user_id").val()==""){
			$("#forgot_user_id_error").html("User ID information is not filled.");
			flag = false;
		}
		else if($("#forgot_user_id").val().length<6 || $("#forgot_user_id").val().length>30){
			$("#forgot_user_id_error").html("ID entered by you does not exist or is incorrect, kindly check the ID and try again.");
			flag = false;
		}
		else{
			if(!isEmail($("#forgot_user_id").val())){
				var letters = /^[a-zA-Z0-9]+$/;
				if(!(letters.test($("#forgot_user_id").val()))){
					$("#forgot_user_id_error").html("ID entered by you does not exist or is incorrect, kindly check the ID and try again.");
					flag = false;
				}
			}
		}
		/*$("#forgot_user_id_error").html("");
			$(".field-error").html("");
			if($("#forgot_user_id").val()==""){
			$("#forgot_user_id_error").html("User ID information is not filled.");
			flag = false;
			}
			else if($("#forgot_user_id").val().length!=14){
			$("#forgot_user_id_error").html("ID entered by you does not exist or is incorrect, kindly check the ID and try again.");
			flag = false;
			}
			
			if($("#forgotUserID").val()=="addharId"){
			var str = $("#forgot_user_id").val().replace(/\s/g, '') ;
			if(!$.isNumeric(str)){
			$("#forgot_user_id_error").html("Please enter valid aadhar ID.").show();
			flag = false;
			}
			}
			else{
			var str = $("#forgot_user_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="N" || fisrtChar=="C" || fisrtChar=="n" || fisrtChar=="c"){
			var subStr = str.substring(1);
			if(!$.isNumeric(subStr)){
			$("#forgot_user_id_error").html("Please enter valid NAD ID.").show();
			flag = false;
			}
			}
			else{
			$("#forgot_user_id_error").html("Please enter valid NAD ID.").show();
			flag = false;
			}
		}*/
		
		if($("#captch_code").val()==""){
			$("#captch_code_error").html("Please enter captcha code.").fadeIn().css("display","block");
			flag = false;
		}
		else if($("#captch_code").val().length!=5 || !regx.test($("#captch_code").val())){
			$("#captch_code_error").html("Please enter valid captcha code.").fadeIn().css("display","block");
			flag = false;
		}
		if(flag){
			/* $(".stepTabList li:eq(1)").css("pointer-events","auto"); */
			$("#otp_send_msg").fadeIn();
			/*$(".idtype").text($("#forgotUserID option:selected").text());*/
			$(".idtype").text($("#forgot_user_id").val());
			setTimeout(function(){
				$(".steptabOne").css("display","none"); 
				$(".steptabTwo").css("display","block"); 
				$(".stepTabList li:nth-child(2) a").addClass("activeTab");
				$(".stepTabList li:nth-child(2)").addClass("activeLeftLine");
			}, 3000);
			
		}
		return false;
	});
	
	$(".submit-forgot-password2").click(function(){
		var flag = true;
		$(".field-error").html("");
		$("#otp_fail_msg").hide();
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
			flag = false;
		}
		else if($("#otp").val().length!="6"){
			//$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
			$("#otp_fail_msg").show();
			flag = false;
		}
		if(flag){
			/* $(".stepTabList li:eq(2)").css("pointer-events","auto"); */
			/*$("#otp_validate_success").fadeIn();
			setTimeout(function(){ $(".steptabTwo").css("display","none"); $(".steptabThree").css("display","block"); }, 3000);*/
			$(".steptabTwo").css("display","none"); $(".steptabThree").css("display","block");
			$(".stepTabList li:nth-child(3) a").addClass("activeTab");
			$(".stepTabList li:nth-child(2)").addClass("activeRightLine");
		}
		return false;
	});
	
	$(".submit-forgot-password3").click(function(){
		var flag = true;
		$(".field-error").html("");
		$("#password_fail_msg").hide();
		if($("#user_password").val()==""){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password information is not filled.");
			flag = false;
		}
		else {
			var regx = /^[A-Za-z0-9]+$/;
			if($("#user_password").val().length<8){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be minimum 8.");
				flag = false;
			}
			else if($("#user_password").val().length>20){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be maximum 20");
				flag = false;
			}
			else if(!regx.test($("#user_password").val())){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password should contain only alphanumeric.");
				flag = false;
			}
		}
		
		if($("#confirm_password").val()==""){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Confirm Password information is not filled.");
			flag = false;
		}
		
		if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
			if($("#user_password").val()!=$("#confirm_password").val()){
				$("#password_fail_msg").show();
				flag = false;
			}
		}
		if(flag){
			$("#password_change_success").show();
		}
		return false;
	});
	$("#nad_id").keyup(function(e) {
		e.preventDefault();
		var get_focus =f1(document.getElementById($(this).attr('id')));
		var oldvalue = $(this).val().replace(/\s/g, '') ;
		var len = $(this).val().length;
		
		if(get_focus==4 && e.keyCode=="8"){
			get_focus--;
			var str1 = oldvalue.substring(0, 3)+oldvalue.substring(4,5);
			var str2 = oldvalue.substring(5, 9);
			var str3 = oldvalue.substring(9);
		}
		else if(get_focus==9 && e.keyCode=="8"){
			get_focus--;
			var str1 = oldvalue.substring(0, 4);
			var str2 = oldvalue.substring(4, 7)+oldvalue.substring(8,9);
			var str3 = oldvalue.substring(9);
		}
		else{
			var str1 = oldvalue.substring(0, 4);
			var str2 = oldvalue.substring(4, 8);
			var str3 = oldvalue.substring(8);
		}
		if(str3!=""){
			$(this).val(str1+" "+str2+" "+str3);
		}
		else if(str2!=""){
			$(this).val(str1+" "+str2);
		}
		else{
			$(this).val(str1);
		}
		if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
			setCaretPosition($(this).attr('id'), get_focus)
		}
	});
	
	/*$("#forgotUserID").change(function(){
		var inputField = '<input maxlength="14" value="" id="forgot_user_id" type="tel" class="form-control maxlenghtValidation" data-toggle="tooltip" data-placement="bottom" title="User ID should be minimum 6 characters long without spaces" onkeypress="return IsNumeric(event);">';
		if($(this).val() == 'nadId'){
		inputField = '<input maxlength="14" value="" id="forgot_user_id" type="text" class="form-control maxlenghtValidation" data-toggle="tooltip" data-placement="bottom" title="User ID should be minimum 6 characters long without spaces">';
		}
	});*/
	
	$(".masterDataUpdateLink").click(function(){
		if ($('.modal:visible').length) {
			/* $('body').addClass('modal-open'); */
			$('#myModal').modal('hide');
		}
		
		/* $("body").addClass("modal-open"); */
		/* $("body").css({"overflow":"hidden","paddingRight":"17px"});  */
	});
	
	$(".masterType").css("display","block");
	$('#filterBy').change(function(){
		var filterVal = $('#filterBy').val();
		$(".filterByType").css("display","none");
		$("." + filterVal).fadeIn();
		var attr = $(this).attr('hideDiv');
		if (typeof attr !== typeof undefined && attr !== false) {
		    $("#"+attr).addClass("showHideMasterData");
		}
		var attr = $(this).attr('reset_id');
		if (typeof attr !== typeof undefined && attr !== false) {
		    $('#'+attr)[0].reset();
		    $('#filterBy').val(filterVal);
		}
	});
	
	
});
/*************** JS by Deepti end ************/


$(document).ready(function() {
	/* $('input[type!="button"][type!="submit"], select, textarea').val('').blur(); */
	var $scrollingDiv = $(".id-based-edit");
	
	$(window).scroll(function(){			
		$scrollingDiv
		.stop()
		.animate({"marginTop": ($(window).scrollTop()) + "px"}, "slow" );	
	});
	
	$('.show-password-button').on('click', function () {
		
		const $this = $(this);
		const $input = $($this.data('target'));
		
		const newType = ($input.attr('type') === 'password') ? 'text' : 'password';
		if(newType=="text"){
			$(".view-input").show();
			$("#user_passwordClone1").hide();
			$("#user_passwordClone0").hide();
			$("#user_passwordClone2").hide();
		}
		else{
			$(".view-input").hide();
			$("#user_passwordClone1").show();
			$("#user_passwordClone0").show();
			$("#user_passwordClone2").show();
		}	
		$input.attr('type', newType);
		$(this).find("i").toggleClass("fa-eye-slash")
		
	});
	
});




function scroll_error(id){
	$("form input").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$("form select").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$(".bootstrap-select.custom-select").find('button').removeClass('error-focused');
	if(typeof id[0] !== 'undefined' && id[0] !== '' && id[0]!=='term'){
		$('html, body').animate({
			scrollTop: $("#"+id[0]).offset().top-$("nav").height()
		}, 1200);
	}
	$.each(id,function(key,val){
		if(val!="certificate_checkbox_table")
			$('#'+val).addClass('error-focused')
	})
	
	$('.selectpicker').each(function(){
		if ($(this).hasClass('error-focused')){
			$(this).prev().prev('button').addClass('error-focused');
		}
		else{
			$(this).prev().prev('button').removeClass('error-focused');
		}
	});
}

$("#userLoginSubmit").click(function(e){
	var flag=1;
	var regx = /^[A-Za-z0-9]+$/;
	e.preventDefault();
	
	if($("#userId").val()==""){
		$("#userId-error").html("Please enter User Id.").fadeIn().css("display","block");
		flag=0;
		}else if($("#userId").val().length<6 || $("#userId").val().length>30){
		$("#userId-error").html("Please enter valid user ID.").fadeIn().css("display","block");
		flag = 0;
	}
	if($("#userPassword").val()==""){
		$("#userPassword-error").html("Please enter Password.").fadeIn().css("display","block");
		flag=0;
	}
	if($("#captchcode").val()==""){
		$("#captchaCode-error").html("Please enter captcha code.").fadeIn().css("display","block");
		flag=0;
		}else if(!regx.test($("#captchcode").val())){
		$("#captchaCode-error").html("Please enter valid captcha code.").fadeIn().css("display","block");
		flag=0;
	}
	if(flag==1){
		if($("#typeOfMaster").val()=="Student")
		window.location.href="student_landing_page_5_1.html";
		if($("#typeOfMaster").val()=="Academic Institution")
		window.location.href="";
		if($("#typeOfMaster").val()=="Verifier")
		window.location.href="";
	}
});

/* Approve data A2.0 */
function searchMasterData()
{
	$("#master-data-form").addClass("showHideMasterData");
	$(".field-error").html("");
	var regx = /^[A-Za-z0-9]+$/;
	var flag=1;
	if($("#filterBy").val()=="masterType")
	{
		var d=new Date();
		var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();
		
		if($("#to_date").val()==""){
			$("#to_date_error").html("Please select To Date.").fadeIn().css("display","block");
			flag=0;
		}
		if($("#from_date").val()==""){
			$("#from_date_error").html("Please select From Date.").fadeIn().css("display","block");
			flag=0;
		}
		if($("#to_date").val()!="" && $("#from_date").val()!="")
		{
			if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
				$("#from_date_error").html("From Date should not be greater than To Date").fadeIn().css("display","block");
				flag=0;
				}else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
				$("#from_date_error").html("From date should not be more than 90 day old").fadeIn().css("display","block");
				flag=0;
			}
			else if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
				$("#to_date_error").html("To Date should not be greater than current date").fadeIn().css("display","block");
				flag=0;
			}
			
		}
	}
	else if($("#filterBy").val()=="fileUploadReference")
	{
		if($("#reference_id").val()!="")
		{
			if(!regx.test($("#reference_id").val())){
				$("#reference_id_error").html("File Upload Reference No should be alphanumeric.").fadeIn().css("display","block");
				flag = 0;
			}
		}
	}
	if(flag==1)
	{
		$("#master-data-form").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	}
}

function processDateFormat(date) {
	var parts = date.split("/");
	return new Date(parts[2], parts[0] - 1, parts[1]);
}

function updateStatus(type){
	console.log(type);
	$(".field-error").html("");
	var flag=1;
	var atLeastOneIsChecked = false;
	$('input:radio').each(function () {
		if ($(this).is(':checked')) {
			atLeastOneIsChecked = true;
			return false;
		}
	});
	if(atLeastOneIsChecked==false)
	{
		var flag=0;
		$("#masterFile_error").html("Please select a file").fadeIn().css("display","block");
	}
	if(type=="reject" && $("#remark").val()=="")
	{
		$("#remark_error").html("Please enter remakrs").fadeIn().css("display","block");
		var flag=0;
	}
	if(flag==1)
	{
		$("#fileNo").html($("#file_no").val());
		if(type=="approve"){
			$("#masterStatus").html("Approved");
			$("#approveSubmit").attr('href','#myModal');
			}else{
			$("#masterStatus").html("Rejected");
			$("#rejectSubmit").attr('href','#myModal');
		}
	}
}

$("#approveReset").click(function(e){
	e.preventDefault();
	$(".field-error").html("");
	$('#approveMasterDataForm')[0].reset();
	$("#master-data-form").addClass("showHideMasterData");
});

function setFileNo(ref_no)
{
	$("#file_no").val(ref_no);
}
/*Approve Certificate data */
function searchCertificateData()
{
	$("#certificate-data-form").addClass("showHideMasterData");
	$(".field-error").html("");
	var regx = /^[A-Za-z0-9]+$/;
	var flag=1;
	if($("#filterBy").val()=="awardType")
	{
		var d=new Date();
		var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();
		
		if($("#to_date").val()==""){
			$("#to_date_error").html("Please select To Date.").fadeIn().css("display","block");
			flag=0;
		}
		if($("#from_date").val()==""){
			$("#from_date_error").html("Please select From Date.").fadeIn().css("display","block");
			flag=0;
		}
		if($("#to_date").val()!="" && $("#from_date").val()!="")
		{
			if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
				$("#from_date_error").html("From Date should not be greater than To Date").fadeIn().css("display","block");
				flag=0;
				}else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
				$("#from_date_error").html("From date should not be more than 90 day old").fadeIn().css("display","block");
				flag=0;
			}
			else if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
				$("#from_date_error").html("To Date should not be greater than current date").fadeIn().css("display","block");
				flag=0;
			}
			
		}
	}
	else if($("#filterBy").val()=="fileUploadReference")
	{
		if($("#reference_id").val()!="")
		{
			if(!regx.test($("#reference_id").val())){
				$("#reference_id_error").html("File Upload Reference No should be alphanumeric.").fadeIn().css("display","block");
				flag = 0;
			}
		}
	}
		/*24/12/2016*/
	if(flag==1)
	{
		$("#certificate-data-form").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	}
}

function resetForm(formName,divName)
{
	$(".field-error").html("");
	$('#'+formName)[0].reset();
	$("#"+divName).addClass("showHideMasterData");
}

function viewUploadedCertificateData()
{
	$("#viewUploadCertificateData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
}

function onlineVerificationData()
{
	$("#viewOnlineVerirficationData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;

	if($("#filterBy").val()=="CertificateType")
	{
		if($("#university").val().trim()==""){
			flag=0;
			$("#university_error").html("Any of the specified value need to be selected").css("display","block");
		}

		if($("#course").val().trim()==""){
			flag=0;
			$("#course_error").html("Any of the specified value need to be selected").css("display","block");
		}

		if($("#certificate_type").val().trim()==""){
			flag=0;
			$("#certificate_type_error").html("Any of the specified value need to be selected").css("display","block");
		}

		if($("#academic_year").val().trim()==""){
			flag=0;
			$("#academic_year_error").html("Any of the specified value need to be selected").css("display","block");
		}

		if($("#rollNo").val().trim()==""){
			flag=0;
			$("#rollNo_error").html("Roll No is not filled.").css("display","block");
		}
	}
	else if($("#filterBy").val()=="fileUploadReference"){
		if($("#nad_certificate_id").val()==""){
			flag=0;
			$("#nad_certificate_id_error").html("NAD Certificate ID is not filled.").css("display","block");
		}
		else if($("#nad_certificate_id").val().length!="12"){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").show();
		}
		else{
			var str = $("#nad_certificate_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag=0;
					$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
				}
			}
			else{
				flag=0;
				$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
			}
		}
	}

	if(flag==1){
		$("#viewOnlineVerirficationData").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	}
}

/* View UPLOAD STATUS */

function searchUploadStatusData()
{
	$("#viewuploadstatus-data-form").addClass("showHideMasterData");
	$(".field-error").html("");
	var regx = /^[A-Za-z0-9]+$/;
	var flag=1;
	if($("#filterBy").val()=="awardType")
	{
		var d=new Date();
		var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();
		
		if($("#to_date").val()!="" && $("#from_date").val()!="")
		{
			if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
				$("#from_date_error").html("From Date should not be greater than To Date").fadeIn().css("display","block");
				flag=0;
				}else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
				$("#from_date_error").html("From date should not be more than 90 day old").fadeIn().css("display","block");
				flag=0;
			}
			else if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
				$("#from_date_error").html("To Date should not be greater than current date").fadeIn().css("display","block");
				flag=0;
			}
			
		}
	}
	else if($("#filterBy").val()=="fileUploadReference")
	{
		if($("#reference_id").val()!="")
		{
			if(!regx.test($("#reference_id").val())){
				$("#reference_id_error").html("File Upload Reference No should be alphanumeric.").fadeIn().css("display","block");
				flag = 0;
			}
		}
	}
	/*24/12/2016*/
	if(flag==1)
	{
		$("#viewuploadstatus-data-form").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	}
	
	
}



