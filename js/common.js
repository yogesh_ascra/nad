var states = "Andaman and Nicobar Islands|Andhra Pradesh|Arunachal Pradesh|Assam|Bihar|Chandigarh|Chhattisgarh|Dadra and Nagar Haveli|Daman and Diu|Delhi|Goa|Gujarat|Haryana|Himachal Pradesh|Jammu and Kashmir|Jharkhand|Karnataka|Kerala|Lakshadweep|Madhya Pradesh|Maharashtra|Manipur|Meghalaya|Mizoram|Nagaland|Orissa|Pondicherry|Punjab|Rajasthan|Sikkim|Tamil Nadu|Tripura|Uttar Pradesh|Uttaranchal|West Bengal";

var mobile_number = '';
var email_id = '';
function set_type_of_grievance(type,val){
	var arr = [];
	if(type=="Student"){
		if(val=="AI"){
			var arr = ['Verification of NAD ID Pending', 'Verification of NAD ID Erroneously Rejected','Linking of Records Pending Award not seeded with Aadhaar','Invalid Verification request','Others'];
		}
		else if(val=="Verifier"){
			var arr = ['Invalid Verification request','Verifier details not correct','Others'];
		}
		else if(val=="NDML NAD"){
			var arr = ['Technical problem in using system','Incorrect data','Transaction not completed','Payment related issue','Others'];
		}
		else if(val=="CVL NAD"){
			var arr = ['Not able to register through CVL NAD ID','Certificate not linked','Others'];
		}
	}
	else if(type=="Verifier"){
		if(val=="Student"){
			var arr = ['Incorrect data','Others'];
		}
		else if(val=="AI"){
			var arr = ['Others'];
		}
		else if(val=="CVL NAD"){
			var arr = ['Others'];
		}
		else if(val=="NDML NAD"){
			var arr = ['Incorrect data','Technical problem in using system','Transaction not completed','Payment related issue','Others'];
		}
	}
	else if(type=="AI"){
		if(val=="Student"){
			var arr = ['Incorrect data','Others'];
		}
		else if(val=="Verifier"){
			var arr = ['Others'];
		}
		else if(val=="NDML NAD"){
			var arr = ['Incorrect data','Technical problem in using system','Transaction not completed','Payment related issue','Others'];
		}
		else if(val=="CVL NAD"){
			var arr = ['Data not interoperable','Others'];
		}
	}
	var html='';
	for (var i =0; i < arr.length;i++) {
		html+='<option value="'+arr[i]+'">'+arr[i]+'</option>';
	}
	$("#type_of_grievance").html(html);
	if(html!='')
		$('#type_of_grievance').selectpicker('refresh');
}

jQuery(document).ready(function($) {

	$('#main-modal .pop-up-btn').on('click', function() {
		var href = $(this).attr('href');
		location.href = href;
	});

	$('.toolShow').on('mouseover', function() {
		$(this).siblings('.tooltipCustom').fadeIn();
	});

	$('.toolShow').on('mouseout', function() {
		$(this).siblings('.tooltipCustom').fadeOut();
	});

	$("#user_email_id").blur(function(event) {
		if($("#user_email_id").val()!="" && checkEmail($("#user_email_id").val()) && $("#user_id").val().trim()==""){
			$("#user_id").val($("#user_email_id").val());
		}
	});

	$(".resendOtp").click(function(event) {
		$(".resendOtpMsg").show();
		setTimeout(function(){
			$(".resendOtpMsg").fadeOut();
		}, 3000);
	});

	$("#datails_of_party").change(function(event) {
		var type = $(this).attr('type');
		var val = $(this).val();
		set_type_of_grievance(type,val);
	});

	$("#datails_of_party").change();

	$("#iama").change(function(event) {
		var val = $(this).val();
		var arr = [];
		if(val=="Student"){
			var arr = ['Students','Registration Related','Academic Institution Related','My Certificate Availability','Certificate Verification Related','Others'];
		}
		else if(val=="AI"){
			var arr = ['Want to Join NAD','NAD Procedure, Manual','Registering Students','Digital Signature related','Lodging Data','Using NAD','Others'];
		}
		else if(val=="Verifier"){
			var arr = ['Want to Join NAD','Verification Process','Student & Certificate Availability in NAD','Verification Fee','Others'];
		}
		else if(val=="Others"){
			var arr = ['Others'];
		}
		var html='<option value="">Select category for query</option>';
		for (var i =0; i < arr.length;i++) {
			html+='<option value="'+arr[i]+'">'+arr[i]+'</option>';
		}
		$("#category").html(html);
		$('#category').selectpicker('refresh');
	});

	var stateElement = document.getElementById("indiaState");
	if(stateElement!=null){
		stateElement.options[0] = new Option('Select State', '');
	    stateElement.selectedIndex = 0;

	    var state_arr = states.split("|");

	    for (var i = 0; i < state_arr.length; i++) {
	        stateElement.options[stateElement.length] = new Option(state_arr[i], state_arr[i]);
	    }
	}

	$(window).load(function(){
		$('#preloader').fadeOut('slow',function(){$(this).remove();});
		$("#profile_mob").attr('readonly',true);
		$("#profile_email").attr('readonly',true);
	});
	//$("input:text").val("");
});
$(document).ready(function(){
		$(".call_update_profile").click(function(event) {
			var function_name = $(this).attr("function_name");
			if($(this).html().indexOf("Submit")!='-1'){
				if(function_name=="update_additiona_details")
					update_additiona_details($(this).attr("id"));
				else if(function_name=="update_bank_details")
					update_bank_details($(this).attr("id"));
				else if(function_name=="update_other_details")
					update_other_details($(this).attr("id"));
				else if(function_name=="update_communication_details")
					update_communication_details($(this).attr("id"));
				//$(this).html('Edit <span class="edit-icon" id="mob_edit_icon"></span>')
			}
			else{
				if(function_name=="update_additiona_details"){
					$("#collapse-3 .master-div").each(function(index, el) {
						$(this).removeClass('non-editable');
					});
				}
				else if(function_name=="update_communication_details"){
					$("#communication_table").removeClass('non-editable');
				}
				else if(function_name=="update_bank_details"){
					$("#collapse-5 .manage-col-width").each(function(index, el) {
						$(this).removeClass('non-editable');
					});
				}
				else if(function_name=="update_other_details"){
					$("#collapse-6 .manage-col-width").each(function(index, el) {
						$(this).removeClass('non-editable');
					});
				}
				$(this).html('Submit <span class="submit-icon" ></span>');
			}
		});
		$("#name_of_academic").change(function(event) {
			if($(this).val()=="Other"){
				$("#name_of_academic_other").parent().removeClass('non-editable');
			}
			else{
				$("#name_of_academic_other").val('');
				$("#name_of_academic_other").parent().addClass('non-editable');
			}
		});
		$("#type_of_academic").change(function(event) {
			$('#type_of_board').removeClass('error-focused');
			$('#type_of_university').removeClass('error-focused');
			$('#type_of_stand_alone_institutions').removeClass('error-focused');
			$("#type_of_board").val("");
			$("#type_of_board").attr('disabled',true);
			$('#type_of_board').selectpicker('refresh');
			$("#type_of_university").attr('disabled',true);
			$('#type_of_university').selectpicker('refresh');
			$("#type_of_stand_alone_institutions").attr('disabled',true);
			$('#type_of_stand_alone_institutions').selectpicker('refresh');
			if($(this).val()=="Board"){
				$("#type_of_board").removeAttr('disabled');
				$('#type_of_board').selectpicker('refresh');
			}
			else if($(this).val()=="University"){
				$("#type_of_university").removeAttr('disabled');
				$('#type_of_university').selectpicker('refresh');
			}
			else if($(this).val()=="University"){
				$("#type_of_university").removeAttr('disabled');
				$('#type_of_university').selectpicker('refresh');
			}
			else if($(this).val()=="Stand Alone Institute"){
				$("#type_of_stand_alone_institutions").removeAttr('disabled');
				$('#type_of_stand_alone_institutions').selectpicker('refresh');
			}
		});
		$("#file_format").change(function(event) {
			$("#sample_data").attr('accept', $(this).val());
		});

		$(document).on("change","#state",function(e){
			$("#address_state").val($(this).val());
			$('#address_state').selectpicker('refresh');
		});
		$("#method_of_creation").change(function(event) {
			if($(this).val()=="ncex"){
				$("#sample_template_error").html("");
				$("#blank_template_error").html("");
				$("#sample_template_error").parent().addClass('non-editable');
				$("#blank_template_error").parent().addClass('non-editable');
				$("#validation_file_error").parent().removeClass('non-editable');
				$(".xml_file_value:eq(0)").text("");
				$(".xml_file_value:eq(1)").text("");
				$("#sample_template").val("");
				$("#blank_template").val("");
				/*$("#sample_template_error").parent().children('.xml_file_value').val("");
				$("#blank_template_error").parent().children('.xml_file_value').val("");*/
			}
			else{
				$("#validation_file_error").html("");
				$("#sample_template_error").parent().removeClass('non-editable');
				$("#blank_template_error").parent().removeClass('non-editable');
				$("#validation_file_error").parent().addClass('non-editable');
			}
		});

		$("#method_of_creation").change();

	    $('.read-more-content').addClass('hide')
		.before('<a class="read-more-show" href="#">Read More</a>')
		$(".dot-span").html("...");
		$('.read-more-show').on('click', function(e) {
		  $(this).next('.read-more-content').removeClass('hide');
		  $(this).addClass('hide');
		  $(".dot-span").html("")
		  e.preventDefault();
		});

	   $(function () {
	   		if( $.isFunction( $.fn.autocomplete ) ){
	            var availableTags = [
	              "ActionScript",
	              "AppleScript",
	              "Asp",
	              "BASIC",
	              "C",
	              "C++",
	              "Clojure",
	              "COBOL",
	              "ColdFusion",
	              "Erlang",
	              "Fortran",
	              "Groovy",
	              "Haskell",
	              "Java",
	              "JavaScript",
	              "Lisp",
	              "Perl",
	              "PHP",
	              "Python",
	              "Ruby",
	              "Scala",
	              "Scheme"
	            ];
	            $("#project_center").autocomplete({
	                source: availableTags
	            });
	        }
        });

	$(".master_data_select").change(function(){
		$("#error-msg").hide();
	});

	$(".datatitle").click(function() {
		var cur_id = $(this).attr("data-target");
		if($(cur_id).is(':visible')) {
			// console.log('If');
			$(this).find("i").attr('class','more-less glyphicon glyphicon-plus');
		}
		else{
			// console.log('else');
			$(this).find("i").attr('class','more-less glyphicon glyphicon-minus');
		}
		$("panel-heading").each(function(){
			// console.log( $(this) );
			if($(this).attr("data-target")!=cur_id){
				$($(this).attr("data-target")).hide();
				$(this).find("i").attr('class','more-less glyphicon glyphicon-plus');
			}
		});
		// console.log('Bottom');
		// $(cur_id).slideToggle();
		// $(this).parents(".panel").siblings().find('div[aria-expanded="true"]').siblings('.panel-collapse').slideUp();
		// $(this).parents(".panel").siblings().find('div[aria-expanded="true"]').find("i").attr('class','more-less glyphicon glyphicon-plus');
	});

	$('.traning-manual .dataheading').on('click', function () {

		var e = $(this);
		var show_div = $(this).siblings('.panel-collapse');
		var is_show = show_div.is(":visible");
		$('.panel-collapse').slideUp();
		$('.traning-manual i').addClass('glyphicon-plus');
		$('.traning-manual i').removeClass('glyphicon-minus');
		if(!is_show){
			show_div.slideDown(200);
			e.find('i').addClass('glyphicon-minus');
			e.find('i').removeClass('glyphicon-plus');
		}

		/*$(this).siblings('.panel-collapse').slideToggle(500);
		$(this).find("i").toggleClass('glyphicon-minus');
		$(this).parents('.panel').siblings().find('.panel-collapse').slideUp(500);
		$(this).parents('.panel').siblings().find('i').addClass('glyphicon-plus');
		$(this).parents('.panel').siblings().find('i').removeClass('glyphicon-minus');*/
	});

	$('.dataheading').on('click', function () {

		setTimeout(function(){
			var dashboardContentHeight = $(".dashboard-content").height();
			$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
			$(".dashboard_side_menu").css("height",dashboardContentHeight + 200); }, 300);
	});

	$('.faq .dataheading').on('click', function () {
		var e = $(this);
		var show_div = $(this).siblings('.panel-collapse');
		var is_show = show_div.is(":visible");
		$('.panel-collapse').slideUp();
		$('.faq i').addClass('glyphicon-plus');
		$('.faq i').removeClass('glyphicon-minus');
		if(!is_show){
			show_div.slideDown(200);
			e.find('i').addClass('glyphicon-minus');
			e.find('i').removeClass('glyphicon-plus');
		}
		/*$(this).find("i").toggleClass('glyphicon-minus');
		$(this).parents('.panel').siblings().find('.panel-collapse').slideUp(500);
		$(this).parents('.panel').siblings().find('i').addClass('glyphicon-plus');
		$(this).parents('.panel').siblings().find('i').removeClass('glyphicon-minus');*/
	});

	$('.network-control  .owl-prev').addClass('fa fa-arrow-left')

	$('.ai-carousel').owlCarousel({
        loop: true,
        autoplay:false,
        margin: 9,
        dots:false,
        nav: true,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
          },
          600: {
            items: 3,
          },
          1000: {
            items: 3
          }
        }
    })

	$('.ai-info-block').owlCarousel({
	    loop: true,
	    autoplay:false,
	    margin: 10,
	    items:1,
	    dots:false,
	    nav: true,
	    responsiveClass: true
	})

	var n = 2
	$('.next').click(function(e){
		$('.demo').hide();
		$('.demo('+n+')').show();
	  var demo_l = $('.demo').length;
	  if(n == demo_l){
	  	$('.next').attr('disabled', 'disabled');
	  }
	  n++;
	});
	var scrollflag = false;
	$('.tabscroll').click(function () {
	    $("html, body").animate({
	        scrollTop: parseInt($('#scroll_id').position().top)-parseInt($("nav").height())
	    }, 300);
	    return false;
	});
	$('.top-tab-scroll li a').click(function () {
		if(scrollflag){
		    $("html, body").animate({
		        scrollTop: parseInt($('#scroll_id').position().top)-parseInt($("nav").height())
		    }, 300);
		    return false;
		}
		else{
			scrollflag = true;
		}
	});

	$("input[value$='Foreigner']").click(function() {
        $(".hidden-filed").hide();
    });
    $("input[value$='Indian']").click(function() {
        $(".hidden-filed").show();
    });

    $("html,body").on("click", ".show-add-info", function(){
		$(".hide-add-info").show();
        $("head").append($('<style> .info-cat.third-info:after { content: "3" }</style>'));
	 	$("head").append($('<style> .info-cat.fourth-info:after { content: "4" }</style>'));
	 	$("head").append($('<style> .info-cat.fifth-info:after { content: "5" }</style>'));
	});
		$("html,body").on("click", ".hide-add-info-click", function(){
		$(".hide-add-info").hide();
	 	$("head").append($('<style> .info-cat.third-info:after { content: "2" }</style>'));
	 	$("head").append($('<style> .info-cat.fourth-info:after { content: "3" }</style>'));
	 	$("head").append($('<style> .info-cat.fifth-info:after { content: "4" }</style>'));
	});

	$(".blankOtherFiled").blur(function(){
		if($(this).val()!=""){
			$("#university").val($("#university option:first").val());
			$("#course").val($("#course option:first").val());
			$("#academic_year").val($("#academic_year option:first").val());
			$("#academic_month").val($("#academic_month option:first").val());
			$("#unique_ref_no").val("");
			$('#linkingRequestForm input').attr('disabled', true);
			$('#linkingRequestForm select').attr('disabled',true);
			$('.selectpicker').selectpicker('refresh');
			$('#nad_certificate_id').attr('disabled', false);
		}
	});
	$(".blankNadId").blur(function(){
		if($(this).val()!=""){
			$('#nad_certificate_id').attr('disabled', true);
			$("#nad_certificate_id").val("");
		}
	});
	$(".blankNadId").change(function(){
		$('#nad_certificate_id').attr('disabled', true);
		$("#nad_certificate_id").val("");
	});
	$('#xmlFile').val("");
	$("#typeOfMaster").change(function(e){
		$(".hideFormData").hide();
		$(".hideFormData1").hide();
		$(".hideFormData2").hide();
		if($(this).val()=="Student Enrolment"){
			$(".hideFormData").show();
		}
		else if($(this).val()=="Course"){
			$(".hideFormData1").show();
		}
		else if($(this).val()=="Affiliated Institutions"){
			$(".hideFormData2").show();
		}
	});
	$("#typeOfMaster").change();
	$(".typeOfMasterFiltter").change(function(e){
		var val = $(this).val();
		$(".field-error").html("");
		$("#uploadMasterDataForm")[0].reset();
		$(".xml_file_value").html("&nbsp;");
		$(this).val(val);
		$("#master-data-form").addClass("showHideMasterData");
	});
	$("#uploadMasterReset").click(function(e){
		$(".field-error").html("");
		$("#uploadMasterDataForm")[0].reset();
		$("#xmlFile").val("");
		$(".xml_file_value").html("&nbsp;");
		$("#xmlFileText").html("Select File");
		$("#remark").html("&nbsp;");
		$("#error-msg").hide();
		$("#master-data-form").addClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	});

	$("#approveSubmit").click(function(e){
		$("#remark").html("&nbsp");
	});

	$('[data-toggle="popover"]').popover();
    $("input[type=password]").mobilePassword();
    $('[data-toggle="tooltip"]').tooltip({'trigger':'focus'});

	// $('.dp').datepick({dateFormat: 'dd-mm-yyyy'});
    $('.dp').datepicker({});
    $('.dp').on('changeDate', function(ev){
        $(this).datepicker('hide');
        $(".dob").addClass("focused");
	});


});

function change_password(){
	var regx = /^[A-Za-z0-9]+$/;
	var scroll = [];
	var flag = true;
	$(".field-error").html("");
	if($("#old_password").val()==""){
		$("#old_password_error").show();
		$("#old_password_error").html("Old Password information is not filled.");
		flag = false;
		scroll.push('old_password');
	}
	else {
		if($("#old_password").val().length<8){
			$("#old_password_error").show();
			$("#old_password_error").html("Old Password length should be minimum 8.");
			flag = false;
			scroll.push('old_password');
		}
		else if($("#old_password").val().length>20){
			$("#old_password_error").show();
			$("#old_password_error").html("Old Password length should be maximum 20");
			flag = false;
			scroll.push('old_password');
		}
		else if(!regx.test($("#old_password").val())){
			$("#old_password_error").show();
			$("#old_password_error").html("Old Password should contain only alphanumeric.");
			flag = false;
			scroll.push('old_password');
		}
	}
	if($("#new_password").val()==""){
		$("#new_password_error").show();
		$("#new_password_error").html("New Password information is not filled.");
		flag = false;
		scroll.push('new_password');
	}
	else {
		if($("#new_password").val().length<8){
			$("#new_password_error").show();
			$("#new_password_error").html("New Password length should be minimum 8.");
			flag = false;
			scroll.push('new_password');
		}
		else if($("#new_password").val().length>20){
			$("#new_password_error").show();
			$("#new_password_error").html("New Password length should be maximum 20");
			flag = false;
			scroll.push('new_password');
		}
		else if(!regx.test($("#new_password").val())){
			$("#new_password_error").show();
			$("#new_password_error").html("New Password should contain only alphanumeric.");
			flag = false;
			scroll.push('new_password');
		}
	}
	if($("#confirm_password").val()==""){
		$("#confirm_password_error").show();
		$("#confirm_password_error").html("Confirm Password information is not filled.");
		flag = false;
		scroll.push('confirm_password');
	}

	if($("#new_password").val()!="" && $("#confirm_password").val()!=""){
		if($("#new_password").val()!=$("#confirm_password").val()){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Hey the confirm password value does not match.. Enter the same password.");
			flag = false;
			scroll.push('confirm_password');
		}
	}
	scroll_error(scroll);
}

function ai_role_creation(){
	var scroll = [];
	$(".field-error").html("");
	var flag = true;
	if($("#role_name").val()==""){
		scroll.push('role_name');
		$("#role_name_error").html("Role Name information is not filled.").show();
		flag = false;
	}
	var checked = $("#approveCertificateForm input:checked").length > 0;
    if (!checked){
    	$("#role_function_error").html('Please select atleast one role function').show();
        flag = false;
    }
	scroll_error(scroll);
	if(flag==true){
		html ='<p>Role '+$("#role_name").val()+' has been created successfully with following functions:</p>';
		var i=97;
		$('input.certificate_checkbox:checkbox:checked').each(function () {
			var sr_no=getAlphabates(i);
    		var functions = $(this).val();
    		html+='<p>'+sr_no+'. '+functions+'</p>';
    		i++;
		});
		$("#succss_msg").html(html);
	}
	else
		return false;
}

function uploadMasterData(){
	var scroll = [];
	$(".field-error").html("");
	var flag = true;
	if($('#xmlFile').val().trim()==""){
		flag = false;
		$("#xmlFile_error").show().css("display","block");
		$("#xmlFile_error").html("Please select .xml file.");
		scroll.push('xmlFileDiv');
	}
	else{
		var extension = $('#xmlFile').val().split('.').pop().toLowerCase();
		if($.inArray(extension, ['xml']) == -1) {
			flag = false;
			$("#xmlFile_error").show().css("display","block");
			$("#xmlFile_error").html("File extension should be .xml.");
			scroll.push('xmlFileDiv');
		}
	}
	scroll_error(scroll);
	if(flag){
		$("#error-msg").show();
		$("#master-data-form").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#master-data-form").offset().top-$("nav").height()
		}, 1200);
	}
}

function sideMenu(){
	var dashboardContentHeight = $(".dashboard-content").height();
	if(dashboardContentHeight > 700)
	{
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	}else{
		$(".dashboard_side_menu").css("height","800px");
	}
}

$(window).load(function(){
	sideMenu();
});

$(document).ready(function() {
	/*vaibhav js*/

	/*24/1/2017*/
	/*$(".search-text-toggle-link").on("click",function(e){
		$(".search-text-toggle").fadeIn(400);
	});*/
	$(document).on('touchstart', function (event) {
		if (!$(event.target).closest('.searchBox').length) {
			$('#MyDivId').hide()
		}
	});

	/*10/01/2017*/
	$(document).on("click",".multiselect",function(e){
		$(this).next(".multiselect-container").find("li a label.checkbox").each(function(){
			var a = $(this).text();
			$(this).find("span").remove();
			$(this).contents().filter(function(){ return this.nodeType != 1; }).remove();
			$(this).append("<span>"+a+"</span>");
			$(".multiselect-container").addClass("mCustomScrollbar");
			$(".multiselect-container").mCustomScrollbar();
		});
	});

	$(".knowing-nad-owl-carousel").owlCarousel({
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		mouseDrag:false,
		nav: true,
		autoplay:false,
		smartSpeed : 1000,
		dots: false,
		items:1,
		loop: true,
		margin:20,
		rewindNav : false,
		pagination : false,

	});
	$(".knowing_nad_tab li a").click(function(e){
		if(!$(this).hasClass("active"))
		{
			var for_value = parseInt($(this).attr("for"));
			$(".knowing_nad_tab li a").removeClass("active");
			$(this).addClass("active");
			$(".knd").slideUp(200);
			$(".knowing_nad_tabs"+for_value).slideDown(200);
		}
	});

	/*6-1-2017*/
	$(".checkbox").find("input").trigger("click");
	$(".checkbox").find("input").trigger("click");
	$(".manadatory_star_wrap").find(".filter-option").addClass("active");



	/* 22 / 12 / 2016*/


	var $window = $(window), previousScrollTop = 0, scrollLock = false;

    $window.scroll(function(event) {
        if(scrollLock) {
            $window.scrollTop(previousScrollTop);
		}

        previousScrollTop = $window.scrollTop();
	});



	$(".dashboard-header, .header").click(function(e){
		if($(".nad_main_menu_wrap").css("left") == "0px")
		{
			 var container = $(".nad_main_menu_wrap");

			if (!container.is(e.target) // if the target of the click isn't the container...
				&& container.has(e.target).length === 0) // ... nor a descendant of the container
			{
				//scrollLock = !scrollLock;
				$(".dropdown_menu").slideUp();
				$(".nad_main_menu_wrap").css({"left":"-82%"});
				$(".menu_icon_ctrl").removeClass("active");
				$(".lower-menu ul li.dropdown a").removeClass("active");
				$(".black_overlay").removeClass("running"); $("body").removeClass("noscroll");
			}

		}
	});
	$(".menu_icon_ctrl").click(function(e){
		e.stopPropagation();
		//scrollLock = !scrollLock;
		$("body").toggleClass("noscroll");
		$(this).toggleClass("active");
		$(".black_overlay").toggleClass("running");
		if($(this).hasClass("active"))
		{
			$(".nad_main_menu_wrap").addClass('openMe');
			$(".nad_main_menu_wrap").css({"left":"0"});
		}
		else
		{
			$(".nad_main_menu_wrap").removeClass('openMe');
			$(".nad_main_menu_wrap").css({"left":"-82%"});
		}
	});

	$('.nad_main_menu_wrap').append('<span class="close-button-for-menu"></span>');

	$('.close-button-for-menu').on('click', function() {
		$(".menu_icon_ctrl").trigger('click');
	});

	$(".lower-menu ul li.dropdown ul.dropdown_menu li a, .black_overlay, .lower-menu ul li a.no_dropdown, .ai_dashboard_hamburger_wrapper ").click(function(e){
		$(".dropdown_menu").slideUp();
		 $(".nad_main_menu_wrap").css({"left":"-82%"});
		 $(".menu_icon_ctrl").removeClass("active");
		 $(".lower-menu ul li.dropdown a").removeClass("active");
	$(".black_overlay").removeClass("running");
	$("body").removeClass("noscroll");
	$(".nad_main_menu_wrap").removeClass('openMe');
});

	// $('.orange-bg, .dashboard_content_wrapper').on('click', function (event) {
	// 	$(".nad_main_menu_wrap").removeClass('openMe');
	// })

	$(".lower-menu ul li.dropdown a").click(function(e){
		if($(window).width() < 768)
		{
			$(this).toggleClass("active");
			if($(this).hasClass("active"))
			{
			    $(".dropdown_menu").slideUp();
				$(this).next(".dropdown_menu").slideDown();
			}
			else
			{

				$(this).next(".dropdown_menu").slideUp();
			}
		}
	});


	$(".lower-menu ul li.dropdown").hover(function(e){
		//$(this).find(".dropdown_menu").stop(true,true).fadeIn(400);
		},function(e){
		//$(this).find(".dropdown_menu").stop(true,true).hide(0);
	});


	/*21/12/2016*/
	$(".header-search").on("click",function(e){
		e.preventDefault();
		$(this).next(".searchBox").toggle(".searchBox");
	});
	$('body').on('click', function (e) {

		var container = $(this).find(".search_link");
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			$(".searchBox").hide(400);
		}
	});

	/*19/12/2016*/
	$(".xmlLabel input").change(function(e){
		var names = '';
	    for (var i = 0; i < $(this).get(0).files.length; ++i) {
	    	if(names=='')
	    		names= $(this).get(0).files[i].name;
	    	else
	    		names = names+","+$(this).get(0).files[i].name;
	    }
		if ( $('body').hasClass('a12_1_2') ) {
			$(this).parents().prev(".xml_file_value").html(names);
		} else {
			$(this).parents().siblings(".xml_file_value").html(names);
		}
	});
	/*16/12/2016*/
	$('#yesUpdate, #noUpdate').on('hidden.bs.modal', function (e) {
	   $("body").removeClass("modal-open");
	   $("body").css("paddingRight","0px");
	   $(this).css("paddingRight","0px");
	})
	var flag_pop = 1;
	$('#myModal').on('hidden.bs.modal', function (e) {
		if(flag_pop == 1)
		{
		$("body").addClass("modal-open");
		if($(window).width() > 768)
		{
			$(this).css("paddingRight","17px");
			$("body").css("paddingRight","17px");
		}
		else
		{
			$(this).css("paddingRight","0px");
			$("body").css("paddingRight","0px");
		}

	   }

	});
	$('#myModal').on('click', function (e) {

		var container = $(this).find(".modal-content");
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			flag_pop = 0;
			setTimeout(function(){
				$("body").css("paddingRight","0px");
		   $("body").removeClass("modal-open");
		   $(this).css("paddingRight","0px"); }, 2000);

		}
	});
	$('#myModal .modal-header .close').on('click', function (e) {
		  flag_pop = 0;
			setTimeout(function(){
				$("body").css("paddingRight","0px");
		   $("body").removeClass("modal-open");
		   $(this).css("paddingRight","0px"); }, 2000);
	});

	$('#myModal, #yesUpdate, #noUpdate').on('shown.bs.modal', function (e) {

	   $("body").addClass("modal-open");
	   if($(window).width() > 768)
	   {
		  $("body").css("paddingRight","17px");
		  $(this).css("paddingRight","17px");
	   }
	   else
	   {
		   $(this).css("paddingRight","0px");
		   $("body").css("paddingRight","0px");
	   }
	})

	$(".masterDataUpdateLink-main").on("click",function(e){
		$('#yesUpdate, #noUpdate').modal('hide');
	});
	$(".masterDataUpdateLink-yes").on("click",function(e){
		flag_pop = 1;
		$('#myModal, #noUpdate').modal('hide');
		$(this).hasClass("active");
	});
	$(".masterDataUpdateLink-no").on("click",function(e){
		flag_pop = 1;
		$('#myModal, #yesUpdate').modal('hide');
		$(this).hasClass("active");
		$("body").css("overflow-y","scroll");
	});

	/*14/13/216*/
	/* var dashboardContentHeight = $(".dashboard-content").innerHeight();
		var dashboardMenuHeight = $("ul.dashboard_menu").innerHeight();
		if(dashboardContentHeight > dashboardMenuHeight)
		{
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 150);

		}else{
		$(".dashboard_side_menu").css("height",dashboardMenuHeight + 150);
	} */




	/*12/13/2016*/
	$(".student_ai_link").hover(function(e){
		if($(window).width() > 992){ $(this).find(".ai_desc").fadeIn();}
		},function(){
		if($(window).width() > 992){ $(this).find(".ai_desc").fadeOut();}
	});
	$(".student_ai_link").click(function(e){
		if($(window).width() < 993){ $(this).find(".ai_desc").fadeToggle(); }
	});

	$("body").click(function(e){
		var container = $(".student_ai_link");
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			$(".ai_desc").fadeOut(0);
		}
	});

	$(".ad_hover").hover(function(e){
		$(this).find(".ms_default").fadeOut(100);
		$(this).find(".ms_active ").fadeIn(100);
		},function(e){
		$(this).find(".ms_default").fadeIn(100);
		$(this).find(".ms_active ").fadeOut(100);
	});


	$(".student_submit_button1").click(function(e){
		var flag = true;
		//$(".field-error").html("");
		var attr = $(this).attr('for');

		var searchInput = $(".students-acoounts-details-point12 .form-group.info-field > input");
		searchInput.removeClass('non-editable');

		if (typeof attr !== typeof undefined && attr !== false) {
			if(attr=="other_email_id"){
				if($("#other_email_id").val()==""){
					$("#other_email_id_error").show().css("display","block");
					$("#other_email_id_error").html("Email ID information is not filled.");
					flag = false;
					$("#mobile_number").addClass("error-focused");
				}
				else if((!isEmail($("#other_email_id").val())/* || $("#other_email_id").val().length<9*/)){
					$("#other_email_id_error").show().css("display","block");
					$("#other_email_id_error").html("Please check the email ID provided, it looks incorrect.");
					flag = false;
					$("#other_email_id").addClass("error-focused");
				}
				else{
					$("#other_email_id").removeClass("error-focused");
					$("#other_email_id_error").html("");
				}
			}
		}

		if(flag){
			$(".otp-email-message").hide();
			var searchInput = $(".students-acoounts-details-point12 .form-group.info-field > input");
			searchInput.addClass('non-editable');
			$(this).fadeOut(200);
			$(".student_account_details_edit_mail_btn").removeClass("active").fadeIn(200);
			$(".student_account_details_edit_mobile_btn").removeClass("active");
			$(".students-acoounts-details-point12").addClass("student-edit");
		}
	});

	// otp jquery
	$(".student_account_details_submit_btn").click(function(e){

		var flag = true;
		//$(".field-error").html("");
		var attr = $(this).attr('for');

		/*if ( $('body').hasClass('v5_0') ) {
			$(".student_account_details_edit_mobile_btn").removeClass("active").fadeIn(200);
			$(this).fadeOut('200');
			$('.student_cancel_button').fadeOut('200');
		}*/

		if (typeof attr !== typeof undefined && attr !== false) {
			if(attr=="mobile_number"){
			    var mobile_regx=/[0-9]/;
				if($("#mobile_number").val().length=="4"){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Mobile number information is not filled.");
					flag = false;
					$("#mobile_number").addClass("error-focused");
				}
				else if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15"){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
					flag = false;
					$("#mobile_number").addClass("error-focused");
				}
				else if($("#mobile_number").val()!="" &&  !mobile_regx.test($("#mobile_number").val())){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Please check the mobile number entered.");
					flag = false;
					$("#mobile_number").addClass("error-focused");
				}
				else{
					$("#mobile_number").removeClass("error-focused");
					$("#mobile_number_error").html("");
					var dashboardContentHeight = $(".dashboard-content").height();
					$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
					$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
					$(".otp-message").hide();
				}
			}
			if(attr=="bill_mobile_number"){
			    var mobile_regx=/[0-9]/;
				if($("#bill_mobile_number").val().length=="4"){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Mobile number information is not filled.");
					flag = false;
					$("#bill_mobile_number").addClass("error-focused");
				}
				else if($("#bill_mobile_number").val().length!="4" && $("#bill_mobile_number").val().length!="15"){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
					flag = false;
					$("#bill_mobile_number").addClass("error-focused");
				}
				else if($("#bill_mobile_number").val()!="" &&  !mobile_regx.test($("#bill_mobile_number").val())){
					$("#mobile_number_error").show().css("display","block");
					$("#mobile_number_error").html("Please check the mobile number entered.");
					flag = false;
					$("#bill_mobile_number").addClass("error-focused");
				}
				else{
					$("#bill_mobile_number").removeClass("error-focused");
					$("#mobile_number_error").html("");
					var dashboardContentHeight = $(".dashboard-content").height();
					$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
					$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
					$(".otp-message").hide();
				}
			}
		}

		if(flag){
			$(".students_accounts_details_otp").slideDown(400);
			var searchInput = $(".students-acoounts-details-point12 .form-group.info-field > input");
			searchInput.addClass('non-editable');
			$(".students-acoounts-details-point12, .students-acoounts-details-point11").addClass("student-edit");
			$(this).fadeOut(200);
			$(".otp-box input").focus();
			$(".student_account_details_edit_mobile_btn").removeClass("active").fadeIn(200);
			$(".student_account_details_edit_mail_btn").removeClass("active");

			/* height*/
			$("#master-data-form").removeClass("showHideMasterData");
			var dashboardContentHeight = $(".dashboard-content").height();
			$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
			$(".dashboard_side_menu").height(dashboardContentHeight + 250);
			$("#mobile_number").addClass('non-editable');
		}

	});

	$('#reason_for_sharing').on('change', function(){
		var str = $(this).val();
		if (str.search(/other/i) >= 0)
		{
			if($(window).width() < 768)
			{
				$(".rfs_field").slideDown();
			}
			else
			{
				$(".rfs_field").fadeIn();
			}
		}
		else
		{
			if($(window).width() < 768)
			{
				$(".rfs_field").slideUp();
			}
			{
				$(".rfs_field").fadeOut();
			}
		}
	});
	/* end of 12/13/2016*/

	// account details
	/*$(".ad_hover").hover(function(e){
		$(this).find(".ms_default").fadeOut(200);
		$(this).find(".ms_active ").fadeIn(200);
		},function(e){
		$(this).find(".ms_default").fadeIn(200);
		$(this).find(".ms_active ").fadeOut(200);
	});*/
	//put cursor at end
	jQuery.fn.putCursorAtEnd = function() {

		return this.each(function() {

			// Cache references
			var $el = $(this),
			el = this;

			// Only focus if input isn't already
			if (!$el.is(":focus")) {
				$el.focus();
			}

			// If this function exists... (IE 9+)
			if (el.setSelectionRange) {

				// Double the length because Opera is inconsistent about whether a carriage return is one character or two.
				var len = $el.val().length * 2;

				// Timeout seems to be required for Blink
				setTimeout(function() {
					el.setSelectionRange(len, len);
				}, 1);

				} else {

				// As a fallback, replace the contents with itself
				// Doesn't work in Chrome, but Chrome supports setSelectionRange
				$el.val($el.val());

			}

			// Scroll to the bottom, in case we're in a tall textarea
			// (Necessary for Firefox and Chrome)
			this.scrollTop = 999999;

		});

	};
	// students accounts details
	$(".student_cancel_button1").click(function(e){
		$(this).fadeOut(200);
		$(this).siblings(".student_submit_button1").fadeOut(200);
		$(".student_account_details_edit_mail_btn").fadeIn(200);
		$(".students-acoounts-details-point12").addClass("student-edit");
		var searchInput = $(".students-acoounts-details-point12 .form-group.info-field > input");
		searchInput.addClass('non-editable');
		searchInput.val(email_id);
		$("#mobile_number_error").html('');
		$(".otp-email-message").hide();
	});
	$(".student_cancel_button").click(function(e){
		$(this).fadeOut(200);
		$(".students-acoounts-details-otp-txt").fadeOut(200);
		$(this).siblings(".student_submit_button").fadeOut(200);
		$(".student_account_details_edit_mobile_btn").fadeIn(200);
		$(".students-acoounts-details-point11").addClass("student-edit");
		$("#mobile_number").val(mobile_number);
		var searchInput = $(".students-acoounts-details-point11 .form-group.info-field > input");
		searchInput.addClass('non-editable');
		searchInput.val(mobile_number);
		$(".otp-message").hide();
		$("#mobile_number_error").html('');
		$("#mobile_number").removeClass('error-focused');
	});
	$(".student_submit_button1").click(function(e){
		$(".student_cancel_button1").fadeOut(200);
	});
	$(".student_submit_button").click(function(e){
		$(".student_cancel_button").fadeOut(200);
	});

	$(".student_account_details_edit_mail_btn").click(function(e){
		$("#otpClone0").val("");
		$("#otp-message").text('');
		$("#otp-email-message").text('');
		$(".success-message").hide();
		$(".resend-msg").hide();
		$("#mobile_number").val(mobile_number);

		var searchInput = $(".students-acoounts-details-point12 .form-group.info-field > input");
		searchInput.focus();
		if ( searchInput.val() == '' ) {
			searchInput.val('abcxyz@email.com');
		}
		email_id = searchInput.val();
		if($(".students_accounts_details_otp").css("display") == "none")
		{
			$(".students-acoounts-details-otp-txt").fadeOut(200);
		}
		$(this).fadeOut(200);
		$(this).prev(".student_submit_button1").fadeIn(200);
		$(this).next(".student_cancel_button1").fadeIn(200);
		$(".otp-email-message").fadeIn(200);
		$(".otp-message").hide();
		$("#mobile_number_error").html('');
		$("#mobile_number").removeClass('error-focused');
		searchInput.caret(searchInput.val().length);
		$(".students-acoounts-details-point12").removeClass("student-edit");
		searchInput.removeClass('non-editable');
		$(this).addClass("active");
		$(this).find(".ms_default").fadeOut(200);
		$(this).find(".ms_active ").fadeIn(200);

		$(".students-acoounts-details-point11").addClass("student-edit");
		var searchInput = $(".students-acoounts-details-point11 .form-group.info-field > input");
			searchInput.addClass('non-editable');
		$(".student_account_details_edit_mobile_btn").fadeIn(200);
		$(".student_account_details_edit_mobile_btn").prev(".student_submit_button").fadeOut(200);
		$(".student_account_details_edit_mobile_btn").next(".student_cancel_button").fadeOut(200);

	});

	$(".student_account_details_edit_mobile_btn").click(function(e){
		$("#otpClone0").val("");
		$("#otp-message").text('');
		$("#otp-email-message").text('');
		$(".success-message").hide();
		$(".resend-msg").hide();

		var searchInput = $(".students-acoounts-details-point11 .form-group.info-field > input");
		searchInput.focus();
		mobile_number = searchInput.val();
		searchInput.removeClass('non-editable');
		$(this).parents().siblings().removeAttr('readonly');
		$(this).parents().siblings().focus();
		$(".students-acoounts-details-otp-txt").fadeIn(200);
		$(this).fadeOut(200);

		$(".students_accounts_details_otp").slideUp(200);
		$(".otp-message").fadeIn(200);
		$(".otp-email-message").hide();
		if(email_id=="")
			email_id = $("#other_email_id").val();
		$("#other_email_id").val(email_id);
		$("#other_email_id_error").html('');
		$("#other_email_id").removeClass('error-focused');
		$(this).prev(".student_submit_button").fadeIn(200);
		$(this).next(".student_cancel_button").fadeIn(200);
		searchInput.caret(searchInput.val().length);
		$(".students-acoounts-details-point11").removeClass("student-edit");
		searchInput.removeClass('non-editable');
		$(".student_account_details_mobile").attr("id","mobile_number");
		$(this).addClass("active");
		$(".students-acoounts-details-point12").addClass("student-edit");
		var searchInput = $(".students-acoounts-details-point12 .form-group.info-field > input");
		searchInput.addClass('non-editable');
		$(".student_account_details_edit_mail_btn").fadeIn(200);
		$(".student_account_details_edit_mail_btn").prev(".student_submit_button1").fadeOut(200);
		$(".student_account_details_edit_mail_btn").next(".student_cancel_button1").fadeOut(200);

		invokeMobileValidation();



	});

	// my certificates
	$(".dashboard_responsive_table  table tr td.university_box_trigger").hover(function(e){
		var a22 = $(".dashboard_responsive_table table tbody tr td:nth-child(1)").innerWidth();
		var a33 = $(".dashboard_responsive_table table tbody tr td:nth-child(2)").width();
		var t44 =  $(".dashboard_responsive_table table tbody tr td:nth-child(2)").innerHeight()/2;
		var t55 =  $(".dashboard_responsive_table table tbody tr td:nth-child(2)").innerHeight();
		var thh =  $(".dashboard_responsive_table table thead tr th").innerHeight();
		var len = $(".dashboard_responsive_table  table tbody tr").length;
		var ind = $(this).parent("tr").index();
		/*if(len == ind+1)
		{	*/


		$(".university_box").css({"left":a22+a33,"top":20+thh+t44+(t55*(ind))});
		/*}
			else
			{
			$(this).parent("tr").css("position","relative");
			$(".university_box").css({"left":a22+a33,"top":'20px'});
		}*/

		$(this).find(".university_box").fadeIn(300);
		}, function(e){
		$(this).find(".university_box").fadeOut(0);
	});

	var position_box, position_box_value, a33 , p44;

	$("body").click(function(e){
		var container = $(".dashboard_responsive_table  table tr td.university_box_trigger");
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			container.removeClass("active1");
			$(".university_box").removeClass("active").fadeOut(0);
		}
	});

	$(".dashboard_responsive_table  table tr td.university_box_trigger").click(function(e){
		if($(this).hasClass("active1"))
		{
			$(this).find(".university_box").removeClass("active").fadeOut(0);
			$(this).removeClass("active1");
		}
		else
		{
			position_box = $(this);
			position_box_value = position_box.position();
			//alert( "left: " + position.left + ", top: " + position.top );
			p44 = $(".dashboard_responsive_table").scrollLeft();
			var a22 = $(".dashboard_responsive_table table tbody tr td:nth-child(1)").innerWidth();
			a33  = $(".dashboard_responsive_table table tbody tr td:nth-child(2)").width();
			var t44 =  $(".dashboard_responsive_table table tbody tr td:nth-child(2)").innerHeight()/2;
			var t55 =  $(".dashboard_responsive_table table tbody tr td:nth-child(2)").innerHeight();
			var thh =  $(".dashboard_responsive_table table thead tr th").innerHeight();
			var ind = $(this).parent("tr").index();
			$(".university_box").css({"left":position_box_value.left+a33,"top":10+thh+t44+(t55*(ind))});
			$(".dashboard_responsive_table  table tr td.university_box_trigger").removeClass("active1");
			if($(".university_box").hasClass("active"))
			{
				$(".university_box").fadeOut(0);
				$(".university_box").removeClass("active");
			}
			$(this).addClass("active1");
			$(this).find(".university_box").addClass("active").fadeIn(200);
		}

	});
	$(".dashboard_responsive_table").scroll(function() {
		var p33 = $(this).scrollLeft();
	    $(".university_box").css({"left":position_box_value.left+a33+p44-p33});
	});



	$(".steptabOne").css("display","block");
	if($(window).width() < 768)
	{
		$(".dashboard_side_menu .link_title").css("display","none");
	}



	function invokeMobileValidation()
	{
		if(typeof selected == 'undefined'){
			var selected = "Indian";
		}
		var scroll = [];
		var mobile_regx=/[0-9]/;
		if($("#mobile_number").val().length=="4" && selected=="Indian"){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Mobile number information is not filled.");
			flag = false;
		}
		if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15" && selected=="Indian"){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			flag = false;
		}
		if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15"){
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			scroll.push('mobile_number');
			flag = false;
		}
		if(!mobile_regx.test($("#mobile_number").val())){
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number entered.");
			scroll.push('mobile_number');
			flag = false;
		}
		$("#mobile_number").keydown(function(e) {
			var selected = $(".nationality:checked").val();
			if(selected=="Foreigner"){
			}
			else{
				var oldvalue=$(this).val();
				var field=this;
				/*setTimeout(function () {
					if(field.value.indexOf('+91 ') !== 0) {
						$(field).val(oldvalue);
					}
				}, 1);*/
				if(field.value.indexOf('+91 ') !== 0) {
					if(oldvalue=="+91")
						oldvalue="+91 ";
					$("#mobile_number").val(oldvalue);
				}
			}
		});
		$("#mobile_number").keyup(function(e) {

			var get_focus =f1(document.getElementById('mobile_number'));
			var oldvalue = $(this).val().replace(/\s/g, '') ;
			var len = $(this).val().length;

			var selected = $(".nationality:checked").val();
			if(selected=="Foreigner"){
				/*if(get_focus==11 && e.keyCode=="8"){
					var str1 = oldvalue.substring(0, 5);
					var str2 = oldvalue.substring(5, 9)+oldvalue.substring(10,11);
					var str3 = oldvalue.substring(11);
					}
					else*/ if(get_focus==5 && e.keyCode=="8"){
					get_focus--;
					var str1 = oldvalue.substring(0, 4)+oldvalue.substring(5,6);
					var str2 = oldvalue.substring(6, 11);
					var str3 = oldvalue.substring(11);
				}
				else{
					var str1 = oldvalue.substring(0, 5);
					var str2 = oldvalue.substring(5, 10);
					var str3 = oldvalue.substring(10);
				}
				/*if(str3!=""){
					if(get_focus!=12)
					get_focus++;
					$(this).val(str1+" "+str2+" "+str3);
					}
					else*/ if(str2!=""){
					/*get_focus++;
						if(get_focus==5){
						get_focus++;
						get_focus++;
					}*/
					$(this).val(str1+" "+str2);
				}
				if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
					setCaretPosition('mobile_number', get_focus)
				}
			}
			else{
				if(get_focus==9 && e.keyCode=="8"){
					get_focus--;
					var str1 = oldvalue.substring(0, 3);
					var str2 = oldvalue.substring(3, 7)+oldvalue.substring(8,9);
					var str3 = oldvalue.substring(9);
				}
				else{
					var str1 = oldvalue.substring(0, 3);
					var str2 = oldvalue.substring(3, 8);
					var str3 = oldvalue.substring(8);
				}

				if(str3!=""){
					if(str3.length>5){
		                str3 = str3.slice(0, -1);
		            }
					$(this).val(str1+" "+str2+" "+str3);
				}
				else if(str2!=""){
					$(this).val(str1+" "+str2);
				}
				if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
					setCaretPosition('mobile_number', get_focus)
				}
			}
		});
	}


	// dashboard menu 24/12/2016
	$(".dashboard_hamburger").click(function(e){
		if($("body").hasClass("big-dash-m"))
		{
			$("body").removeClass("big-dash-m").addClass("small-dash-m");
			if($(window).width() > 767){ $(".dashboard_side_menu .link_title").fadeOut(0);}
			else { var a = $(".dashboard_side_menu").innerWidth(); $(".dashboard-content").fadeOut(200); $(".dashboard-content").css("position","absolute"); $(".dashboard_side_menu .link_title, .dashboard_btn_close").fadeIn(200); $(".black_overlay1").addClass("running");}
		}
		else{

			$("body").removeClass("small-dash-m").addClass("big-dash-m");
			if($(window).width() > 767){ $(".dashboard_side_menu .link_title").fadeIn(400);}
			else {$(".dashboard-content").fadeIn(200);  $(".dashboard-content").css("position","static");   $(".dashboard_side_menu .link_title, .dashboard_btn_close").fadeOut(0);  $(".black_overlay1").removeClass("running");}
		}
	});


	//resize
	$(window).resize(function(e){
		if($(window).width() > 767)
		{
			$(".dashboard-content").css("paddingTop","40px")
		}
		else
		{
			$(".dashboard-content").css("paddingTop","30px");
			$(".student-landing-banner-menu").css("position","relative");
		}
		dashboard_paddTop = parseInt($(".dashboard-content").css("paddingTop"),10);
	});





	//nano scroller
	/* $(".nano").nanoScroller({ sliderMaxHeight: 200 }); */

	$(".dashboard_menu li a.dashboard-dropdown").on("click",function(e){
		if(!($(this).hasClass("turn")))
		{
			$(".dashboard_menu li ul.dashboard-dropdown-menu").slideUp();
			$(".dashboard_menu li a.dashboard-dropdown").removeClass("turn");
		}
		$(this).toggleClass("turn");
		$(this).next(".dashboard_menu li ul.dashboard-dropdown-menu").slideToggle(200);
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").removeClass("active");
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").closest("li").removeClass("super_active");
		$(".dashboard_menu li ul.dashboard-dropdown-menu li:first-child a").addClass("active");
		$(".dashboard_menu li ul.dashboard-dropdown-menu li:first-child a").closest("li").addClass("super_active");
	});
	$(".dashboard_menu li a.no-dashboard-dropdown").click(function(e){
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").removeClass("active");
		$(this).addClass("active");
		$(".dashboard_menu li ul.dashboard-dropdown-menu").slideUp(200);
		$(".dashboard_menu li a.dashboard-dropdown").removeClass("turn");
		if($(window).width() < 768)
		{
			$("body").removeClass("small-dash-m").addClass("big-dash-m");
			$(".dashboard_side_menu .link_title").fadeOut(0);
			$(".dashboard-content").fadeIn(200);  $(".dashboard-content").css("position","static");
		}
		$(".black_overlay, .black_overlay1 ").removeClass("running");
	});



	$(".dashboard_menu li ul.dashboard-dropdown-menu li a").click(function(e){
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").removeClass("active");
		$(".dashboard_menu li a, .dashboard_menu li ul.dashboard-dropdown-menu li a").closest("li").removeClass("super_active");
		$(this).addClass("active");
		$(this).closest("li").addClass("super_active");
		if($(window).width() < 768)
		{
			$("body").removeClass("small-dash-m").addClass("big-dash-m");
			$(".dashboard_side_menu .link_title").fadeOut(0);
			$(".dashboard-content").fadeIn(200);  $(".dashboard-content").css("position","static");
		}
		$(".black_overlay, .black_overlay1").removeClass("running");
	});
	/*24/12/2016*/
	$(".dashboard_btn_close, .black_overlay1, .dashboard-navbar, .dashboard_wrapper, .dashboard-footer, .student-landing-banner-menu-wrap ").click(function(e){
		if($(window).width() < 768)
		{
			$(".dashboard-content").css("position","static");
			$(".dashboard-content").fadeIn(200);
			$("body").removeClass("small-dash-m").addClass("big-dash-m");
			$(".black_overlay1").removeClass("running");
			$(".dashboard_side_menu .link_title").fadeOut(0);
		}
	});



	function  dashboardMenuTopValue()
	{
		var dashboard_navbar = parseInt($(".dashboard-navbar").innerHeight(),10);;
		var dashboard_wrapper = parseInt($(".dashboard_wrapper").innerHeight(),10);
		var dashboard_banner_menu = parseInt($(".student-landing-banner-menu").innerHeight(),10);
		var dashboard_menu_top_value = dashboard_navbar + dashboard_wrapper + dashboard_banner_menu;
		return dashboard_menu_top_value;
	}
	function dashboarBannerHieght()
	{
		var dashboard_banner_menu = parseInt($(".student-landing-banner-menu").innerHeight(),10);
		return dashboard_banner_menu;
	}
	function studentImageTop()
	{
		var a = dashboardMenuTopValue() -  parseInt($(".student-landing-banner-menu").innerHeight(),10);;
		return a;
	}
	function mobilestudentImageTop()
	{
		var a = dashboardMenuTopValue();
		return a;
	}

	function scrollFixed(a)
	{
		$('html, body').stop().animate({
			scrollTop: $(a).offset().top+2
		},500);
	}

	/*end vaibhav js*/
});




function dateDisplay() {
	console.log("werwe")
	$('input[name="datefilter"]').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
	},
	function(start, end, label) {
		var years = moment().diff(start, 'years');
		/* alert("You are " + years + " years old."); */
	});
};

function dateMasterDisplay(inputName) {
	$("#"+inputName).daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		yearRange: '1972:2011'
	},
	function(start, end, label) {
		var years = moment().diff(start, 'years');
		/* alert("You are " + years + " years old."); */
	});
};


$(document).ready(function(){
	$("#affiliated_institute").change(function(event) {
		if($(this).val()=="Yes"){
			$("#college_div").show();
		}
		else{
			$("#college_div").hide();
		}
	});
	$("#affiliated_institute").change();
	$(".share_certificate_selection").click(function(e){
		$(".field-error").hide();
		var flag = true;
		var scroll = [];

		var check_flag = false;
		var values = $('input:checkbox:checked.certificate_checkbox').map(function () {
		  check_flag = true;
		}).get();
		if(!check_flag){
			flag = false;
			scroll.push('certificate_checkbox_table');
			$("#certificate_checkbox_error").html("Please select atleast one certificate.").show();
		}

		if($("#type_of_verifier").val()==""){
			scroll.push('type_of_verifier');
			$("#type_of_verifier_error").html("Type of verifier information is not filled.").show();
			flag = false;
		}

		if($("#verifier_name").val()==""){
			scroll.push('type_of_verifier');
			$("#verifier_name_error").html("Verifier name information is not filled.").show();
			flag = false;
		}

		if($("#reason_for_sharing").val()==""){
			scroll.push('reason_for_sharing');
			$("#reason_for_sharing_error").html("Reason for sharing information is not filled.").show();
			flag = false;
		}

		if($("#reason").val()=="" && $("#reason_for_sharing").val()=="Other Reasons"){
			scroll.push('reason');
			$("#reason_error").html("Reason for sharing information is not filled.").show();
			flag = false;
		}

		if($("#days").val()==""){
			scroll.push('days');
			$("#days_error").html("Period for allowing access (In Days) information is not filled.").show();
			flag = false;
		}
		scroll_error(scroll);
		return flag;
	});


	$(".auth_transaction_order").click(function(e){
		$(".field-error").hide();
		var flag = true;
		var scroll = [];
		if($("#address1").val()==""){
			scroll.push('address1');
			$("#address1_error").show();
			$("#address1_error").html("Address 1 information is not filled.");
			flag = false;
		}
		if($("#address2").val()==""){
			scroll.push('address2');
			$("#address2_error").show();
			$("#address2_error").html("Address 2 information is not filled.");
			flag = false;
		}
		if($("#city").val()==""){
			scroll.push('city');
			$("#city_error").show();
			$("#city_error").html("City /Town /Village information is not filled.");
			flag = false;
		}
		if($("#state").val()==""){
			scroll.push('state');
			$("#state_error").show();
			$("#state_error").html("State information is not filled.");
			flag = false;
		}
		if($("#auth_pincode").val()==""){
			scroll.push('auth_pincode');
			$("#pincode_error").show();
			$("#pincode_error").html("Pincode information is not filled.");
			flag = false;
		}
		else if($("#auth_pincode").val().length!=6){
			scroll.push('auth_pincode');
			$("#pincode_error").show();
			$("#pincode_error").html("Please enter valid pincode.");
			flag = false;
		}
		if($("#cont").val()==""){
			scroll.push('cont');
			$("#cont_error").show();
			$("#cont_error").html("Country information is not filled.");
			flag = false;
		}
		scroll_error(scroll);
		if(!flag){
			$("#master-data-form").removeClass("showHideMasterData");
			var dashboardContentHeight = $(".dashboard-content").height();
			$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
			$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		}
		return flag;
	});

	$(".auth-selection-certficate").click(function(e){
		$(".field-error").hide();
		var flag = false;
		var scroll = [];
		var values = $('input:checkbox:checked.certificate_checkbox').map(function () {
		  flag = true;
		}).get();
		if(!flag){
			$("#certificate_checkbox_error").html("Please select atleast one certificate.").show();
			scroll.push('certificate_checkbox_table');
		}
		scroll_error(scroll);
		return flag;
	});

	$(".DOB").focus(function(){
		dateDisplay();
		$(this).addClass("focusedField");
	}).blur(function(){
		$(".DOB").removeClass("focusedField");
	});

	$("#to_date").focus(function(){
		dateMasterDisplay('to_date');
	});
	$("#from_date").focus(function(){
		dateMasterDisplay('from_date');
	});

	$(".date_img").click(function(event){
		dateMasterDisplay($(this).attr("date_id"));
		$('#'+$(this).attr("date_id")).data('daterangepicker').toggle();
		$(".DOB").removeClass("focusedField");
		$(this).siblings(".DOB").addClass("focusedField");
	});


	/*$("#forgotUserID").change(function(){
		$("#forgot_user_id").val("");
		if($(this).val()=="addharId"){
		$("#forgot_user_id").attr("onkeypress","return IsNumeric(event);");
		$("#forgot_user_id").attr("text","tel");
		$("#forgot_user_id").removeClass("text-capitalize");
		$("#forgot_user_id").focus();
		}
		else{
		$("#forgot_user_id").attr("onkeypress","return nadIDValidation(event);");
		$("#forgot_user_id").attr("text","text");
		$("#forgot_user_id").addClass("text-capitalize");
		$("#forgot_user_id").focus();
		}
		});
	$("#forgotUserID").change();*/

	var selected = $(".nationality:checked").val();
	if(selected=="Foreigner"){
		//$('#country').attr('readonly', false);
		var state_html = '<label class="control-label">State </label>';
		state_html+='<input id="state" maxlength="30" type="text" class="text-capitalize form-control maxlenghtValidation" data-toggle="tooltip" data-placement="bottom" title="Enter State.">';
		state_html+='<span id="state_error" class="field-error">Error goes here!</span>';
		$("#stateDiv").html(state_html);
		$("#country").removeClass("non-editable");
		$("#mobile_number").val("");
		$("#mobile_number").attr("maxlength","11");
		$("#pincode").removeAttr("maxlength");
		$("#pincode").removeAttr("onkeypress");
		$("#city").removeAttr("onkeypress");
		$("#pincode").removeAttr("title");
		$("#pincode").removeAttr("data-original-title");
		//$("#pincode").attr("onkeypress","return isAlphaNumeric(event);");
	}
	else{
		///$('#country').attr('readonly', true);
		var state_html = '<select id="state" class="selectpicker custom-select state-select" >';
		state_html+='<option value="">State</option>';
		state_html+='<option value="Maharashtra">Maharashtra</option>';
		state_html+='<option value="Goa">Goa</option>';
		state_html+='</select>';
		state_html+='<span id="state_error" class="field-error">Error goes here!</span>';
		$("#country").val("India").focus();
		$("#country").blur();
		$("#stateDiv").html(state_html);
		$("#country").addClass("non-editable");
		$("#mobile_number").val("+91 ");
		$("#mobile_number").attr("maxlength","15");
		$("#pincode").attr("maxlength","6");
		$("#pincode").attr("title","Please enter Pin code");
		$("#pincode").attr("data-original-title","Please enter Pin code");
		$("#pincode").attr("onkeypress","return IsNumeric(event);");
		$("#city").attr("onkeypress","return isAlphaSpace(event);");
		$('.selectpicker').selectpicker();
	}

	$(".nationality").change(function(e){
		$("#pincode").val("");
		if($(this).val()=="Indian"){
			//$('#country').attr('readonly', true).removeClass("form-control");
			var state_html = '<select id="state" class="selectpicker custom-select state-select" >';
			state_html+='<option value="">State <i class="mandatory">*</i></option>';
			state_html+='<option value="Maharashtra">Maharashtra</option>';
			state_html+='<option value="Goa">Goa</option>';
			state_html+='</select>';
			state_html+='<span id="state_error" class="field-error">Error goes here!</span>';

			$("#stateDiv").html(state_html);
			$("#country").val("India").focus();
			$("#country").addClass("non-editable");
			$("#mobile_number").val("+91 ").focus();
			$("#mobile_number").attr("maxlength","15");
			$("#pincode").attr("maxlength","6");
			$("#pincode").attr("onkeypress","return IsNumeric(event);");
			$("#city").attr("onkeypress","return isAlphaSpace(event);");
			$('.selectpicker').selectpicker();
			$("#pincode").attr("title","Enter your City/Town/Village Name.");
			$("#pincode").attr("data-original-title","Enter your City/Town/Village Name.");
			$("#country").blur();
		}
		else{
			var state_html = '<label class="control-label">State</label>';
			state_html+='<input id="state" maxlength="30" type="text" class="text-capitalize form-control maxlenghtValidation" data-toggle="tooltip" data-placement="bottom" title="Enter State.">';
			state_html+='<span id="state_error" class="field-error">Error goes here!</span>';
			$("#stateDiv").html(state_html);

			$("#country").removeClass("non-editable");
			//$('#country').attr('readonly', false);
			$("#country").val("").blur();
			$("#mobile_number").val("").blur();
			$("#mobile_number").attr("maxlength","11");
			//$("#pincode").attr("maxlength","7");
			//$("#pincode").attr("onkeypress","return isAlphaNumeric(event);");
			$("#pincode").removeAttr("maxlength");
			$("#pincode").removeAttr("onkeypress");
			$("#city").removeAttr("onkeypress");
			$("#pincode").removeAttr("title");
			$("#pincode").removeAttr("data-original-title");
		}
	});

	$(".stud_details_submit").click(function(e){
		var regx=/[0-9]/;
		$("#otp_error").html("");
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().show();
			$("#otpClone0").addClass("error-focused");
			return false;
		}
		else if($("#otp").val().length!="6" || !regx.test($("#otp").val())){
			$("#otp_error").html("Please enter valid OTP.").fadeIn().show();
			$("#otpClone0").addClass("error-focused");
			return false;
		}
		else{
			//$("#otp").val("");
			mobile_number = $("#mobile_number").val();
			var searchInput = $(".students-acoounts-details-point11 .form-group.info-field > input");
			searchInput.addClass('non-editable');
			$("#otpClone0").removeClass("error-focused");
			$(".students_accounts_details_otp").hide();
			$(".students-acoounts-details-otp-txt").hide();
			$(".success-message").fadeIn();
			return true;
		}
	});

	$(".subs-submit-btn").click(function(e){
		var	flag = true;
		$("#subs-email-success").hide().css("display","none");
		$(".field-error").hide();
		if($("#subs-email").val()==""){
			$("#subs-email-error").show().css("display","block");
			$("#subs-email-error").html("Email ID information is not filled.");
			flag = false;
		}
		else if(!isEmail($("#subs-email").val()) || $("#subs-email").val().length<9){
			$("#subs-email-error").show().css("display","block");
			$("#subs-email-error").html("Please check the email ID provided, it looks incorrect.");
			flag = false;
		}
		else{
			$("#subs-email-error").hide().css("display","none");
			$("#subs-email-success").show().css("display","block");
			$("#subs-email-error").html("");
			flag = false;
		}
		return flag;
	});

	$(".validate-btn").click(function(e){
		$(".field-error").hide();
		var flag = true;
		if($("#nad_id").val()==""){
			flag = false;
			$("#nad_id_error").html("NAD ID information is not filled.").show();
		}
		else if($("#nad_id").val().length!="14"){
			flag = false;
			$("#nad_id_error").html("Please enter valid NAD ID.").show();
		}
		else{
			var str = $("#nad_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag = false;
					$("#nad_id_error").html("Please enter valid NAD ID1.").show();
				}
			}
			else{
				flag = false;
				$("#nad_id_error").html("Please enter valid NAD ID2.").show();
			}
		}
		if(flag){
			$("#nad_id").addClass("non-editable");
			$("#id-based-otp").show();
		}
		return false;
	});

	$(document).on('keydown','.disbleInput',function(e){
		//$(".disbleInput").keydown(function(){
		return false;
	});

	$(".verify-nad-id").click(function(e){
		var regx=/[0-9]/;
		$(".field-error").hide();
		if($("#nad_otp").val()==""){
			$("#nad_otp_error").html("Please enter OTP.").fadeIn().show();
			return false;
		}
		else if($("#nad_otp").val().length!="6" || !regx.test($("#nad_otp").val())){
			$("#nad_otp_error").html("Please enter valid OTP.").fadeIn().show();
			return false;
		}
		return true;
	});

	$("#email_id").blur(function(){
		if($("#email_id").val()!=""){
			if(isEmail($("#email_id").val())){
				if($("#user_id").val()==""){
					$("#user_id").val($("#email_id").val());
					$("#user_id").parent('div').addClass('focused');
				}
			}
		}
	});

	$(".nad-reg-submit").click(function(e){
		$(".field-error").hide();
		var flag = true;
		var regx = /^[A-Za-z]+$/;
		var mobile_regx=/[0-9]/;
		var scroll = [];
		if($("#first_name").val()==""){
			scroll.push('first_name');
			$("#first_name_error").show();
			$("#first_name_error").html("First Name information is not filled.");
			flag = false;
		}
		else if($("#first_name").val().length<3){
			scroll.push('first_name');
			$("#first_name_error").show();
			$("#first_name_error").html("First Name length should be minimum 3.");
			flag = false;
		}
		else if($("#first_name").val().length>25){
			scroll.push('first_name');
			$("#first_name_error").show();
			$("#first_name_error").html("First Name length should be maximum 25");
			flag = false;
		}

		/*if($("#middle_name").val()==""){
			$("#middle_name_error").show();
			$("#middle_name_error").html("Middle Name information is not filled.");
			flag = false;
			}
			else if($("#middle_name").val().length>25){
			$("#middle_name_error").show();
			$("#middle_name_error").html("Middle Name length should be maximum 25");
			flag = false;
		}*/

		if($("#last_name").val()==""){
			scroll.push('last_name');
			$("#last_name_error").show();
			$("#last_name_error").html("Last Name information is not filled.");
			flag = false;
		}
		else if($("#last_name").val().length>25){
			scroll.push('last_name');
			$("#last_name_error").show();
			$("#last_name_error").html("Last Name length should be maximum 25");
			flag = false;
		}

		if($("#father_name").val()==""){
			scroll.push('father_name');
			$("#father_name_error").show();
			$("#father_name_error").html("Father’s name information is not filled.");
			flag = false;
			}else if(!regx.test($("#father_name").val())){
			$("#father_name_error").show();
			$("#father_name_error").html("Please enter only alphabets.");
			flag = false;
		}
		if($("#dob").val()==""){
			scroll.push('dob');
			$("#dob_error").show();
			$("#dob_error").html("Date of Birth information is not filled.");
			flag = false;
		}
		else{
			var age = getAge($("#dob").val());
			if(parseInt(age)<parseInt(10) || parseInt(age)>parseInt(75)){
				$("#dob_error").show();
				$("#dob_error").html("Please check the date provided. Registration open for age between 10 years and 75 years.");
				flag = false;
				scroll.push('dob');
			}
		}
		if($("#gender").val()==""){
			$("#gender_error").show().css("display","block");
			$("#gender_error").html("Gender information is not filled.");
			flag = false;
			scroll.push('gender');

		}
		if($("#address1").val()==""){
			scroll.push('address1');
			$("#address1_error").show();
			$("#address1_error").html("Address 1 information is not filled.");
			flag = false;
		}
		if($("#address2").val()==""){
			scroll.push('address2');
			$("#address2_error").show();
			$("#address2_error").html("Address 2 information is not filled.");
			flag = false;
		}
		if($("#city").val()==""){
			scroll.push('city');
			$("#city_error").show();
			$("#city_error").html("City /Town /Village information is not filled.");
			flag = false;
		}
		if($("#state").val()=="" && $("#country").val()=="India"){
			scroll.push('state');
			$("#state_error").show();
			$("#state_error").html("State information is not filled.");
			flag = false;
		}
		if($("#pincode").val()=="" /*&& $("#country").val()=="India"*/){
			scroll.push('pincode');
			$("#pincode_error").show();
			$("#pincode_error").html("Pincode information is not filled.");
			flag = false;
		}
		else{
			if($("#pincode").val()!="" && $("#country").val()=="India"){

				if($("#pincode").val().length!="6"){
					$("#pincode_error").show();
					$("#pincode_error").html("Please enter valid Pincode.");
					scroll.push('pincode');
					flag = false;
				}
			}
		}
		if($("#country").val()==""){
			scroll.push('country');
			$("#country_error").show();
			$("#country_error").html("Country information is not filled.");
			flag = false;
		}
		if($("#browsImg").val()==""){
			scroll.push('browsImg_error');
			$("#browsImg_error").show().css("display","block");
			$("#browsImg_error").html("Please upload photo.");
			flag = false;
		}
		else{
			var image_size = $("#browsImg")[0].files[0].size;
			var image_extension = $('#browsImg').val().split('.').pop().toLowerCase();
			if($.inArray(image_extension, ['jpeg', 'tiff', 'jpg']) == -1) {
				flag = false;
				scroll.push('browsImg_error');
				$("#browsImg_error").show().css("display","block");
				$("#browsImg_error").html("Only TIFF,JPG,JPEG allowed!");
			}
			else{
				if(image_size>15360){
					validflag = false;
					scroll.push('browsImg_error');
					$("#browsImg_error").show().css("display","block");
					$("#browsImg_error").html('File size should be less than 15kb');
				}
			}
		}
		if($("#browsSig").val()==""){
			$("#browsSig_error").show();
			$("#browsSig_error").html("Please upload signature.");
			flag = false;
			scroll.push('browsSig');
		}
		else{
			var image_size = $("#browsSig")[0].files[0].size;
			var image_extension = $('#browsSig').val().split('.').pop().toLowerCase();
			if($.inArray(image_extension, ['jpeg', 'tiff', 'jpg']) == -1) {
				flag = false;
				scroll.push('browsSig');
				$("#browsSig_error").show();
				$("#browsSig_error").html("Only TIFF,JPG,JPEG allowed!");
			}
			else{
				if(image_size>10240){
					validflag = false;
					scroll.push('browsSig');
					$("#browsSig_error").show();
					$("#browsSig_error").html('File size should be less than 10kb');
				}
			}
		}

		var selected = $(".nationality:checked").val();
		if($("#mobile_number").val().length=="4" && selected=="Indian"){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Mobile number information is not filled.");
			flag = false;
		}
		if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15" && selected=="Indian"){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			flag = false;
		}
		if($("#mobile_number").val()!="" &&  !mobile_regx.test($("#mobile_number").val())){
			scroll.push('mobile_number');
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number entered.");
			flag = false;
		}
		if($("#name_of_university").val()==""){
			$("#name_of_university_error").show();
			$("#name_of_university_error").html("Name of University information is not filled.");
			flag = false;
			scroll.push('name_of_university');
		}
		if($("#course").val()==""){
			$("#course_error").show();
			$("#course_error").html("Course information is not filled.");
			flag = false;
			scroll.push('course');
		}
		if($("#year_of_passing").val()==""){
			$("#year_of_passing_error").show();
			$("#year_of_passing_error").html("Year of Passing information is not filled.");
			flag = false;
			scroll.push('year_of_passing');
		}
		if($("#unique_ref_no").val()==""){
			$("#unique_ref_no_error").show();
			$("#unique_ref_no_error").html("Unique Ref No. information is not filled.");
			flag = false;
			scroll.push('unique_ref_no');
		}

		if($("#user_id").val()==""){
			$("#user_id_error").show();
			$("#user_id_error").html("User ID information is not filled.");
			flag = false;
			scroll.push('user_id');
		}
		else if($("#user_id").val().length<6 || $("#user_id").val().length>30){
			$("#user_id_error").show();
			$("#user_id_error").html("Please enter valid user ID.");
			flag = false;
			scroll.push('user_id');
		}
		else{
			var regx = /^[A-Za-z0-9]+$/;
			if(!isEmail($("#user_id").val())){
				if(!regx.test($("#user_id").val())){
					$("#user_id_error").show();
					$("#user_id_error").html("User ID should contain only alphanumeric.");
					flag = false;
					scroll.push('user_id');
				}
			}
		}

		if($("#user_password").val()==""){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password information is not filled.");
			flag = false;
			scroll.push('user_password');
		}
		else {
			var regx = /^[A-Za-z0-9]+$/;
			if($("#user_password").val().length<8){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be minimum 8.");
				flag = false;
				scroll.push('user_password');
			}
			else if($("#user_password").val().length>20){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be maximum 20");
				flag = false;
				scroll.push('user_password');
			}
			else if(!regx.test($("#user_password").val())){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password should contain only alphanumeric.");
				flag = false;
				scroll.push('user_password');
			}
		}

		if($("#confirm_password").val()==""){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Confirm Password information is not filled.");
			flag = false;
			scroll.push('confirm_password');
		}

		if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
			if($("#user_password").val()!=$("#confirm_password").val()){
				$("#confirm_password_error").show();
				$("#confirm_password_error").html("Hey the confirm password value does not match.. Enter the same password and yes do remember it");
				flag = false;
				scroll.push('confirm_password');
			}
		}

		if($("#captch_code").val()==""){
			$("#captch_code_error").html("Please enter captcha code.").fadeIn().css("display","block");
			scroll.push('captch_code');
			flag = false;
		}
		else if(!regx.test($("#captch_code").val())){
			$("#captch_code_error").html("Invalid captcha code.").fadeIn().css("display","block");
			scroll.push('captch_code');
			flag = false;
		}

		if($("#email_id").val()==""){
			$("#email_id_error").show().css("display","block");
			$("#email_id_error").html("Email ID information is not filled.");
			flag = false;
			scroll.push('email_id');
		}
		else if(!isEmail($("#email_id").val()) || $("#email_id").val().length<9){
			$("#email_id_error").show().css("display","block");
			$("#email_id_error").html("Please check the email ID provided, it looks incorrect.");
			flag = false;
			scroll.push('email_id');
		}

		if(!document.getElementById('checkboxInput').checked){
			$("#term_error").show().css({"display":"block","clear":"both"});
			$("#term_error").html("You must agree terms and conditions before registration.");
			flag = false;
		}
		scroll_error(scroll);
		return flag;
	});

	$(".maxlenghtValidation").keyup(function(e) {
		var max = $(this).attr("maxlength");
		if ($(this).val().length > max) {
			$(this).val($(this).val().substr(0, max));
		}
	});

	$(".firstLatterCappital").keyup(function(e) {
		var string = $(this).val();
		$(this).val(string.charAt(0).toUpperCase() + string.slice(1));
	});

	$(".mob-verify-btn").click(function(e){
		var mobile_regx=/[0-9]/;
		$(".field-error").hide();
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
			return false;
		}
		else if($("#otp").val().length!="6" || !mobile_regx.test($("#otp").val())){
			$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
			return false;
		}
		else{
			$(".reg-success").show();
			return true;
		}

	});

	$("#mob-verify-btn-bill").click(function(e){
		var mobile_regx=/[0-9]/;
		$(".field-error").hide();
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
			return false;
		}
		else if($("#otp").val().length!="6" || !mobile_regx.test($("#otp").val())){
			$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
			return false;
		}
		else{
			$(".reg-success").show();
			return false;
		}

	});

	$(".student-reg-submit-bill").click(function(){
		$(".field-error").hide();
		var flag = true;
		var student_reg = [];
		if($("#bill_mobile_number").val().length=="4"){
			$("#bill_mobile_number_error").show().css("display","block");
			$("#bill_mobile_number_error").html("Mobile number information is not filled.");
			flag = false;
		}
		else if($("#bill_mobile_number").val().length!="4" && $("#bill_mobile_number").val().length!="15"){
			$("#bill_mobile_number_error").show().css("display","block");
			$("#bill_mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			flag = false;
		}
		if($("#email_id").val()==""){
			$("#email_id_error").show().css("display","block");
			$("#email_id_error").html("Email ID information is not filled.");
			flag = false;
		}
		else if(!isEmail($("#email_id").val()) || $("#email_id").val().length<9){
			$("#email_id_error").show().css("display","block");
			$("#email_id_error").html("Please check the email ID provided, it looks incorrect.");
			flag = false;
		}
		if($("#user_id").val()==""){
			$("#user_id_error").show();
			$("#user_id_error").html("User ID information is not filled.");
			flag = false;
			student_reg.push('user_id');
		}
		else if($("#user_id").val().length<6 || $("#user_id").val().length>30){
			$("#user_id_error").show();
			$("#user_id_error").html("Please enter valid user ID.");
			flag = false;
			student_reg.push('user_id');
		}
		else if(!isEmail($("#user_id").val())){
			var regx = /^[A-Za-z0-9]+$/;
			if(!regx.test($("#user_id").val())){
				$("#user_id_error").show();
				$("#user_id_error").html("User ID should contain only alphanumeric.");
				flag = false;
				student_reg.push('user_id');
			}
		}
		if($("#user_password").val()==""){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password information is not filled.");
			student_reg.push('user_password');
			flag = false;
		}
		else {
			var regx = /^[A-Za-z0-9]+$/;
			if($("#user_password").val().length<8){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be minimum 8.");
				flag = false;
				student_reg.push('user_password');
			}
			else if($("#user_password").val().length>20){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be maximum 20");
				flag = false;
				student_reg.push('user_password');
			}
			else if(!regx.test($("#user_password").val())){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password should contain only alphanumeric.");
				flag = false;
				student_reg.push('user_password');
			}
		}

		if($("#confirm_password").val()==""){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Confirm Password information is not filled.");
			flag = false;
			student_reg.push('confirm_password');
		}

		if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
			if($("#user_password").val()!=$("#confirm_password").val()){
				$("#confirm_password_error").show();
				$("#confirm_password_error").html("Hey the confirm password value does not match.. Enter the same password and yes do remember it");
				flag = false;
				student_reg.push('confirm_password');
			}
		}

		if($("#address1").val()==""){
			$("#address1_error").show();
			$("#address1_error").html("Address 1 information is not filled.");
			flag = false;
		}
		if($("#address2").val()==""){
			$("#address2_error").show();
			$("#address2_error").html("Address 2 information is not filled.");
			flag = false;
		}
		if($("#city").val()==""){
			$("#city_error").show();
			$("#city_error").html("City /Town /Village information is not filled.");
			flag = false;
		}
		if($("#state").val()==""){
			$("#state_error").show();
			$("#state_error").html("State information is not filled.");
			flag = false;
		}
		if($("#bill_pincode").val()=="" && $("#country").val()=="India"){
			$("#bill_pincode_error").show();
			$("#bill_pincode_error").html("Pincode information is not filled.");
			flag = false;
		}
		else{
			if($("#bill_pincode").val()!=""){
				if($("#bill_pincode").val().length!="7"){
					$("#bill_pincode_error").show();
					$("#bill_pincode_error").html("Please enter valid Pincode.");
					flag = false;
				}
			}
		}
		if($("#country").val()==""){
			$("#country_error").show();
			$("#country_error").html("Country information is not filled.");
			flag = false;
		}

		if($("#browsSig").val()!=""){
			var image_size = $("#browsSig")[0].files[0].size;
			var image_extension = $('#browsSig').val().split('.').pop().toLowerCase();
			if($.inArray(image_extension, ['jpeg', 'tiff', 'jpg']) == -1) {
				flag = false;
				$("#browsSig_error").show();
				$("#browsSig_error").html("Only TIFF,JPG,JPEG allowed!");
			}
			else{
				if(image_size>10240){
					validflag = false;
					$("#browsSig_error").show();
					$("#browsSig_error").html('file size should be less than 10kb');
				}
			}
		}
		if(!document.getElementById('checkboxInput').checked){
			$("#term_error").show().css({"display":"block","clear":"both"});
			$("#term_error").html("You must agree terms and conditions before registration.");
			student_reg.push('checkboxInput');
			flag = false;
		}
		scroll_error(student_reg)
		return flag;
	});

	$(".student-reg-submit").click(function(e){
		//e.preventDefault();
		var regx = /^[A-Za-z]+$/;
		var mobile_regx = /[0-9]/;
		var flag = true;
		var student_reg = [];
		$("#father_name_error").html("");
		$("#user_id_error").html("");
		$("#user_password_error").html("");
		$("#confirm_password_error").html("");
		$("#term_error").html("");
		$("#other_email_id_error").html("");
		$("#mobile_number_error").html("");
		if($("#father_name").val()==""){
			$("#father_name_error").show();
			$("#father_name_error").html("Father’s name information is not filled.");
			student_reg.push('father_name');
			flag = false;
			}else if(!regx.test($("#father_name").val())){
			$("#father_name_error").show();
			$("#father_name_error").html("Please enter only alphabets.");
			student_reg.push('father_name');
			flag = false;
		}

		if($("#user_id").val()==""){
			student_reg.push('user_id');
			$("#user_id_error").show();
			$("#user_id_error").html("User ID information is not filled.");
			flag = false;
		}
		else if($("#user_id").val().length<6 || $("#user_id").val().length>30){
			$("#user_id_error").show();
			$("#user_id_error").html("Please enter valid user ID.");
			student_reg.push('user_id');
			flag = false;
		}
		else if(!isEmail($("#user_id").val())){
			var regx = /^[A-Za-z0-9]+$/;
			if(!regx.test($("#user_id").val())){
				$("#user_id_error").show();
				$("#user_id_error").html("User ID should contain only alphanumeric.");
				student_reg.push('user_id');
				flag = false;
			}
		}

		if($("#user_password").val()==""){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password information is not filled.");
			student_reg.push('user_password');
			flag = false;
		}
		else {
			var regx = /^[A-Za-z0-9]+$/;
			if($("#user_password").val().length<8){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be minimum 8.");
				student_reg.push('user_password');
				flag = false;
			}
			else if($("#user_password").val().length>20){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be maximum 20");
				student_reg.push('user_password');
				flag = false;
			}
			else if(!regx.test($("#user_password").val())){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password should contain only alphanumeric.");
				student_reg.push('user_password');
				flag = false;
			}
		}

		if($("#confirm_password").val()==""){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Confirm Password information is not filled.");
			student_reg.push('confirm_password');
			flag = false;
		}

		if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
			if($("#user_password").val()!=$("#confirm_password").val()){
				$("#confirm_password_error").show();
				$("#confirm_password_error").html("Hey the confirm password value does not match.. Enter the same password and yes do remember it");
				student_reg.push('confirm_password');
				flag = false;
			}
		}

		if($("#other_email_id").val()!="" && (!isEmail($("#other_email_id").val()) || $("#other_email_id").val().length<9)){
			$("#other_email_id_error").show().css("display","block");
			$("#other_email_id_error").html("Please check the email ID provided, it looks incorrect.");
			student_reg.push('other_email_id');
			flag = false;
		}

		if($("#mobile_number").val().length!="4" && $("#mobile_number").val().length!="15"){
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number provided, it looks incorrect.");
			student_reg.push('mobile_number');
			flag = false;
		}
		if($("#mobile_number").val()!="" && !mobile_regx.test($("#mobile_number").val()))
		{
			$("#mobile_number_error").show().css("display","block");
			$("#mobile_number_error").html("Please check the mobile number entered.");
			student_reg.push('mobile_number');
			flag = false;
		}
		if(!document.getElementById('checkboxInput').checked){
			$("#term_error").show().css({"display":"block","clear":"both"});
			$("#term_error").html("You must agree terms and conditions before registration.");
			student_reg.push('term');
			flag = false;
		}
		if(!flag){
			scroll_error(student_reg)
			return false;
		}
		else{
			return flag;
		}
	});
	$(".update-aadhaar-number").click(function(e){
		e.preventDefault();
		$(".adhar-number-validation").hide();
		$(".field-error").hide();
		var mobile_regx=/[0-9]/;
		var regx = /^[A-Za-z0-9]+$/;
		var sum = 0;
		$( ".inputs" ).each(function( index ) {
			sum = sum + parseInt($( this ).val().length);
		});
		if(($("#adharNumber1").val()!="" && !mobile_regx.test($("#adharNumber1").val())) || ($("#adharNumber2").val()!="" && !mobile_regx.test($("#adharNumber2").val())) || ($("#adharNumber3").val()!="" && !mobile_regx.test($("#adharNumber3").val())))
		{
			$(".adhar-number-validation").html("Please enter valid Aadhaar Number.").fadeIn().css("display","block");
		}
		if(sum=='12' && document.getElementById('checkboxInput').checked){
			$(this).hide();
			$("#verify-btn").show();
			$("#checkboxInput").attr('disabled', true);
			$("#otp-section").fadeIn();
			$(".inputs").addClass('non-editable');
		}
		else{
			if(!document.getElementById('checkboxInput').checked)
			$("#checkboxInput_error").html("Please check checkbox for authentication.").fadeIn().css("display","block");
			if(sum=='0')
			$(".adhar-number-validation:eq(0)").fadeIn();
			else if(sum!='12')
			$(".adhar-number-validation:eq(1)").fadeIn();
		}

		return false;
	});
	$(".submit-aadhaar-number").click(function(e){
		e.preventDefault();
		$(".adhar-number-validation").hide();
		$(".field-error").hide();
		var mobile_regx=/[0-9]/;
		var regx = /^[A-Za-z0-9]+$/;
		var sum = 0;
		$( ".inputs" ).each(function( index ) {
			sum = sum + parseInt($( this ).val().length);
		});
		if(($("#adharNumber1").val()!="" && !mobile_regx.test($("#adharNumber1").val())) || ($("#adharNumber2").val()!="" && !mobile_regx.test($("#adharNumber2").val())) || ($("#adharNumber3").val()!="" && !mobile_regx.test($("#adharNumber3").val())))
		{
			$(".adhar-number-validation").html("Please enter valid Aadhaar Number.").fadeIn().css("display","block");
		}
		if(sum=='12' && document.getElementById('checkboxInput').checked && $("#captch_code").val().length=="5"){
			$(this).hide();
			$("#verify-btn").show();
			$("#captcha-section").css("display","none");
			$("#checkboxInput").attr('disabled', true);
			$("#otp-section").fadeIn();
			$(".inputs").addClass('non-editable');
		}
		else{
			if($("#captch_code").val()=="")
				$("#captch_code_error").html("Please enter captcha code.").fadeIn().css("display","block");
			else if($("#captch_code").val().length!=5 || !regx.test($("#captch_code").val()))
				$("#captch_code_error").html("Please enter valid captcha code.").fadeIn().css("display","block");
			if(!document.getElementById('checkboxInput').checked)
				$("#checkboxInput_error").html("Please check checkbox for authentication.").fadeIn().css("display","block");
			if(sum=='0')
				$(".adhar-number-validation:eq(0)").fadeIn();
			else if(sum!='12')
				$(".adhar-number-validation:eq(1)").fadeIn();
		}

		return false;
	});

	$("#verify-btn").click(function(){
		var mobile_regx=/[0-9]/;
		$(".field-error").hide();
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
			return false;
		}
		else if($("#otp").val().length!="6" || !mobile_regx.test($("#otp").val())){
			$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
			return false;
		}
	});
	/* Js By Anjali*/
	$(".inputs").keyup(function (e) {
		if(e.keyCode=='8' || e.keyCode=='46' || e.keyCode=='229'){
			if (this.value.length == '0' ) {
				$(this).prev('.inputs').focus();
			}
		}
		else{
			if (this.value.length == '4' ) {
				$(this).next('.inputs').focus();
			}
		}
	});
});

var specialKeys = new Array();
specialKeys.push(8); //Backspace
specialKeys.push(9); //Tab
specialKeys.push(13); //Enter
specialKeys.push(46); //Delete
specialKeys.push(35); //End
specialKeys.push(36); //Home
specialKeys.push(37); //Left Arrow
specialKeys.push(38); //Up Arrow
specialKeys.push(39); //Right Arrow
specialKeys.push(40); //Down Arrow
function IsNumeric(event) {
	/*var keyCode = e.which ? e.which : e.keyCode
		if (e.shiftKey)
		return false;
		var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
	return ret;*/
	var keyCode = event.keyCode || event.which
	// alert(keyCode)
	if($.isNumeric( String.fromCharCode(event.which) ))
	return true;
	var keyCode = event.keyCode || event.which
	if(String.fromCharCode(event.which)=="'" || String.fromCharCode(event.which)==".")
	return false;

	if (event.shiftKey)
	return false;

	//console.log(keyCode+"=="+event.keyCode+"=="+event.which)
	if(keyCode==40)
	return false;
	/*if(keyCode==46)
	return false;*/
	if(keyCode==8 || keyCode==46 || keyCode==229 || specialKeys.indexOf(keyCode) != -1)
	return true;
	var regex = new RegExp("^[0-9]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		return false;
	}
}

$('input[readonly]').focus(function(){
	this.blur();
});

$(document).on('focus blur','.form-control',function(e){
	e.preventDefault();
	if(e.type === 'focusin' || this.value.length > 0){
		$(this).parents('.form-group').addClass('focused');
	}
	else{
		$(this).parents('.form-group').removeClass('focused');
	}
});
$(document).ready(function(){
	$('.form-control').on('focus blur', function (e) {
		$(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
	}).trigger('blur');
});

/*if($(window).width() > 768){
	$(".dropdown").hover(
	function() {
		$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
		$(this).toggleClass('open');
	},
	function() {
		$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
		$(this).toggleClass('open');
	}
	);
};*/

function printContent(el){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}

function isAlphaSpace(event) {
	var inputValue = event.charCode;
	if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
		event.preventDefault();
	}
}

function isAlphaNumeric(event) {
	var keyCode = event.keyCode || event.which
	if (event.shiftKey)
		return false;

	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}

	var regex = new RegExp("^[a-zA-Z0-9-]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function addressValidation(event) {
	var keyCode = event.keyCode || event.which

	if (event.shiftKey)
	return false;

	if(String.fromCharCode(event.which)=="'")
		return false;

	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}

	var regex = new RegExp("^[a-zA-Z0-9-.,/ ]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function onlyAlphaNumericref(event) {
	var keyCode = event.keyCode || event.which

	/*if (event.shiftKey)
	return false;*/

	if(String.fromCharCode(event.which)=="'" || String.fromCharCode(event.which)=="$" || String.fromCharCode(event.which)=="#" || String.fromCharCode(event.which)=="%" || String.fromCharCode(event.which)=="&" || String.fromCharCode(event.which)=="(")
		return false;

	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}

	var regex = new RegExp("^[a-zA-Z0-9-.:/ ]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function nadIDValidation(event) {
		var str = $('#nad_id').val();
	if(typeof str == 'undefined'){
		var str= $('#nad_certificate_id').val();
		if(typeof str == 'undefined'){
			var str= $('#forgot_user_id').val();
		}
	}
	var keyCode = event.keyCode || event.which
	//console.log(keyCode)
	if(str.length == 0 && keyCode>=48 && keyCode<=57 ){
		return false;
	}
	if(keyCode == 46 || keyCode == 39){
		return false;
	}
	if (keyCode!="78" && keyCode!="67" && event.shiftKey)
	return false;
	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || keyCode==229 || keyCode==46 || (keyCode >= 35 && keyCode <= 40) || keyCode==99 || keyCode==110 || keyCode==78 || keyCode==67) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		if(str.length>0 && (keyCode==99 || keyCode==110 || keyCode==78 || keyCode==67)){
			return false;
		}
		return;
	}

	var regex = new RegExp("^[NC0-9]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function onlyAlphaNumeric(event) {
	var keyCode = event.keyCode || event.which
	if (event.shiftKey || keyCode=="39")
	return false;

	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}

	var regex = new RegExp("^[a-zA-Z0-9]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function onlyAlphabets(event) {
	var keyCode = event.keyCode || event.which
	if (event.shiftKey || keyCode=="39")
	return false;

	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}

	var regex = new RegExp("^[a-zA-Z]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function onlyAlphaNumericSpace(event) {
	var keyCode = event.keyCode || event.which
	if (event.shiftKey || keyCode=="39")
	return false;

	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}

	var regex = new RegExp("^[a-zA-Z0-9 ]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function uniquerefnovalidation(event) {
	var keyCode = event.keyCode || event.which

	if (event.shiftKey)
	return false;

	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || (keyCode >= 35 && keyCode <= 40)) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		return;
	}

	var regex = new RegExp("^[a-zA-Z0-9-/]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function isEmail(email) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(email);
	/*var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);*/
}



$("#mobile_number").keydown(function(e) {
	var selected = $(".nationality:checked").val();
	if(selected=="Foreigner"){
	}
	else{
		var oldvalue=$(this).val();
		var field=this;
		if(field.value.indexOf('+91 ') !== 0) {
			if(oldvalue=="+91")
				oldvalue="+91 ";
			$("#mobile_number").val(oldvalue);
		}
		else{
		}
		/*var oldvalue=$(this).val();
		var field=this;
		setTimeout(function () {
			if(field.value.indexOf('+91 ') !== 0) {
				$(field).val(oldvalue);
			}
		}, 1);*/
	}
});

$("#bill_mobile_number").keydown(function(e) {
	/*var oldvalue=$(this).val();
	var field=this;
	setTimeout(function () {
		if(field.value.indexOf('+91 ') !== 0) {
			$(field).val(oldvalue);
		}
	}, 1);*/
	var oldvalue=$(this).val();
	var field=this;
	setTimeout(function () {
		if(field.value.indexOf('+91 ') !== 0) {
			$(field).val(oldvalue);
		}
	}, 1);
});

$("#bill_mobile_number").keyup(function(e) {
	var get_focus =f1(document.getElementById('bill_mobile_number'));
	var oldvalue = $(this).val().replace(/\s/g, '') ;
	var len = $(this).val().length;

	if(get_focus==9 && e.keyCode=="8"){
		get_focus--;
		var str1 = oldvalue.substring(0, 3);
		var str2 = oldvalue.substring(3, 7)+oldvalue.substring(8,9);
		var str3 = oldvalue.substring(9);
	}
	else{
		var str1 = oldvalue.substring(0, 3);
		var str2 = oldvalue.substring(3, 8);
		var str3 = oldvalue.substring(8);
	}

	if(str3!=""){
		$(this).val(str1+" "+str2+" "+str3);
	}
	else if(str2!=""){
		$(this).val(str1+" "+str2);
	}
	if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
		setCaretPosition('bill_mobile_number', get_focus)
	}
});

$("#bill_pincode").keyup(function(e) {
	var get_focus =f1(document.getElementById('bill_pincode'));
	var oldvalue = $(this).val().replace(/\s/g, '') ;
	var len = $(this).val().length;

	if(get_focus==3 && e.keyCode=="8"){
		get_focus--;
		var str1 = oldvalue.substring(0, 2)+oldvalue.substring(3, 4);
		var str2 = oldvalue.substring(4);
	}
	else{
		var str1 = oldvalue.substring(0, 3);
		var str2 = oldvalue.substring(3);
	}
	if(str2!=""){
		$(this).val(str1+" "+str2);
	}
	if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
		setCaretPosition('bill_pincode', get_focus)
	}
});

$("#mobile_number").keyup(function(e) {

	var get_focus =f1(document.getElementById('mobile_number'));
	var oldvalue = $(this).val().replace(/\s/g, '') ;
	var len = $(this).val().length;

	var selected = $(".nationality:checked").val();
	if(selected=="Foreigner"){
		/*if(get_focus==11 && e.keyCode=="8"){
			var str1 = oldvalue.substring(0, 5);
			var str2 = oldvalue.substring(5, 9)+oldvalue.substring(10,11);
			var str3 = oldvalue.substring(11);
			}
			else*/ if(get_focus==5 && e.keyCode=="8"){
			get_focus--;
			var str1 = oldvalue.substring(0, 4)+oldvalue.substring(5,6);
			var str2 = oldvalue.substring(6, 11);
			var str3 = oldvalue.substring(11);
		}
		else{
			var str1 = oldvalue.substring(0, 5);
			var str2 = oldvalue.substring(5, 10);
			var str3 = oldvalue.substring(10);
		}
		/*if(str3!=""){
			if(get_focus!=12)
			get_focus++;
			$(this).val(str1+" "+str2+" "+str3);
			}
			else*/ if(str2!=""){
			/*get_focus++;
				if(get_focus==5){
				get_focus++;
				get_focus++;
			}*/
			$(this).val(str1+" "+str2);
		}
		if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
			setCaretPosition('mobile_number', get_focus)
		}
	}
	else{
		if(get_focus==9 && e.keyCode=="8"){
			get_focus--;
			var str1 = oldvalue.substring(0, 3);
			var str2 = oldvalue.substring(3, 7)+oldvalue.substring(8,9);
			var str3 = oldvalue.substring(9);
		}
		else{
			var str1 = oldvalue.substring(0, 3);
			var str2 = oldvalue.substring(3, 8);
			var str3 = oldvalue.substring(8);
		}

		if(str3!=""){
			$(this).val(str1+" "+str2+" "+str3);
		}
		else if(str2!=""){
			$(this).val(str1+" "+str2);
		}
		if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
			setCaretPosition('mobile_number', get_focus)
		}
	}
});




function setCaretPosition(elemId, caretPos) {
	var el = document.getElementById(elemId);

	if (el !== null) {

		if (el.createTextRange) {
			var range = el.createTextRange();
			range.move('character', caretPos);
			range.select();
			return true;
		}
		else {
			if (el.selectionStart || el.selectionStart === 0) {
				el.focus();
				el.setSelectionRange(caretPos, caretPos);
				return true;
			}

			else  { // fail city, fortunately this never happens (as far as I've tested) :)
				el.focus();
				return false;
			}
		}
	}
}

function f1(el) {
	var val = el.value;
	return val.slice(0, el.selectionStart).length;
}



function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#imgView').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#browsImg").change(function(){
	$(".add_doc").html($(this).val());
	readURL(this);
});




function readURL1(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#signatureView').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#browsSig").change(function(){
	$(".id_doc").html($(this).val());
	readURL1(this);
});



/*$(".mob-verify-btn").click(function(e){
	e.preventDefault();
	$(".reg-success").fadeIn();
	setInterval(function(){ $(".reg-success").fadeOut(); }, 3000);
});*/

function getAge(birth) {
	var today = new Date();
	var curr_date = today.getDate();
	var curr_month = today.getMonth() + 1;
	var curr_year = today.getFullYear();

	var pieces = birth.split('/');
	var birth_date = pieces[1];
	var birth_month = pieces[0];
	var birth_year = pieces[2];

	if (curr_month == birth_month && curr_date >= birth_date) return parseInt(curr_year-birth_year);
	if (curr_month == birth_month && curr_date < birth_date) return parseInt(curr_year-birth_year-1);
	if (curr_month > birth_month) return parseInt(curr_year-birth_year);
	if (curr_month < birth_month) return parseInt(curr_year-birth_year-1);
}




$(document).ready(function(){
	$('input[type="radio"][name=upload1]').change(function(){
		$(".field-error").html("");
		//$("#transaction").addClass("showHideMasterData");
		//$("#academic").addClass("showHideMasterData");
		resetForm("transactiondeatailForm","transaction");
		resetForm("nadinfoForm","academic");
		if($(this).attr("value")=="yes-content"){
			$(".yes-content").show();
			$(".no-content").hide();
			// $("#no-text").html("Have you ever registered earlier with NAD (through NDML or CVL)?");
			}else{
			$(".yes-content").hide();
			$(".no-content").show();
			// $("#no-text").html("To get a consolidated view of all your certificates, it is important to use your existing NAD ID if already registered.");
		}
	});

	$('input[type="radio"][name=r1]').change(function(){
		if($(this).attr("value")=="yes-content"){
			$(".printed_r1").show();
			$(".digital_r2").hide();
		}
		else{
			$(".printed_r1").hide();
			$(".digital_r2").show();
		}
	});
});


/************** JS by Deepti start *************/
$(document).ready(function(){
	//owl slider
	$("#owl-demo").owlCarousel({
		nav: false,
		autoplay:true,
		dots: true,
		items:1,
		loop: true,
		animateOut: 'fadeOut'
	});

	var winWidth = $(window).width();
	var winHeight = $(window).height();
	if (winWidth > 768){
		var headerHeight = $(".custom-nav").height() + 30;
		var bannerImgHeight = winHeight - headerHeight;
		$(".banner-img").css("height", bannerImgHeight);
		$("#owl-demo li").css("height", bannerImgHeight);
		/* var bannerImg = $("#owl-demo .owl-item img").attr("src");
		$("#owl-demo .owl-item li").css({"backgroundImage": "url(" + bannerImg + ")","background-size":"cover"}); */
	}

	$("#owl-demo1").owlCarousel({
		nav: true,
		autoplay:false,
		smartSpeed : 1000,
		dots: false,
		items:1,
		loop: true,
		margin:20,
		rewindNav : false,
		pagination : false
	});

	$("#owl-demo2").owlCarousel({
		nav: true,
		autoplay:true,
		smartSpeed : 1000,
		dots: false,
		items:3,
		loop: true,
		margin:20,
		rewindNav : false,
		pagination : false,
		responsive: {
			0: {
				items: 1
			},
			480: {
				items: 2
			},
			1024: {
				items: 3
			}
		},
	});

	$('.nad-presentation-slider').owlCarousel({
          autoplay: false,
          loop:true,
          margin:10,
          dots:false,
          nav:true,
          responsive:{
              320:{
                  items:1
              },
              350:{
                  items:1
              },
              450:{
                  items:1
              },
              600:{
                  items:1
              }
          }
    });


	$(".stepTabList li a").click(function(){
		$(".stepTabList li a").removeClass("activeTab");
		$(this).addClass("activeTab");
		var tabName = $(this).attr("for");
		$(".step-div").css("display","none");
		$("." + tabName).fadeIn();
	});

	/* JS BY Anjali*/

	$(".submit-forgot-password4").click(function(){
		var flag = true;
		var regx = /^[A-Za-z0-9]+$/;
		var scroll = [];
		$("#forgot_user_id_error").html("");
		$(".field-error").html("");
		if($("#forgot_user_id").val()==""){
			$("#forgot_user_id_error").html("User ID information is not filled.");
			flag = false;
			scroll.push("forgot_user_id");
		}
		else if($("#forgot_user_id").val().length<6 || $("#forgot_user_id").val().length>30){
			$("#forgot_user_id_error").html("ID entered by you does not exist or is incorrect, kindly check the ID and try again.");
			flag = false;
			scroll.push("forgot_user_id");
		}
		else{
			if(!isEmail($("#forgot_user_id").val())){
				var letters = /^[a-zA-Z0-9]+$/;
				if(!(letters.test($("#forgot_user_id").val()))){
					$("#forgot_user_id_error").html("ID entered by you does not exist or is incorrect, kindly check the ID and try again.");
					flag = false;
					scroll.push("forgot_user_id");
				}
			}
		}
		scroll_error(scroll);
		if(flag){
			/* $(".stepTabList li:eq(1)").css("pointer-events","auto"); */
			$("#otp_send_msg").fadeIn();
			/*$(".idtype").text($("#forgotUserID option:selected").text());*/
			$(".idtype").val($("#forgot_user_id").val());
			$(".idtype1").val($("#forgot_user_id").val());
			setTimeout(function(){
				$(".steptabOne").css("display","none");
				$(".steptabTwo").css("display","block");
				$(".stepTabList li:nth-child(2) a").addClass("activeTab");
				$(".stepTabList li:nth-child(2)").addClass("activeLeftLine");
				$("#otp_send_msg").fadeOut();
			}, 3000);

		}
		return false;
	});


	$(".submit-forgot-password1").click(function(){
		var flag = true;
		var regx = /^[A-Za-z0-9]+$/;
		var scroll = [];
		$("#forgot_user_id_error").html("");
		$(".field-error").html("");
		if($("#forgot_user_id").val()==""){
			$("#forgot_user_id_error").html("User ID information is not filled.");
			flag = false;
			scroll.push("forgot_user_id");
		}
		else if($("#forgot_user_id").val().length<6 || $("#forgot_user_id").val().length>30){
			$("#forgot_user_id_error").html("ID entered by you does not exist or is incorrect, kindly check the ID and try again.");
			flag = false;
			scroll.push("forgot_user_id");
		}
		else{
			if(!isEmail($("#forgot_user_id").val())){
				var letters = /^[a-zA-Z0-9]+$/;
				if(!(letters.test($("#forgot_user_id").val()))){
					$("#forgot_user_id_error").html("ID entered by you does not exist or is incorrect, kindly check the ID and try again.");
					flag = false;
					scroll.push("forgot_user_id");
				}
			}
		}
		/*$("#forgot_user_id_error").html("");
			$(".field-error").html("");
			if($("#forgot_user_id").val()==""){
			$("#forgot_user_id_error").html("User ID information is not filled.");
			flag = false;
			}
			else if($("#forgot_user_id").val().length!=14){
			$("#forgot_user_id_error").html("ID entered by you does not exist or is incorrect, kindly check the ID and try again.");
			flag = false;
			}

			if($("#forgotUserID").val()=="addharId"){
			var str = $("#forgot_user_id").val().replace(/\s/g, '') ;
			if(!$.isNumeric(str)){
			$("#forgot_user_id_error").html("Please enter valid aadhar ID.").show();
			flag = false;
			}
			}
			else{
			var str = $("#forgot_user_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="N" || fisrtChar=="C" || fisrtChar=="n" || fisrtChar=="c"){
			var subStr = str.substring(1);
			if(!$.isNumeric(subStr)){
			$("#forgot_user_id_error").html("Please enter valid NAD ID.").show();
			flag = false;
			}
			}
			else{
			$("#forgot_user_id_error").html("Please enter valid NAD ID.").show();
			flag = false;
			}
		}*/

		if($("#captch_code").val()==""){
			$("#captch_code_error").html("Please enter captcha code.").fadeIn().css("display","block");
			flag = false;
			scroll.push("captch_code");
		}
		else if($("#captch_code").val().length!=5 || !regx.test($("#captch_code").val())){
			$("#captch_code_error").html("Please enter valid captcha code.").fadeIn().css("display","block");
			flag = false;
			scroll.push("captch_code");
		}
		scroll_error(scroll);
		if(flag){
			/* $(".stepTabList li:eq(1)").css("pointer-events","auto"); */
			$("#otp_send_msg").fadeIn();
			/*$(".idtype").text($("#forgotUserID option:selected").text());*/
			$(".idtype").text($("#forgot_user_id").val());
			setTimeout(function(){
				$(".steptabOne").css("display","none");
				$(".steptabTwo").css("display","block");
				$(".stepTabList li:nth-child(2) a").addClass("activeTab");
				$(".stepTabList li:nth-child(2)").addClass("activeLeftLine");
			}, 3000);

		}
		return false;
	});

	$(".submit-forgot-password2").click(function(){
		var flag = true;
		var scroll = [];
		$(".field-error").html("");
		$("#otp_fail_msg").hide();
		if($("#otp").val()==""){
			$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
			flag = false;
			scroll.push("otp");
		}
		else if($("#otp").val().length!="6"){
			//$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
			$("#otp_fail_msg").show();
			flag = false;
			scroll.push("otp");
		}
		scroll_error(scroll);
		if(flag){
			/* $(".stepTabList li:eq(2)").css("pointer-events","auto"); */
			/*$("#otp_validate_success").fadeIn();
			setTimeout(function(){ $(".steptabTwo").css("display","none"); $(".steptabThree").css("display","block"); }, 3000);*/
			$("#otp_send_msg").fadeOut();
			$(".steptabTwo").css("display","none"); $(".steptabThree").css("display","block");
			$(".stepTabList li:nth-child(3) a").addClass("activeTab");
			$(".stepTabList li:nth-child(2)").addClass("activeRightLine");
		}
		return false;
	});

	$(".submit-forgot-password3").click(function(){
		var scroll = [];
		var flag = true;
		$(".field-error").html("");
		$("#password_fail_msg").hide();
		if($("#user_password").val()==""){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password information is not filled.");
			flag = false;
			scroll.push("user_password");
		}
		else {
			var regx = /^[A-Za-z0-9]+$/;
			if($("#user_password").val().length<8){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be minimum 8.");
				flag = false;
				scroll.push("user_password");
			}
			else if($("#user_password").val().length>20){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password length should be maximum 20");
				flag = false;
				scroll.push("user_password");
			}
			else if(!regx.test($("#user_password").val())){
				$("#user_password_error").show();
				$("#user_password_error").html("User Password should contain only alphanumeric.");
				flag = false;
				scroll.push("user_password");
			}
		}

		if($("#confirm_password").val()==""){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Confirm Password information is not filled.");
			flag = false;
			scroll.push("confirm_password");
		}

		if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
			if($("#user_password").val()!=$("#confirm_password").val()){
				$("#password_fail_msg").show();
				flag = false;
			}
		}
		scroll_error(scroll);
		if(flag){
			$("#password_change_success").show();
			$("#lastForm").hide();
			// $('.setpassword-view')
			$("html, body").animate({
        scrollTop: $('.setpassword-container').offset().top - 150
      }, 1000);
			/*$("#user_password").val("");
			$("#user_passwordClone2").val("");
			$("#confirm_passwordClone3").val("");
			$(".userid-no").val("");*/
		}
		return false;
	});
	$("#nad_id").keyup(function(e) {
		e.preventDefault();
		var get_focus =f1(document.getElementById($(this).attr('id')));
		var oldvalue = $(this).val().replace(/\s/g, '') ;
		var len = $(this).val().length;

		if(get_focus==4 && e.keyCode=="8"){
			get_focus--;
			var str1 = oldvalue.substring(0, 3)+oldvalue.substring(4,5);
			var str2 = oldvalue.substring(5, 9);
			var str3 = oldvalue.substring(9);
		}
		else if(get_focus==9 && e.keyCode=="8"){
			get_focus--;
			var str1 = oldvalue.substring(0, 4);
			var str2 = oldvalue.substring(4, 7)+oldvalue.substring(8,9);
			var str3 = oldvalue.substring(9);
		}
		else{
			var str1 = oldvalue.substring(0, 4);
			var str2 = oldvalue.substring(4, 8);
			var str3 = oldvalue.substring(8);
		}
		if(str3!=""){
            if(str3.length>4)
            {
                str3 = str3.slice(0, -2);
            }
            $(this).val(str1+" "+str2+" "+str3);
        }
		else if(str2!=""){
			$(this).val(str1+" "+str2);
		}
		else{
			$(this).val(str1);
		}
		if(get_focus!=len && e.keyCode!="37" && e.keyCode!="39"){
			setCaretPosition($(this).attr('id'), get_focus)
		}
	});

	/*$("#forgotUserID").change(function(){
		var inputField = '<input maxlength="14" value="" id="forgot_user_id" type="tel" class="form-control maxlenghtValidation" data-toggle="tooltip" data-placement="bottom" title="User ID should be minimum 6 characters long without spaces" onkeypress="return IsNumeric(event);">';
		if($(this).val() == 'nadId'){
		inputField = '<input maxlength="14" value="" id="forgot_user_id" type="text" class="form-control maxlenghtValidation" data-toggle="tooltip" data-placement="bottom" title="User ID should be minimum 6 characters long without spaces">';
		}
	});*/

	$(".masterDataUpdateLink").click(function(){
		if ($('.modal:visible').length) {
			/* $('body').addClass('modal-open'); */
			$('#myModal').modal('hide');
		}

		/* $("body").addClass("modal-open"); */
		/* $("body").css({"overflow":"hidden","paddingRight":"17px"});  */
	});

	$(".masterType").css("display","block");
	$('#filterBy,#filterBy1').change(function(){
		$(".field-error").html("");
		var id = $(this).attr('id');
		$("form input").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
			$('#'+$(this).attr("id")).removeClass('error-focused')
		});
		$("form select").each(function(){
			if(typeof $(this).attr("id") !== "undefined")
			$('#'+$(this).attr("id")).removeClass('error-focused')
		});
		var filterVal = $('#'+id).val();
		if(id=="filterBy1")
			$(".filterByType1").css("display","none");
		else
			$(".filterByType").css("display","none");
		$("." + filterVal).fadeIn();
		var attr = $(this).attr('hideDiv');
		if (typeof attr !== typeof undefined && attr !== false) {
		    $("#"+attr).addClass("showHideMasterData");
		}
		var attr = $(this).attr('reset_id');
		if (typeof attr !== typeof undefined && attr !== false) {
		    $('#'+attr)[0].reset();
		    $('#'+id).val(filterVal);
		}
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		if(id=="filterBy" && $('#viewOnlineVerirficationData').length){
			$('html, body').animate({
				scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
			}, 1200);
		}
	});


});
/*************** JS by Deepti end ************/


$(document).ready(function() {
	/* $('input[type!="button"][type!="submit"], select, textarea').val('').blur(); */
	var $scrollingDiv = $(".id-based-edit");

	$(window).scroll(function(){
		$scrollingDiv
		.stop()
		.animate({"marginTop": ($(window).scrollTop()) + "px"}, "slow" );
	});

	$('.show-password-button').on('click', function () {

		var $this = $(this);
		var $input = $($this.data('target'));

		var newType = ($input.attr('type') === 'password') ? 'text' : 'password';
		if(newType=="text"){
			$(".view-input").show();
			$("#user_passwordClone1").hide();
			$("#user_passwordClone0").hide();
			$("#user_passwordClone2").hide();
		}
		else{
			$(".view-input").hide();
			$("#user_passwordClone1").show();
			$("#user_passwordClone0").show();
			$("#user_passwordClone2").show();
		}
		$input.attr('type', newType);
		$(this).find("i").toggleClass("fa-eye-slash")

	});

});




function scroll_error(id){
	$("input").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$("textarea").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$("select").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$(".bootstrap-select.custom-select").find('button').removeClass('error-focused');
	if(typeof id[0] !== 'undefined' && id[0] !== '' && id[0]!=='term'&& id[0]!=='certificate_checkbox_table'){
		console.log(id[0]+"==id")
		$('html, body').animate({
			scrollTop: $("#"+id[0]).offset().top-$("nav").height()
		}, 1200);
	}
	$.each(id,function(key,val){
		if(val!="certificate_checkbox_table" && val!="xmlFileDiv")
			$('#'+val).addClass('error-focused')
	})

	$('.selectpicker').each(function(){
		if ($(this).hasClass('error-focused')){
			$(this).prev().prev('button').addClass('error-focused');
		}
		else{
			$(this).prev().prev('button').removeClass('error-focused');
		}
	});
}

$(document).ready(function(){

	$("#userLoginSubmit").click(function(e) {
		var flag=1;
		var regx = /^[A-Za-z0-9]+$/;
		e.preventDefault();
		$(".field-error").html("");

		if($("#userId").val()==""){
			$("#userId-error").html("Please enter User Id.").fadeIn().css("display","block");
			flag=0;
		}
		else if($("#userId").val().length>30){
			$("#userId-error").html("Please enter valid user ID.").fadeIn().css("display","block");
			flag = 0;
		}
		else{
			if(!isEmail($("#userId").val())){
				var letters = /^[a-zA-Z0-9]+$/;
				if(!(letters.test($("#userId").val()))){
					$("#userId-error").html("Please enter valid user ID.").fadeIn().css("display","block");
					flag = 0;
				}
			}
		}
		if($("#userPassword").val()==""){
			$("#userPassword-error").html("Please enter Password.").fadeIn().css("display","block");
			flag=0;
		}
		else if($("#userPassword").val().length<8 || $("#userPassword").val().length>20){
			$("#userPassword-error").html("Please enter valid Password.").fadeIn().css("display","block");
			flag = 0;
		}
		else if(!regx.test($("#userPassword").val())){
			$("#userPassword-error").html("Please enter valid Password.").fadeIn().css("display","block");
			flag=0;
		}

		if($("#captchcode").val()==""){
			$("#captchaCode-error").html("Please enter captcha code.").fadeIn().css("display","block");
			flag=0;
		}
		else if($("#captchcode").val().length!=5){
			$("#captchaCode-error").html("Please enter valid captcha code.").fadeIn().css("display","block");
			flag=0;
		}
		else if(!regx.test($("#captchcode").val())){
			$("#captchaCode-error").html("Please enter valid captcha code.").fadeIn().css("display","block");
			flag=0;
		}
		if(flag==1){
			if($("#typeOfMaster").val()=="Student")
			window.location.href="student_landing_page_5_1.html";
			if($("#typeOfMaster").val()=="Academic Institution")
			window.location.href="";
			if($("#typeOfMaster").val()=="Verifier")
			window.location.href="";
		}
	});

});

function sub_submit1(){
	var flag = true;
	$("#emailtext_error").html("");
	$("#emailtext_success").hide();
	 if($("#emailtext").val()==""){
        $("#emailtext_error").show().css("display","block");
        $("#emailtext_error").html("Email ID information is not filled.");
        flag = false;
    }
    else if(!isEmail($("#emailtext").val()) || $("#emailtext").val().length<9){
        $("#emailtext_error").show().css("display","block");
        $("#emailtext_error").html("Please check the email ID provided, it looks incorrect.");
        flag = false;
    }
    else{
    	$("#emailtext_success").show();
    }
}

function update_other_details(id){
	$(".field-error").html("");
	var mobregx = /^[0-9]+$/;
	var flag = 1;
	var scroll = [];
	if($("#pan_no").val()==""){
		$("#pan_no_error").html("PAN No. information is not filled.").show();
		flag=0;
		scroll.push('pan_no');
	}
	if($("#service_tax").val()==""){
		$("#service_tax_error").html("Any of the specified value need to be selected.").css('display','inline-block');
		flag=0;
		scroll.push('service_tax');
	}
	if($("#service_tax").val()=="Yes" && $("#service_tax_no").val()==""){
		$("#service_tax_no_error").html("Service Tax No. information is not filled.").css('display','inline-block');
		flag=0;
		scroll.push('service_tax_no');
	}
	if($("#service_tax_no").val()!="" && ($("#service_tax_no").val().length!="15" || !mobregx.test($("#service_tax_no").val()))){
		$("#service_tax_no_error").html("Please enter valid Service Tax No.").css('display','inline-block');
		flag=0;
		scroll.push('service_tax_no');
	}
	if($("#service_tax").val()=="No" &&  $("#service_tax_details").val()==""){
		$("#service_tax_details_error").html("Service Tax Exemption Details information is not filled.").css('display','inline-block');
		flag=0;
		scroll.push('service_tax_details');
	}
	scroll_error(scroll);
	if(flag==1){
		$("#"+id).html('Edit <span class="edit-icon" id="mob_edit_icon"></span>');
		$("#collapse-6 .manage-col-width").each(function(index, el) {
			$(this).addClass('non-editable');
		});
	}
}

function update_communication_details(id){
	$(".field-error").html("");
	var flag = 1;
	var scroll = [];
	var mobregx = /^[0-9]+$/;
	$(".communication_name").each(function(index){
		if($(this).val()==""){
			flag=0;
			scroll.push('communication_name'+index);
			$("#communication_name"+index+"_error").html("Name information is not filled.").show();
		}
	});
	$(".mobileno").each(function(index){
		if($(this).val()==""){
			flag=0;
			scroll.push('mob'+index);
			$("#mob"+index+"_error").html("Contact Number information is not filled.").show();
		}
		else if($(this).val().length!="10"){
			flag=0;
			scroll.push('mob'+index);
			$("#mob"+index+"_error").html("Please check the contact number provided, it looks incorrect.").show();
		}
		else if(!mobregx.test($(this).val())){
			scroll.push('mob'+index);
			$("#mob"+index+"_error").html("Please check the contact number provided, it looks incorrect.").show();
			flag = 0;
		}
	});
	$(".communication_email").each(function(index){
		if($(this).val()==""){
			flag=0;
			scroll.push('email'+index);
			$("#email"+index+"_error").html("Email ID information is not filled.").show();
		}
		else if(!isEmail($(this).val())){
			scroll.push('email'+index);
			$("#email"+index+"_error").html("Please check the email Id provided, it looks incorrect.").show();
			flag = 0;
		}
	});
	scroll_error(scroll);
	if(flag==1){
		$("#"+id).html('Edit <span class="edit-icon" id="mob_edit_icon"></span>');
		$("#communication_table").addClass('non-editable');
	}
}

function update_bank_details(id){
	$(".field-error").html("");
	var flag = 1;
	var scroll = [];
	var mobregx = /^[0-9]+$/;
	if($("#bank_name").val()==""){
		$("#bank_name_error").html("Any of the specified value need to be selected.").show();
		flag=0;
		scroll.push('bank_name');
	}
	if($("#account_no").val()==""){
		$("#account_no_error").html("Account No information is not filled.").show();
		flag=0;
		scroll.push('account_no');
	}
	else if(!mobregx.test($("#account_no").val())){
		$("#account_no_error").html("Please enter valid Account No.").show();
		flag=0;
		scroll.push('account_no');
	}
	if($("#account_type").val()==""){
		$("#account_type_error").html("Any of the specified value need to be selected.").show();
		flag=0;
		scroll.push('account_type');
	}
	if($("#mirc_code").val()==""){
		$("#mirc_code_error").html("MICR Code information is not filled.").show();
		flag=0;
		scroll.push('mirc_code');
	}
	if($("#ifsc_code").val()==""){
		$("#ifsc_code_error").html("IFSC Code information is not filled.").show();
		flag=0;
		scroll.push('ifsc_code');
	}
	scroll_error(scroll);
	if(flag==1){
		$("#"+id).html('Edit <span class="edit-icon" id="mob_edit_icon"></span>');
		$("#collapse-5 .manage-col-width").each(function(index, el) {
			$(this).addClass('non-editable');
		});
	}
}

function update_additiona_details(id){
	$(".field-error").html("");
	var flag = 1;
	var scroll = [];
	if($("#type_of_ownership").val()==""){
		$("#type_of_ownership_error").html("Any of the specified value need to be selected.").show();
		flag=0;
		scroll.push('type_of_ownership');
	}
	if($("#govtfinancialsupport").val()==""){
		$("#govtfinancialsupport_error").html("Any of the specified value need to be selected.").show();
		flag=0;
		scroll.push('govtfinancialsupport');
	}
	if($("#ai_reg").val()==""){
		$("#ai_reg_error").html("Any of the specified value need to be selected.").show();
		flag=0;
		scroll.push('ai_reg');
	}
	scroll_error(scroll);
	if(flag==1){
		$("#"+id).html('Edit <span class="edit-icon" id="mob_edit_icon"></span>');
		$("#collapse-3 .master-div").each(function(index, el) {
			$(this).addClass('non-editable');
		});
	}
}

function pay_now(){
	$(".field-error").html("");
	var mobregx = /^[0-9]+$/;
	var scroll = [];
	var flag=1;
	if($("#amount").val()==""){
		flag = 0;
		$("#amount_error").html("Amount to be deposited information is not filled.").show();
		scroll.push("amount");
	}
	else if(!mobregx.test($("#amount").val())){
		scroll.push('amount');
		$("#amount_error").html("Please enter valid amount").show();
		flag = 0;
	}
	else if(1000 > parseInt($("#amount").val())){
		scroll.push('amount');
		$("#amount_error").html("Minimum Amount Rs. 1,000 and Maximum Amount Rs. 1 Lakh.").show();
		flag = 0;
	}
	else if(100000 < parseInt($("#amount").val())){
		scroll.push('amount');
		$("#amount_error").html("Minimum Amount Rs. 1,000 and Maximum Amount Rs. 1 Lakh.").show();
		flag = 0;
	}
	if($("#email_id").val()==""){
		flag = 0;
		$("#email_id_error").html("Email ID information is not filled.").show();
		scroll.push("email_id");
	}
	else if(!isEmail($("#email_id").val())){
		$("#email_id_error").html("Please check the email ID provided, it looks incorrect.").show();
		flag = 0;
		scroll.push('email_id');
	}
}

function verifier(text)
{
	$(".field-error").html("");
	var scroll = [];
	var flag=1;
	$("#transaction").addClass("showHideMasterData");
	$("#academic").addClass("showHideMasterData");
	if(text=='academic'){
		if($("#entity").val()==""){
			$("#entity_error").html("Any of the specified value need to be selected.").show();
			flag=0;
			scroll.push('state');
		}
		if($("#indiaState").val()==""){
			$("#state_error").html("Any of the specified value need to be selected.").show();
			flag=0;
			scroll.push('indiaState');
		}
		if($("#type_of_academic_institution").val()==""){
			$("#type_of_academic_institution_error").html("Any of the specified value need to be selected.").show();
			flag=0;
			scroll.push('type_of_academic_institution');
		}
		if(flag==1)
		{
			$("#academic").removeClass("showHideMasterData");
			var dashboardContentHeight = $(".dashboard-content").height();
			$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
			$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
			$('.error-msg-no-data').addClass('now-Show');
			$('html, body').animate({
				scrollTop: $("#academic").offset().top-$("nav").height()
			}, 1200);
		}
		scroll_error(scroll);
	}
	else if(text=='transaction'){
		if($("#from_date").val()==""){
			/*$("#from_date_error").html("Please enter From Date.").fadeIn().css("display","block");
			flag=0;*/
		}
		if($("#to_date").val()==""){
			/*$("#to_date_error").html("Please enter to Date.").fadeIn().css("display","block");
			flag=0;*/
		}
		if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
			$("#from_date_error").html("From date should not be greater than to date").fadeIn().css("display","block");
			flag=0;
			scroll.push("from_date");
		}
		if(flag==1)
		{
			$("#transaction").removeClass("showHideMasterData");
			var dashboardContentHeight = $(".dashboard-content").height();
			$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
			$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
			$('.error-msg-no-data').addClass('now-Show');
			$('html, body').animate({
				scrollTop: $("#transaction").offset().top-$("nav").height()
			}, 1200);
		}
		scroll_error(scroll);
	}

}

function submitCertfctDetails()
{
	$(".field-error").html("");
	var scroll = [];
	var flag=1;
	if($("#template_award").val()==""){
		$("#template_award_error").html("Please select Template to be Mapped with Award Events.").fadeIn().css("display","block");
		flag=0;
		scroll.push("template_award");
	}
	/*if($("#description").val()==""){
		$("#description_error").html("Short Description of the Template information is not filled.").fadeIn().css("display","block");
		flag=0;
		scroll.push("description");
	}*/
	var d=new Date();
	var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
	var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();

	/*if($("#to_date").val()==""){
		$("#to_date_error").html("Please select Template Applicability Upto Date.").fadeIn().css("display","block");
		flag=0;
		scroll.push("to_date");
	}*/
	if($("#from_date").val()==""){
		$("#from_date_error").html("Please select Template Applicability From Date.").fadeIn().css("display","block");
		flag=0;
		scroll.push("from_date");
	}
	if($("#to_date").val()!="" && $("#from_date").val()!="")
	{
		if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
			$("#from_date_error").html("Template Applicability From date should not be greater than to date").fadeIn().css("display","block");
			flag=0;
			scroll.push("from_date");
		}
		/*else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
			$("#from_date_error").html("Template Applicability From date should not be more than 90 day old").fadeIn().css("display","block");
			flag=0;
			scroll.push("from_date");
		}
		else if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
			$("#to_date_error").html("Template Applicability Upto Date should not be greater than current date").fadeIn().css("display","block");
			flag=0;
			scroll.push("to_date");
		}*/
	}
	$(".seat-number").each(function(index, el) {
		if($(this).val()==""){
			flag=0;
			$("#seat_number_error").html("Please fill all seat no.").fadeIn().css("display","block");
		}
	});
	scroll_error(scroll);
}
function viewMapping(){
	$("#creationTemplateDetail").addClass("showHideMasterData");
	$(".field-error").html("");
	var scroll = [];
	var flag=1;
	if($("#course_level").val()==""){
		$("#course_level_error").html("Please select Course Level.").fadeIn().css("display","block");
		flag=0;
		scroll.push("course_level");
	}
	var course_flag = false;
	var scroll = [];
	var values = $('input:checkbox:checked.certificate_checkbox').map(function () {
	  course_flag = true;
	}).get();
	if(!course_flag){
		flag = 0;
		$("#course_error").html("Please select atleast one course.").show();
		scroll.push('checkboxInput');
	}
	if($("#mapping_status").val()==""){
		$("#mapping_status_error").html("Please select Course Level.").fadeIn().css("display","block");
		flag=0;
		scroll.push("mapping_status");
	}
	scroll_error(scroll);
	if(flag==1)
	{
		$("#creationTemplateDetail").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#creationTemplateDetail").offset().top-$("nav").height()
		}, 1200);
	}
}
function create_mapping()
{
	$("#creationTemplateDetail").addClass("showHideMasterData");
	$(".field-error").html("");
	var scroll = [];
	var flag=1;
	if($("#course_level").val()==""){
		$("#course_level_error").html("Please select Course Level.").fadeIn().css("display","block");
		flag=0;
		scroll.push("course_level");
	}
	if($("#course").val()==""){
		$("#course_error").html("Please select Course.").fadeIn().css("display","block");
		flag=0;
		scroll.push("course");
	}
	if($("#award_type").val()==""){
		$("#award_type_error").html("Please select Award Type.").fadeIn().css("display","block");
		flag=0;
		scroll.push("award_type");
	}
	if($("#award_event").val()==""){
		$("#award_event_error").html("Please select Award Event.").fadeIn().css("display","block");
		flag=0;
		scroll.push("award_event");
	}
	scroll_error(scroll);
	if(flag==1)
	{
		$("#creationTemplateDetail").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#creationTemplateDetail").offset().top-$("nav").height()
		}, 1200);
	}
}

function submit_certf_form(){
	$("#remark_error").html("");
	$("#remark").removeClass('error-focused');
}

function raise_query(){
	$(".field-error").html("");
	var scroll = [];
	var flag=1;
	if($("#remark").val()==""){
		$("#remark_error").html("Remark information is not filled.").fadeIn().css("display","block");
		flag=0;
		scroll.push("remark");
	}
	scroll_error(scroll);
}

function templateCreatedSubmit()
{
	$(".field-error").html("");
	var scroll = [];
	var flag=1;
	var regx = /^[A-Za-z0-9 ]+$/;
	if($("#certificate_type").val()==""){
		$("#certificate_type_error").html("Any of the specified value need to be selected.").fadeIn().css("display","block");
		flag=0;
		scroll.push("certificate_type");
	}
	if($("#description").val()==""){
		$("#description_error").html("Short Description of the Template information is not filled.").fadeIn().css("display","block");
		flag=0;
		scroll.push("description");
	}
	else if(!regx.test($("#description").val())){
		$("#description_error").html("Short Description of the Template should be alphanumeric.").fadeIn().css("display","block");
		flag = 0;
		scroll.push("description");
	}
	if($("#details").val()==""){
		$("#details_error").html("General Details about Template Applicability information is not filled.").fadeIn().css("display","block");
		flag=0;
		scroll.push("details");
	}
	else if(!regx.test($("#details").val())){
		$("#details_error").html("General Details about Template Applicability should be alphanumeric.").fadeIn().css("display","block");
		flag = 0;
		scroll.push("details");
	}
	if($("#type_crtf_data_upload").val()==""){
		$("#type_crtf_data_upload_error").html("Any of the specified value need to be selected.").fadeIn().css("display","block");
		flag=0;
		scroll.push("type_crtf_data_upload");
	}
	if($("#method_of_creation").val()==""){
		$("#method_of_creation_error").html("Any of the specified value need to be selected.").fadeIn().css("display","block");
		flag=0;
		scroll.push("method_of_creation");
	}
	if($("#sample_template").val()=="" && $("#method_of_creation").val()=="ncx"){
		$("#sample_template_error").html("Please upload Sample Template image.").fadeIn().css("display","block");
		flag=0;
		scroll.push("sample_template");
	}
	if($("#blank_template").val()=="" && $("#method_of_creation").val()=="ncx"){
		$("#blank_template_error").html("Please upload Blank Template image.").fadeIn().css("display","block");
		flag=0;
		scroll.push("blank_template");
	}
	if($("#validation_file").val()=="" && $("#method_of_creation").val()=="ncex"){
		$("#validation_file_error").html("Please select Existing Data Validation file [XSD].").fadeIn().css("display","block");
		flag=0;
		scroll.push("validation_file");
	}
	if($("#file_format").val()==""){
		$("#file_format_error").html("Please select File format of Sample Data.").fadeIn().css("display","block");
		flag=0;
		scroll.push("file_format");
	}
	if($("#sample_data").val()==""){
		$("#sample_data_error").html("Please select File format of Sample Data.").fadeIn().css("display","block");
		flag=0;
		scroll.push("sample_data");
	}
	scroll_error(scroll);
	if(flag==1){
		return true;
	}
	return false;
}

/* Approve data A2.0 */
function searchMasterData()
{
	$("#master-data-form").addClass("showHideMasterData");
	$(".field-error").html("");
	var regx = /^[A-Za-z0-9]+$/;
	var flag=1;
	var scroll = [];
	if($("#filterBy").val()=="masterType")
	{
		var d=new Date();
		var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();

		if($("#to_date").val()==""){
			$("#to_date_error").html("Please select To Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("to_date");
		}
		if($("#from_date").val()==""){
			$("#from_date_error").html("Please select From Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("from_date");
		}
		if($("#to_date").val()!="" && $("#from_date").val()!="")
		{
			if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
				$("#from_date_error").html("From date should not be greater than to date").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
			else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
				$("#from_date_error").html("From date should not be more than 90 day old").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
			else if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
				$("#to_date_error").html("To Date should not be greater than current date").fadeIn().css("display","block");
				flag=0;
				scroll.push("to_date");
			}
		}
	}
	else if($("#filterBy").val()=="fileUploadReference")
	{
		if($("#reference_id").val()==""){
			$("#reference_id_error").html("File Upload Reference No information is not filled.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("reference_id");
		}
		else{
			if(!regx.test($("#reference_id").val())){
				$("#reference_id_error").html("File Upload Reference No should be alphanumeric.").fadeIn().css("display","block");
				flag = 0;
				scroll.push("reference_id");
			}
		}
	}
	if(flag==1)
	{
		$("#master-data-form").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('.error-msg-no-data').addClass('now-Show');
		$('html, body').animate({
			scrollTop: $("#certificate_checkbox_table").offset().top-$("nav").height()
		}, 1200);
	}
	scroll_error(scroll);
}

function processDateFormat(date) {
	var parts = date.split("/");
	return new Date(parts[2], parts[0] - 1, parts[1]);
}

function newFunctionForPopUp() {
    console.log("newFunctionForPopUp");
	var SelectedOption = $('.filter-option').text();

	if ( SelectedOption.indexOf('Verified') > -1 ) {
		console.log("Verified");
		updateStatus('approve')
	} else if ( SelectedOption.indexOf('Rejected') > -1 ) {
		console.log("Rejected");
		updateStatus('reject')
	}
}


function grievanceRedressalSubmit(){
	var scroll = [];
	var flag=1;
	var mobregx = /^[0-9]+$/;
	$(".field-error").html("");
	/*if($("#categoryuser").val()==""){
		$("#categoryuser_error").html("Category of the User information is not filled.").show();
		scroll.push("categoryuser");
		flag = 0;
	}
	if($("#name").val()==""){
		flag = 0;
		$("#name_error").html("Name information is not filled.").show();
		scroll.push("name");
	}
	if($("#user_id").val()==""){
		flag = 0;
		$("#user_id_error").html("User Id information is not filled.").show();
		scroll.push("user_id");
	}
	if($("#role").val()==""){
		flag = 0;
		$("#role_error").html("Role information is not filled.").show();
		scroll.push("role");
	}
	if($("#c_email_id").val()==""){
		scroll.push('c_email_id');
		$("#c_email_id_error").html("Email information is not filled.").show();
		flag = 0;
	}
	else if($("#c_email_id").val()!="" && (!isEmail($("#c_email_id").val()) || $("#c_email_id").val().length<7)){
		$("#c_email_id_error").html("Please check the email ID provided, it looks incorrect.").show();
		flag = 0;
		scroll.push('c_email_id');
	}
	if($("#mobno").val()==""){
		scroll.push('mobno');
		$("#mobno_error").html("Mobile No information is not filled.").show();
		flag = 0;
	}
	else if($("#mobno").val().length!="10"){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = 0;
	}
	else if(!mobregx.test($("#mobno").val())){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = 0;
	}*/
	if($("#filterBy").val()=="Grievance"){
		if($("#type_of_grievance").val()==""){
			scroll.push('type_of_grievance');
			$("#type_of_grievance_error").html("Please select Type of Grievance.").show();
			flag = 0;
		}
		if($("#datails_of_party").val()==""){
			scroll.push('datails_of_party');
			$("#datails_of_party_error").html("Please select Details of the Party against whom the Grievance is faced.").show();
			flag = 0;
		}
		if($("#name_of_party").val()==""){
			scroll.push('name_of_party');
			$("#name_of_party_error").html("Please select Name of the Party against whom the Grievance is faced.").show();
			flag = 0;
		}
		if($("#officer_mobno").val()!="" && $("#officer_mobno").val().length!="10"){
			scroll.push('officer_mobno');
			$("#officer_mobno_error").html("Please enter valid Mobile No.").show();
			flag = 0;
		}
		else if($("#officer_mobno").val()!="" && !mobregx.test($("#officer_mobno").val())){
			scroll.push('officer_mobno');
			$("#officer_mobno_error").html("Please enter valid Mobile No.").show();
			flag = 0;
		}
		if($("#officer_email").val()!="" && (!isEmail($("#officer_email").val()) || $("#officer_email").val().length<7)){
			$("#officer_email_error").html("Please check the email ID provided, it looks incorrect.").show();
			flag = 0;
			scroll.push('officer_email');
		}
		if($("#grievance_description").val()==""){
			$("#grievance_description_error").html("Grievance Description information is not filled.").show();
			flag = 0;
			scroll.push('grievance_description');
		}
		if($("#expected_resolution_outcome").val()==""){
			$("#expected_resolution_outcome_error").html("Expected Resolution Outcome information is not filled.").show();
			flag = 0;
			scroll.push('expected_resolution_outcome');
		}
	}
	else if($("#filterBy").val()=="Feedback"){
		/*if($("#feedback").val()==""){
			$("#feedback_error").html("Feedback information is not filled.").show();
			flag = 0;
			scroll.push('feedback');
		}*/
	}
	else if($("#filterBy").val()=="Query"){
		if($("#type_of_query").val()==""){
			scroll.push('type_of_query');
			$("#type_of_query_error").html("Please select Type of Query.").show();
			flag = 0;
		}
		if($("#query_description").val()==""){
			$("#query_description_error").html("Query Description information is not filled.").show();
			flag = 0;
			scroll.push('query_description');
		}
		if($("#expected_information").val()==""){
			$("#expected_information_error").html("Expected Information information is not filled.").show();
			flag = 0;
			scroll.push('expected_information');
		}
	}
	var regx = /^[A-Za-z0-9]+$/;
	if($("#captchcode").val()==""){
		scroll.push('captchcode');
		$("#captchaCode-error").html("Please enter captcha code.").fadeIn().css("display","block");
		flag=0;
	}
	else if($("#captchcode").val().length!=5){
		scroll.push('captchcode');
		$("#captchaCode-error").html("Please enter valid captcha code.").fadeIn().css("display","block");
		flag=0;
	}
	else if(!regx.test($("#captchcode").val())){
		scroll.push('captchcode');
		$("#captchaCode-error").html("Please enter valid captcha code.").fadeIn().css("display","block");
		flag=0;
	}
	scroll_error(scroll);
	if(flag==1){
		$('#mygrievanceModal').modal('show');
	}
}

function nad_home_submit(){
	var scroll = [];
	var flag=1;
	var mobregx = /^[0-9]+$/;
	$(".field-error").html("");
	if($("#iama").val()==""){
		$("#iama_error").html("Please select I am a.").show();
		scroll.push("iama");
		flag = 0;
	}
	if($("#name").val()==""){
		$("#name_error").html("Name information is not filled.").show();
		scroll.push("name");
		flag = 0;
	}
	if($("#category").val()==""){
		$("#category_error").html("Please select category for query.").show();
		scroll.push("category");
		flag = 0;
	}
	if($("#mobno").val()!="" && $("#mobno").val().length!="10"){
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		scroll.push('mobno');
		flag = 0;
	}
	else if($("#mobno").val()!="" && !mobregx.test($("#mobno").val())){
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		scroll.push('mobno');
		flag = 0;
	}
	if($("#c_email_id").val()==""){
		$("#c_email_id_error").html("Email information is not filled.").show();
		scroll.push('c_email_id');
		flag = 0;
	}
	else if($("#c_email_id").val()!="" && (!isEmail($("#c_email_id").val()) || $("#c_email_id").val().length<7)){
		$("#c_email_id_error").html("Please check the email ID provided, it looks incorrect.").show();
		flag = 0;
		scroll.push('c_email_id');
	}
	scroll_error(scroll);
	if(flag==1){
		$('#succes_msgs').modal('show');
	}
}

function contact_us_submit(){
	var scroll = [];
	var flag=1;
	var mobregx = /^[0-9]+$/;
	$(".field-error").html("");
	if($("#iama").val()==""){
		$("#iama_error").html("Please select I am a.").show();
		scroll.push("iama");
		flag = 0;
	}
	/*if($("#name").val()==""){
		$("#name_error").html("Name information is not filled.").show();
		scroll.push("name");
	}*/
	if($("#category").val()==""){
		$("#category_error").html("Please select category for query.").show();
		scroll.push("category");
		flag = 0;
	}
	/*if($("#query").val()==""){
		$("#query_error").html("Query is not filled.").show();
		scroll.push("query");
	}*/
	if($("#mobno").val()==""){
		/*scroll.push('mobno');
		$("#mobno_error").html("Mobile No information is not filled.").show();
		flag = 0;*/
	}
	else if($("#mobno").val().length!="10"){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = 0;
	}
	else if(!mobregx.test($("#mobno").val())){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = 0;
	}
	if($("#c_email_id").val()==""){
		scroll.push('c_email_id');
		$("#c_email_id_error").html("Email information is not filled.").show();
		flag = 0;
	}
	else if($("#c_email_id").val()!="" && (!isEmail($("#c_email_id").val()) || $("#c_email_id").val().length<7)){
		$("#c_email_id_error").html("Please check the email ID provided, it looks incorrect.").show();
		flag = 0;
		scroll.push('c_email_id');
	}
	scroll_error(scroll);
	if(flag==1){
		$("#succes_msgs").show();
	}
}

function updateStatus(type){
	var scroll = [];
	$("#approveSubmit").attr('href','javascript:void(0)');
	$("#rejectSubmit").attr('href','javascript:void(0)');
	$(".field-error").html("");
	var flag=1;
	var atLeastOneIsChecked = false;
	$('input:radio').each(function () {
		if ($(this).is(':checked')) {
			atLeastOneIsChecked = true;
			return false;
		}
	});
	if(atLeastOneIsChecked==false)
	{
	console.log("atLeastOneIsChecked");
		flag=0;
		$("#masterFile_error").html("Please select a file").fadeIn().css("display","block");
		scroll.push("certificate_checkbox_table");
	}
	if(type=="reject" && $("#remark").val().trim()=="")
	{
	console.log("type==\"reject\"");
		$("#remark_error").html("Please enter remakrs").fadeIn().css("display","block");
		scroll.push("remark");
		flag=0;
	}
	scroll_error(scroll);
	if(flag==1)
	{
	console.log("flag==1");
		$("#fileNo").html($("#file_no").val());
		if(type=="approve"){
			$("#masterStatus").html("<span class='green_color'>Approved</span>");
			$("#approveSubmit").attr('href','#myModal');
			}else{
			$("#masterStatus").html("<span class='red_color'>Rejected</span>");
			$("#rejectSubmit").attr('href','#myModal');
		}
	}
}

function updateLinkStatus(){
	var scroll = [];
	$(".field-error").html("");
	var flag=1;
	var atLeastOneIsChecked = false;
	$('input:checkbox').each(function () {
		if ($(this).is(':checked')) {
			atLeastOneIsChecked = true;
			return false;
		}
	});
	if(atLeastOneIsChecked==false)
	{
		var flag=0;
		$("#masterFile_error").html("Please select a NAD certificate ID ").fadeIn().css("display","block");
		scroll.push("certificate_checkbox_table");
	}
	scroll_error(scroll);
	if(flag==1){
		$('#myModal').modal({
	        show: 'false'
	    });
	}
}

$("#approveReset").click(function(e){
	e.preventDefault();
	$("form input").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$("form select").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$(".field-error").html("");
	$('#approveMasterDataForm')[0].reset();
	$("#master-data-form").addClass("showHideMasterData");
	var dashboardContentHeight = $(".dashboard-content").height();
	$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
	$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	$("#filterBy").change();
	$("#filterBy1").change();
});

function setFileNo(ref_no)
{
	$("#file_no").val(ref_no);
}
function viewPrintCertificate(type)
{
	var scroll = [];
	$(".field-error").html("");
	var flag=1;
	var atLeastOneIsChecked = false;
	$('input:radio').each(function () {
		if ($(this).is(':checked')) {
			atLeastOneIsChecked = true;
			return false;
		}
	});
	if(atLeastOneIsChecked==false)
	{
		var flag=0;
		$("#masterFile_error").html("Please select a file").fadeIn().css("display","block");
		scroll.push("certificate_checkbox_table");
	}
	scroll_error(scroll);
	if(flag==1){
		if(type=="print"){
			var win = window.open($("#approveSubmit").attr("pdf_path"), '_blank');
  			win.focus();
		}
	}
}
function downloadPrintFile()
{
	$(".field-error").html("");
	var flag=true;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	if($("#course").val()==""){
		$("#course_error").html("Any of the specified value need to be selected.").show();
		flag = false;
		scroll.push("course");
	}
	if($("#award_type").val()==""){
		$("#award_type_error").html("Any of the specified value need to be selected.").show();
		flag = false;
		scroll.push("award_type");
	}
	if($("#academic_year").val()==""){
		$("#academic_year_error").html("Any of the specified value need to be selected.").show();
		flag = false;
		scroll.push("academic_year");
	}
	if($("#semester").val()!=""){
		if(!regx.test($("#semester").val())){
			flag=false;
			$("#semester_error").html("Please enter valid Semester.").show();
			scroll.push('semester');
		}
	}
	if($("#from").val()!=""){
		if(!regx.test($("#from").val())){
			flag=false;
			$("#from_error").html("Please enter valid From.").show();
			scroll.push('from');
		}
	}
	if($("#to").val()!=""){
		if(!regx.test($("#to").val())){
			flag=false;
			$("#to_error").html("Please enter valid To.").show();
			scroll.push('to');
		}
	}
	if($("#file_format").val()==""){
		$("#file_format_error").html("Any of the specified value need to be selected.").show();
		flag = false;
		scroll.push("file_format");
	}
	scroll_error(scroll);
}
function verifiermyinbox(){
	$(".field-error").html("");
	$("#viewverifiermyinbox").addClass("showHideMasterData");
	var scroll = [];
	var flag = 1;
	var regx1 = /^[0-9]+$/;
	var regx = /^[A-Za-z0-9]+$/;
	if($("#filterBy").val()=="transactionId"){
		if($("#transaction_id").val()==""){
			/*flag=0;
			$("#transaction_id_error").html("Transaction ID information is not filled.").css("display","block");
			scroll.push('transaction_id');*/
		}
		else if($("#transaction_id").val().length!="11"){
			flag=0;
			$("#transaction_id_error").html("Please enter valid Adhaar ID.").show();
			scroll.push('transaction_id');
		}
		else if(!regx1.test($("#transaction_id").val())){
			$("#transaction_id_error").html("Please enter valid Transaction ID.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("transaction_id");
		}
	}
	else if($("#filterBy").val()=="nadcertid"){
		/*if($("#nad_certificate_id").val()==""){
			flag=0;
			$("#nad_certificate_id_error").html("NAD Certificate ID is not filled.").css("display","block");
			scroll.push('nad_certificate_id');
		}*/
		if($("#nad_certificate_id").val()!="" && !regx.test($("#nad_certificate_id").val())){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").show();
			scroll.push('nad_certificate_id');
		}
		else if($("#nad_certificate_id").val()!="" && $("#nad_certificate_id").val().length!="15"){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").show();
			scroll.push('nad_certificate_id');
		}
	}
	else if($("#filterBy").val()=="AdhaarNo"){
		if($("#adhaar_id").val()==""){
			/*flag=0;
			$("#adhaar_id_error").html("Adhaar ID information is not filled.").css("display","block");
			scroll.push('adhaar_id');*/
		}
		else if($("#adhaar_id").val().length!="12"){
			flag=0;
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").show();
			scroll.push('adhaar_id');
		}
		else if(!regx1.test($("#adhaar_id").val())){
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("adhaar_id");
		}

	}
	else if($("#filterBy").val()=="NadId"){
		if($("#nad_id").val()==""){
			/*flag=0;
			$("#nad_id_error").html("NAD ID is not filled.").css("display","block");
			scroll.push('nad_id');*/
		}
		else if($("#nad_id").val().length!="14"){
			flag = 0;
			$("#nad_id_error").html("Please enter valid NAD ID.").show();
			scroll.push("nad_id");
		}
		else{
			var str = $("#nad_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag = 0;
					$("#nad_id_error").html("Please enter valid NAD ID1.").show();
					scroll.push("nad_id");
				}
			}
			else{
				flag = 0;
				$("#nad_id_error").html("Please enter valid NAD ID2.").show();
				scroll.push("nad_id");
			}
		}
	}
	if(flag==1 && (($("#transaction_id").val()=="" && $("#filterBy").val()=="transactionId") || ($("#nad_certificate_id").val()=="" && $("#filterBy").val()=="nadcertid") || ($("#adhaar_id").val()=="" && $("#filterBy").val()=="AdhaarNo") || ($("#nad_id").val()=="" && $("#filterBy").val()=="NadId"))){
		if($("#type_of_request").val()==""){
			$("#type_of_request_error").html("Any of the specified value need to be selected.").show();
			flag = 0;
			scroll.push("type_of_request");
		}
		var d=new Date();
		var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();

		/*if($("#to_date").val()==""){
			$("#to_date_error").html("Please select To Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("to_date");
		}
		if($("#from_date").val()==""){
			$("#from_date_error").html("Please select From Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("from_date");
		}*/
		if($("#to_date").val()!="" && $("#from_date").val()!="")
		{
			if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
				$("#from_date_error").html("From date should not be greater than to date").fadeIn().css("display","block");
				flag=0;
				}else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
				$("#from_date_error").html("From date should not be more than 90 day old").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
			else if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
				$("#from_date_error").html("From Date should not be greater than current date").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
			else if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
				$("#to_date_error").html("To Date should not be greater than current date.").fadeIn().css("display","block");
				scroll.push("to_date");
				flag=0;
			}

		}
		else if($("#to_date").val()!=""){
			if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
				$("#to_date_error").html("To Date should not be greater than current date.").fadeIn().css("display","block");
				scroll.push("to_date");
				flag=0;
			}
		}
		else if($("#from_date").val()!=""){
			if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
				$("#from_date_error").html("From Date should not be greater than current date").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
		}
	}
	scroll_error(scroll);
	if(flag==1){
		$("#viewverifiermyinbox").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewverifiermyinbox").offset().top-$("nav").height()
		}, 1200);
	}
}
function viewUploadedCertificate()
{
	$(".search-text-toggle").fadeOut();
	$("#certificate-data-form").addClass("showHideMasterData");
	$(".field-error").html("");
	var regx = /^[A-Za-z0-9]+$/;
	var regx1 = /^[0-9]+$/;
	var student_regx = /^[A-Za-z0-9 ]+$/;
	var flag=1;
	var scroll = [];
	if($("#filterBy").val()=="nadcertid")
	{
		if($("#nad_certificate_id").val()==""){
			flag=0;
			$("#nad_certificate_id_error").html("NAD Certificate ID is not filled.").css("display","block");
			scroll.push('nad_certificate_id');
		}
		else if($("#nad_certificate_id").val().length!="15"){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").show();
			scroll.push('nad_certificate_id');
		}
		else{
			var str = $("#nad_certificate_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag=0;
					$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
					scroll.push('nad_certificate_id');
				}
			}
			else{
				flag=0;
				$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
				scroll.push('nad_certificate_id');
			}
		}
	}
	else if($("#filterBy").val()=="AdhaarNo"){
		if($("#adhaar_id").val()==""){
			flag=0;
			$("#adhaar_id_error").html("Adhaar ID is not filled.").css("display","block");
			scroll.push('adhaar_id');
		}
		else if($("#adhaar_id").val().length!="12"){
			flag=0;
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").show();
			scroll.push('adhaar_id');
		}
		else if(!regx1.test($("#adhaar_id").val())){
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("adhaar_id");
		}
	}
	else if($("#filterBy").val()=="NadId"){
		if($("#uploaded_nad_id").val()==""){
			flag=0;
			$("#uploaded_nad_id_error").html("NAD ID is not filled.").css("display","block");
			scroll.push('uploaded_nad_id');
		}
		else if($("#uploaded_nad_id").val().length!="12"){
			flag=0;
			$("#uploaded_nad_id_error").html("Please enter valid NAD ID.").show();
			scroll.push('uploaded_nad_id');
		}
		else if(!regx.test($("#uploaded_nad_id").val())){
			$("#uploaded_nad_id_error").html("Please enter valid NAD ID.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("uploaded_nad_id");
		}
	}
	else if($("#filterBy").val()=="studentname"){
		if($("#student_name").val()==""){
			flag=0;
			$("#student_name_error").html("Student Name is not filled.").css("display","block");
			scroll.push('student_name');
		}
		else if(!student_regx.test($("#student_name").val())){
			$("#student_name_error").html("Please enter valid Student Name.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("student_name");
		}
	}
	else if($("#filterBy").val()=="Course"){
		if($("#course").val()==""){
			flag=0;
			$("#course_error").html("Course is not filled.").css("display","block");
			scroll.push('course');
		}
		if($("#award_type").val()==""){
			flag=0;
			$("#award_type_error").html("Award Type is not filled.").css("display","block");
			scroll.push('award_type');
		}
		if($("#academic_year").val()==""){
			flag=0;
			$("#academic_year_error").html("Academic Year is not filled.").css("display","block");
			scroll.push('academic_year');
		}
		if($("#academic_month").val()==""){
			flag=0;
			$("#academic_month_error").html("Academic Month is not filled.").css("display","block");
			scroll.push('academic_month');
		}
		if($("#refno").val()==""){
			flag=0;
			$("#refno_error").html("Unique Ref No. is not filled.").css("display","block");
			scroll.push('refno');
		}
		else if(!regx.test($("#refno").val())){
			$("#refno_error").html("Please enter valid Unique Ref No.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("refno");
		}
	}
	scroll_error(scroll);
	if(flag==1)
	{
		//$(".search-text-toggle").fadeIn();
		$("#certificate-data-form").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#certificate-data-form").offset().top-$("nav").height()
		}, 1200);
	}
	else{
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	}
}
/*Approve Certificate data */
function searchCertificateData()
{
	var scroll = [];
	$(".table-3").hide();
	$(".view-details").show();
	$("#certificate-data-form").addClass("showHideMasterData");
	$(".field-error").html("");
	var regx = /^[A-Za-z0-9]+$/;
	var flag=1;
	if($("#filterBy").val()=="awardType")
	{
		var d=new Date();
		var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();

		if($("#to_date").val()==""){
			$("#to_date_error").html("Please select To Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("to_date");
		}
		if($("#from_date").val()==""){
			$("#from_date_error").html("Please select From Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("from_date");
		}
		if($("#to_date").val()!="" && $("#from_date").val()!="")
		{
			if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
				$("#from_date_error").html("From date should not be greater than to date").fadeIn().css("display","block");
				flag=0;
				}else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
				$("#from_date_error").html("From date should not be more than 90 day old").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
			else if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
				$("#from_date_error").html("To Date should not be greater than current date").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
			else if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
				$("#to_date_error").html("To Date should not be greater than current date.").fadeIn().css("display","block");
				scroll.push("to_date");
				flag=0;
			}

		}
	}
	else if($("#filterBy").val()=="fileUploadReference")
	{
		if($("#reference_id").val()=="")
		{
			$("#reference_id_error").html("File Upload Reference No information is not filled.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("reference_id");
		}
		else if(!regx.test($("#reference_id").val())){
			$("#reference_id_error").html("File Upload Reference No should be alphanumeric.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("reference_id");
		}
	}
		/*24/12/2016*/
	scroll_error(scroll);
	if(flag==1)
	{
		$("#certificate-data-form").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#certificate-data-form").offset().top-$("nav").height()
		}, 1200);
	}
}

function reset_form(formName){
	$("form input").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$("form textarea").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$("form select").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$('.selectpicker').val('');
	$('.selectpicker').selectpicker('refresh');
	$(".field-error").html("");
	$('#'+formName)[0].reset();
	$("#role_div").hide();
	$("#succss_msg").html("");
}

function resetForm(formName,divName)
{
	$(".status_details_txt").hide();
	// $(".hideSubmitForm").hide();
	$(".certificate_upload_data").hide();
	$(".xml_file_value").text('');
	$(".field-error").html("");
	$(".search-text-toggle").fadeOut();
	$("form input").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$("form textarea").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$("form select").each(function(){
		if(typeof $(this).attr("id") !== "undefined")
		$('#'+$(this).attr("id")).removeClass('error-focused')
	});
	$(".table-3").hide();
	$(".field-error").html("");
	$('#'+formName)[0].reset();
	$('#filterBy').change();
	$("#"+divName).addClass("showHideMasterData");
	//alert(divName);
	var dashboardContentHeight = $(".dashboard-content").height();
	$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
	$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	$('#'+formName+' input').attr('disabled', false);
	$('#'+formName+' select').attr('disabled',false);
	$('.selectpicker').selectpicker('refresh');
	$('.dummy-placeholder').show();
	$("#file_format").change();
	$("#method_of_creation").change();
}

function viewUploadedCertificateData()
{
	$("#viewUploadCertificateData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
}

function submit_reject_reason()
{
	$(".field-error").html("");
	var flag=1;
	if($("#reject_reason").val()==""){
		flag=0;
		$("#reject_reason_error").html("Please enter reason for rejecting a request.").show();
	}
	if(flag==1){
		$("#reject_reason").val("");
		window.location.reload();
	}
}

function forgotuservalidation(){
	$(".field-error").html("");
	$("#success_msg").hide();
	var scroll = [];
	var flag=1;
	var mobregx = /^[0-9]+$/;
	if($("#category").val()==""){
		$("#category_error").html("Please select category.").fadeIn().css("display","block");
		flag=0;
		scroll.push("category");
	}
	if($("#from_date").val()==""){
		$("#from_date_error").html("Please select Date of Birth.").fadeIn().css("display","block");
		flag=0;
		scroll.push("from_date");
	}
	var age = getAge($("#from_date").val());
	if(parseInt(age)<parseInt(10) || parseInt(age)>parseInt(75)){
		$("#from_date_error").html("Please check the date provided.").fadeIn().css("display","block");
		flag = 0;
		scroll.push("from_date");
	}
	var regx1 = /^[0-9]+$/;
	if($("#nad_id").val()=="" && $("#adhaar_id").val()==""){
		if($("#nad_id").val()==""){
			flag = 0;
			$("#nad_id_error").html("NAD ID information is not filled.").show();
			scroll.push("nad_id");
		}
		if($("#adhaar_id").val()==""){
			flag=0;
			$("#adhaar_id_error").html("Adhaar ID is not filled.").css("display","block");
			scroll.push('adhaar_id');
		}
	}
	else{
		var nad_flag = 1;
		var adhaar_flag = 1;
		if($("#adhaar_id").val()==""){
			/*flag=0;
			$("#adhaar_id_error").html("Adhaar ID is not filled.").css("display","block");
			scroll.push('adhaar_id');*/
		}
		else if($("#adhaar_id").val().length!="12"){
			adhaar_flag=0;
		}
		else if(!regx1.test($("#adhaar_id").val())){
			adhaar_flag = 0;
		}
		if($("#nad_id").val()==""){
			/*flag = 0;
			$("#nad_id_error").html("NAD ID information is not filled.").show();
			scroll.push("nad_id");*/
		}
		else if($("#nad_id").val().length!="14"){
			nad_flag = 0;
		}
		else{
			var str = $("#nad_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					nad_flag = 0;
				}
			}
			else{
				nad_flag = 0;
			}
		}

		if(adhaar_flag==0 && adhaar_flag==0){
			if($("#adhaar_id").val()==""){
				/*flag=0;
				$("#adhaar_id_error").html("Adhaar ID is not filled.").css("display","block");
				scroll.push('adhaar_id');*/
			}
			else if($("#adhaar_id").val().length!="12"){
				flag=0;
				$("#adhaar_id_error").html("Please enter valid Adhaar ID.").show();
				scroll.push('adhaar_id');
			}
			else if(!regx1.test($("#adhaar_id").val())){
				$("#adhaar_id_error").html("Please enter valid Adhaar ID.").fadeIn().css("display","block");
				flag = 0;
				scroll.push("adhaar_id");
			}
			if($("#nad_id").val()==""){
				/*flag = 0;
				$("#nad_id_error").html("NAD ID information is not filled.").show();
				scroll.push("nad_id");*/
			}
			else if($("#nad_id").val().length!="14"){
				flag = 0;
				$("#nad_id_error").html("Please check the date provided. Registration open for age between 10 years and 75 years.").show();
				scroll.push("nad_id");
			}
			else{
				var str = $("#nad_id").val().replace(/\s/g, '') ;
				var fisrtChar = str.charAt(0);
				if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
					var subStr = str.substring(1);
					if(!$.isNumeric(subStr)){
						flag = 0;
						$("#nad_id_error").html("Please enter valid NAD ID1.").show();
						scroll.push("nad_id");
					}
				}
				else{
					flag = 0;
					$("#nad_id_error").html("Please enter valid NAD ID2.").show();
					scroll.push("nad_id");
				}
			}
		}
	}
	/*if($("#mobno").val()==""){
		scroll.push('mobno');
		$("#mobno_error").html("Mobile No information is not filled.").show();
		flag = 0;
	}
	else if($("#mobno").val().length!="10"){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = 0;
	}
	else if(!mobregx.test($("#mobno").val())){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = 0;
	}*/
	scroll_error(scroll);
	if(flag==1){
		$("#success_msg").show();
	}
}

function role_creation(){
	$(".info-end-par").hide();
	$(".field-error").html("");
	var flag=true;
	var scroll = [];
	if($("#role_no").val()==""){
		scroll.push('role_no');
		$("#role_no_error").html("Role Name information is not filled.").show();
		flag = false;
	}
	var check_flag = false;
	var function_str = '';
	var values = $('input:checkbox:checked.certificate_checkbox').map(function () {
		if(function_str=="")
			function_str+=$(this).val();
		else
			function_str+=", "+$(this).val();
	  check_flag = true;
	}).get();
	if(!check_flag){
		flag = false;
		$("#certificate_checkbox_error").html("Please select atleast one role function.").show();
	}
	scroll_error(scroll);
	if(flag){
		$("#succssmsg").html("Role "+$("#role_no").val()+" has been created successfully with following functions:<br> "+function_str).show();
		$("#role_no").attr('disabled',true);
		$(".certificate_checkbox").attr('disabled',true);
	}
	return false;
}

function reg_complete(){
	$(".field-error").html("");
	$(".reg-success").hide();
	var mobile_regx=/[0-9]/;
	var flag=true;
	if($("#otp").val()==""){
		$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
		return false;
	}
	else if($("#otp").val().length!="6" || !mobile_regx.test($("#otp").val())){
		$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
		return false;
	}
	if(flag){
		$(".reg-success").show();
	}
}

function reg_complete1(){
	var mobile_regx=/[0-9]/;
	$(".field-error").html("");
	$(".reg-success").hide();
	var scroll = [];
	var flag=true;
	if($("#otp").val()==""){
		$("#otp_error").html("Please enter OTP.").fadeIn().css("display","block");
		flag = false;
		scroll.push('otp');
	}
	else if($("#otp").val().length!="6" || !mobile_regx.test($("#otp").val())){
		$("#otp_error").html("Please enter valid OTP.").fadeIn().css("display","block");
		flag = false;
		scroll.push('otp');
	}
	if($("#user_password").val()==""){
		$("#user_password_error").html("User Password information is not filled.").fadeIn().css("display","block");
		flag = false;
		scroll.push('user_password');
	}
	else {
		var regx = /^[A-Za-z0-9]+$/;
		if($("#user_password").val().length<8){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password length should be minimum 8.").css("display","block");
			flag = false;
			scroll.push('user_password');
		}
		else if($("#user_password").val().length>20){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password length should be maximum 20").css("display","block");
			flag = false;
			scroll.push('user_password');
		}
		else if(!regx.test($("#user_password").val())){
			$("#user_password_error").show();
			$("#user_password_error").html("User Password should contain only alphanumeric.").css("display","block");
			flag = false;
			scroll.push('user_password');
		}
	}

	if($("#confirm_password").val()==""){
		$("#confirm_password_error").show();
		$("#confirm_password_error").html("Confirm Password information is not filled.").css("display","block");
		flag = false;
		scroll.push('confirm_password');
	}

	if($("#user_password").val()!="" && $("#confirm_password").val()!=""){
		if($("#user_password").val()!=$("#confirm_password").val()){
			$("#confirm_password_error").show();
			$("#confirm_password_error").html("Hey the confirm password value does not match.. Enter the same password and yes do remember it").css("display","block");
			flag = false;
			scroll.push('confirm_password');
		}
	}
	if(flag){
		$(".reg-success").show();
	}
	return false;
}

function ai_reg_leg1(){
	$("#mobile-verify").hide();
	$(".reg-success").hide();
	$(".field-error").html("");
	var flag=true;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	var isAlphaSpace = /^[A-Za-z0-9 ]+$/;
	var mobregx = /^[0-9]+$/;
	if($("#admin_name").val()==""){
		flag = false;
		scroll.push('admin_name');
		$("#admin_name_error").html("Name of the Admin User information is not filled.").show();
	}
	else if(!isAlphaSpace.test($("#admin_name").val())){
		flag = false;
		scroll.push('admin_name');
		$("#admin_name_error").html("Please check the Name of the Admin User provided, it looks incorrect.").show();
	}
	if($("#designation").val()==""){
		flag = false;
		scroll.push('designation');
		$("#designation_error").html("Designation information is not filled.").show();
	}
	else if(!isAlphaSpace.test($("#designation").val())){
		flag = false;
		scroll.push('designation');
		$("#designation_error").html("Please check the Designation provided, it looks incorrect.").show();
	}
	if($("#empid").val()==""){
		scroll.push('empid');
		$("#empid_error").html("Employee ID information is not filled.").show();
		flag = false;
	}
	if($("#from_date").val()==""){
			/*scroll.push('dob');
			$("#dob_error").show();
			$("#dob_error").html("Date of Birth information is not filled.");
			flag = false;*/
	}
	else{
		var age = getAge($("#from_date").val());
		if(parseInt(age)<parseInt(10) || parseInt(age)>parseInt(75)){
			$("#dob_error").show();
			$("#dob_error").html("Please check the date provided. Registration open for age between 10 years and 75 years.");
			flag = false;
			scroll.push('from_date');
		}
		}
	if($("#mobno").val()==""){
		scroll.push('mobno');
		$("#mobno_error").html("Mobile No information is not filled.").show();
		flag = false;
	}
	else if($("#mobno").val().length!="10"){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = false;
	}
	else if(!mobregx.test($("#mobno").val())){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = false;
	}
	if($("#c_email_id").val()==""){
		scroll.push('c_email_id');
		$("#c_email_id_error").html("Email information is not filled.").show();
		flag = false;
	}
	else if($("#c_email_id").val()!="" && (!isEmail($("#c_email_id").val()) || $("#c_email_id").val().length<7)){
		$("#c_email_id_error").html("Please check the email ID provided, it looks incorrect.").show();
		flag = false;
		scroll.push('c_email_id');
	}
	if($("#user_id").val()==""){
		$("#user_id_error").show();
		$("#user_id_error").html("User ID information is not filled.");
		flag = false;
		scroll.push('user_id');
	}
	else if($("#user_id").val().length<6){
		$("#user_id_error").show();
		$("#user_id_error").html("Please enter valid user ID.");
		flag = false;
		scroll.push('user_id');
	}
	else{
		var regx = /^[A-Za-z0-9]+$/;
		if(!isEmail($("#user_id").val())){
			if(!regx.test($("#user_id").val())){
				$("#user_id_error").show();
				$("#user_id_error").html("User ID should contain only alphanumeric.");
				flag = false;
				scroll.push('user_id');
			}
		}
	}
	scroll_error(scroll);
	if(flag){
		$('#mobile-verify').show();
	}
}

function ai_reg_leg(){
	$("#mobile-verify").hide();
	$(".reg-success").hide();
	$(".field-error").html("");
	var flag=true;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	var isAlphaSpace = /^[A-Za-z0-9 ]+$/;
	var mobregx = /^[0-9]+$/;
	if($("#type_of_academic").val()==""){
		scroll.push('type_of_academic');
		$("#type_of_academic_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}
	if($("#type_of_academic").val()=="Board" && $("#type_of_board").val()==""){
		scroll.push('type_of_board');
		$("#type_of_board_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}
	if($("#type_of_academic").val()=="University" && $("#type_of_university").val()==""){
		scroll.push('type_of_university');
		$("#type_of_university_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}
	if($("#type_of_academic").val()=="Stand Alone Institute" && $("#type_of_stand_alone_institutions").val()==""){
		scroll.push('type_of_stand_alone_institutions');
		$("#type_of_stand_alone_institutions_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}
	if($("#state").val()==""){
		scroll.push('state');
		$("#state_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}
	if($("#name_of_academic").val()==""){
		scroll.push('name_of_academic');
		$("#name_of_academic_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}
	else if($("#name_of_academic").val()=="Other" && $("#name_of_academic_other").val()==""){
		scroll.push('name_of_academic_other');
		$("#name_of_academic_other_error").html("Name of Academic Institution- Others information is not filled.").show();
		flag = false;
	}
	/*if($("#type_of_ownership").val()==""){
		scroll.push('type_of_ownership');
		$("#type_of_ownership_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}*/
	/*if($("#gov_financial_support").val()==""){
		scroll.push('gov_financial_support');
		$("#gov_financial_support_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}*/
	/*if($("#ai_recognition").val()==""){
		scroll.push('ai_recognition');
		$("#ai_recognition_error").html("Any of the specified value need to be selected.").show();
		flag = false;
	}*/
	/*if($("#no_of_student").val()!="" && !regx.test($("#no_of_student").val())){
		scroll.push('no_of_student');
		$("#no_of_student_error").html("Please enter numeric value.").show();
		flag = false;
	}*/
	if($("#address1").val()==""){
		scroll.push('address1');
		$("#address1_error").html("Address 1 information is not filled.").show();
		flag = false;
	}
	if($("#address2").val()==""){
		scroll.push('address2');
		$("#address2_error").html("Address 2 information is not filled.").show();
		flag = false;
	}
	/*if($("#city").val()==""){
		scroll.push('city');
		$("#city_error").show();
		$("#city_error").html("City /Town /Village information is not filled.");
		flag = false;
	}*/
	if($("#district").val()==""){
		scroll.push('district');
		$("#district_error").show();
		$("#district_error").html("District information is not filled.");
		flag = false;
	}
	if($("#address_state").val()==""){
		scroll.push('address_state');
		$("#address_state_error").show();
		$("#address_state_error").html("Please select State.");
		flag = false;
	}
	if($("#pincode").val()==""){
		scroll.push('pincode');
		$("#pincode_error").html("Pincode information is not filled.").show();
		flag = false;
	}
	else if($("#pincode").val().length!=6){
		scroll.push('pincode');
		$("#pincode_error").html("Please enter valid pincode.").show();
		flag = false;
	}
	if($("#c_country").val()==""){
		scroll.push('c_country');
		$("#c_country_error").show().css("display","block").html("Country information is not filled.");
		flag = false;
	}
	if($("#phone_no").val()!="" && $("#phone_no").val().length!=15){
		scroll.push('phone_no');
		$("#phone_no_error").html("Please check the Phone Number provided, it looks incorrect.").show();
		flag = false;
	}
	if($("#email_id").val()!="" && (!isEmail($("#email_id").val()) || $("#email_id").val().length<7)){
		$("#email_id_error").html("Please check the email ID provided, it looks incorrect.").show();
		flag = false;
		scroll.push('email_id');
	}
	if($("#contact_person_name").val()==""){
		flag = false;
		scroll.push('contact_person_name');
		$("#contact_person_name_error").html("Contact Person (Nodal Officer) Name information is not filled.").show();
	}
	else if(!isAlphaSpace.test($("#contact_person_name").val())){
		flag = false;
		scroll.push('contact_person_name');
		$("#contact_person_name_error").html("Please check the Contact Person (Nodal Officer) Name provided, it looks incorrect.").show();
	}
	if($("#department").val()==""){
		flag = false;
		scroll.push('department');
		$("#department_error").html("Department information is not filled.").show();
	}
	if($("#designation").val()==""){
		flag = false;
		scroll.push('designation');
		$("#designation_error").html("Designation information is not filled.").show();
	}
	if($("#mobno").val()==""){
		scroll.push('mobno');
		$("#mobno_error").html("Mobile No information is not filled.").show();
		flag = false;
	}
	else if($("#mobno").val().length!="10"){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = false;
	}
	else if(!mobregx.test($("#mobno").val())){
		scroll.push('mobno');
		$("#mobno_error").html("Please enter valid Mobile No.").show();
		flag = false;
	}
	if($("#c_email_id").val()==""){
		scroll.push('c_email_id');
		$("#c_email_id_error").html("Email information is not filled.").show();
		flag = false;
	}
	else if($("#c_email_id").val()!="" && (!isEmail($("#c_email_id").val()) || $("#c_email_id").val().length<7)){
		$("#c_email_id_error").html("Please check the email ID provided, it looks incorrect.").show();
		flag = false;
		scroll.push('c_email_id');
	}
	if($("#supporting_doc").val()!="" && $("#supporting_doc").val().indexOf(' ')!==-1){
		scroll.push('supporting_doc');
		$("#supporting_doc_error").html("Please upload without space file name.").show();
		flag = false;
	}
	if($("#captchcode").val()==""){
		$("#captchcode_error").html("Please enter captcha code.").fadeIn().css("display","block");
		scroll.push('captchcode');
		flag = false;
	}
	else if(!regx.test($("#captchcode").val())){
		$("#captchcode_error").html("Invalid captcha code.").fadeIn().css("display","block");
		scroll.push('captchcode');
		flag = false;
	}
	scroll_error(scroll);
	if(flag){
		$('#myModal').modal('show');
	}

}



function varifier_registration(){
	$(".field-error").html("");
	$(".success-text").hide();
	var flag=true;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	var mobregx = /^[0-9]+$/;
	var selected = $(".nationality:checked").val();
	if($("#entity_name").val()==""){
		scroll.push('entity_name');
		$("#entity_name_error").html("Verifier Entity Name information is not filled.").show();
		flag = false;
	}
	else if(!regx.test($("#entity_name").val())){
		flag=0;
		$("#entity_name_error").html("Please enter valid Verifier Entity Name.").show();
		scroll.push('entity_name');
	}
	if($("#constitution").val()==""){
		scroll.push('constitution');
		$("#constitution_error").html("Please select the Verifier Entity Constitution.").show();
		flag = false;
	}
	if($("#category").val()==""){
		scroll.push('category');
		$("#category_error").html("Please select the Verifier Category.").show();
		flag = false;
	}
	if($("#address1").val()==""){
		scroll.push('address1');
		$("#address1_error").html("Address 1 information is not filled.").show();
		flag = false;
	}
	if($("#address2").val()==""){
		scroll.push('address2');
		$("#address2_error").html("Address 2 information is not filled.").show();
		flag = false;
	}
	if($("#pincode").val()=="" && selected=="Indian"){
		scroll.push('pincode');
		$("#pincode_error").show();
		$("#pincode_error").html("Pincode information is not filled.");
		flag = false;
	}
	else if($("#pincode").val().length!=6 && selected=="Indian"){
		scroll.push('pincode');
		$("#pincode_error").show();
		$("#pincode_error").html("Please enter valid pincode.");
		flag = false;
	}
	if($("#city").val()=="" && selected=="Indian"){
		scroll.push('city');
		$("#city_error").show();
		$("#city_error").html("City /Town /Village information is not filled.");
		flag = false;
	}
	if($("#state").val()=="" && selected=="Indian"){
		scroll.push('state');
		$("#state_error").show();
		$("#state_error").html("City /Town /Village information is not filled.");
		flag = false;
	}
	if($("#c_email_id").val()!="" && (!isEmail($("#c_email_id").val()) || $("#c_email_id").val().length<7)){
		$("#c_email_id_error").show().css("display","block");
		$("#c_email_id_error").html("Please check the email ID provided, it looks incorrect.");
		flag = false;
		scroll.push('email_id');
	}
	if($("#phone_no").val()!="" && $("#phone_no").val().length!=10){
		scroll.push('phone_no');
		$("#phone_no_error").show();
		$("#phone_no_error").html("Please check the Office Phone Number provided, it looks incorrect.");
		flag = false;
	}
	if($("#person_name").val()==""){
		scroll.push('person_name');
		$("#person_name_error").show();
		$("#person_name_error").html("Nodal Person Name information is not filled.");
		flag = false;
	}
	if($("#department").val()==""){
		scroll.push('department');
		$("#department_error").show();
		$("#department_error").html("Department information is not filled.");
		flag = false;
	}
	if($("#designation").val()==""){
		scroll.push('designation');
		$("#designation_error").show();
		$("#designation_error").html("Designation information is not filled.");
		flag = false;
	}
	if($("#v_mobile_number").val()==""){
		scroll.push('v_mobile_number');
		$("#mobile_number_error").show();
		$("#mobile_number_error").html("Mobile No information is not filled.");
		flag = false;
	}
	else if($("#v_mobile_number").val().length!="10"){
		scroll.push('v_mobile_number');
		$("#mobile_number_error").show();
		$("#mobile_number_error").html("Please enter valid Mobile No.");
		flag = false;
	}
	else if(!mobregx.test($("#v_mobile_number").val())){
		scroll.push('v_mobile_number');
		$("#mobile_number_error").show();
		$("#mobile_number_error").html("Please enter valid Mobile No.");
		flag = false;
	}
	if($("#email_id").val()==""){
		$("#email_id_error").show().css("display","block");
		$("#email_id_error").html("Email ID information is not filled.");
		flag = false;
		scroll.push('email_id');
	}
	else if(!isEmail($("#email_id").val()) || $("#email_id").val().length<7){
		$("#email_id_error").show().css("display","block");
		$("#email_id_error").html("Please check the email ID provided, it looks incorrect.");
		flag = false;
		scroll.push('email_id');
	}
	if($("#identity_document").val()==""){
		scroll.push('identity_document');
		$("#identity_document_error").html("Please select the Proof of Identity Document.").show();
		flag = false;
	}
	if($("#address_document").val()==""){
		scroll.push('address_document');
		$("#address_document_error").html("Please select the Proof of Address Document.").show();
		flag = false;
	}
	if(!document.getElementById('checkboxInput').checked){
		$("#term_error").show().css({"display":"block","clear":"both"});
		$("#term_error").html("You must agree terms and conditions before registration.");
		flag = false;
	}
	if(selected=="Foreigner"){
		if($("#country").val()==""){
			$("#country_error").show().css("display","block").html("Country information is not filled.");
			flag = false;
		}
	}
	scroll_error(scroll);
	if(flag){
		$('#validation-modal').modal('show');
		$(".success-text").show();
	}
	return false;
}

function linkingRequestData()
{
	$("#viewOnlineVerirficationData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	if($("#nad_certificate_id").val()=="" && $("#unique_ref_no").val()==""){
		flag=0;
		$("#nad_ref_error").html("Either of the search criteria i.e. NAD Certificate ID or Academic Details is mandatory to search record.").show();
		scroll.push('nad_certificate_id');
		scroll.push('unique_ref_no');
	}
	else{
		if($("#nad_certificate_id").val()==""){
			if($("#university").val().trim()==""){
				flag=0;
				$("#university_error").html("Please select the University.").css("display","block");
				scroll.push('university');
			}
			if($("#course").val().trim()==""){
				flag=0;
				$("#course_error").html("Please select the course name.").css("display","block");
				scroll.push('course');
			}
			if($("#academic_year").val().trim()==""){
				flag=0;
				$("#academic_year_error").html("Please select the Academic Year").css("display","block");
				scroll.push('academic_year');
			}
			if($("#academic_month").val()==""){
				flag=0;
				$("#academic_month_error").html("Academic Month is not filled.").css("display","block");
				scroll.push('academic_month');
			}
			if($("#unique_ref_no").val()==""){
				$("#unique_ref_no_error").html("Please enter a Unique Ref No.").show();
				flag = 0;
				scroll.push('unique_ref_no');
			}
		}
		else if(!regx.test($("#nad_certificate_id").val())){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
			scroll.push('nad_certificate_id');
		}
		else if($("#nad_certificate_id").val().length!=12){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
			scroll.push('nad_certificate_id');
		}
	}
	scroll_error(scroll);
	if(flag==1){
		$("#viewOnlineVerirficationData").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
		}, 1200);
	}
}

function create_req()
{
	$("#certificate_checkbox_error").html("");
	$("#req_error").html("");
	var check_flag = false;
	var values = $('input:checkbox:checked.certificate_checkbox').map(function () {
	  check_flag = true;
	}).get();
	if(!check_flag){
		flag = false;
		$("#certificate_checkbox_error").html("Please select atleast one certificate.").show();
	}
	if ($("input[name='masterFile']:checked").val() && check_flag) {
		$("#req_error").html("");
		location.href = "verifier_my_profile_6_2.html";
	}
	else{
		if(!$("input[name='masterFile']:checked").val())
			$("#req_error").html("Please select option.").show();
	}
}

function actiontakensubmit()
{
	var flag=1;
	var scroll = [];
	$(".field-error").html("");
	if($("#isreg").val()==""){
		$("#isreg_error").html("Any of the specified value need to be selected").show();
		scroll.push("isreg");
		flag=0;
	}
	if($("#nameiscorrect").val()==""){
		$("#nameiscorrect_error").html("Any of the specified value need to be selected").show();
		scroll.push("nameiscorrect");
		flag=0;
	}
	if($("#isvalid").val()==""){
		$("#isvalid_error").html("Any of the specified value need to be selected").show();
		scroll.push("isvalid");
		flag=0;
	}
	if($("#webischeck").val()==""){
		$("#webischeck_error").html("Any of the specified value need to be selected").show();
		scroll.push("webischeck");
		flag=0;
	}
	if($("#aiappstatus").val()==""){
		$("#aiappstatus_error").html("Any of the specified value need to be selected").show();
		scroll.push("aiappstatus");
		flag=0;
	}
	if($("#feereceipt").val()==""){
		$("#feereceipt_error").html("Any of the specified value need to be selected").show();
		scroll.push("feereceipt");
		flag=0;
	}
	if($("#agreementstatus").val()==""){
		$("#agreementstatus_error").html("Any of the specified value need to be selected").show();
		scroll.push("agreementstatus");
		flag=0;
	}
	if($("#nodalofficer").val()==""){
		$("#nodalofficer_error").html("Details of NAD Nodal Officer information is not filled.").show();
		scroll.push("nodalofficer");
		flag=0;
	}
	if($("#action_token").val()=="Raise Query" && $("#remark").val()==""){
		$("#remark_error").html("Remark is not filled.").show();
		scroll.push("remark");
		flag=0;
	}
	scroll_error(scroll);
}

function verifynadidsubmit()
{
	$(".field-error").html("");
	$("#verifySubmit").attr('href','javascript:void(0)');
	if($("#action_token").val()=="Rejected" && $("#remark").val()==""){
		$("#remark_error").html("Remark is not filled.").show();
	}
	else{
		//ModalForVerify

		$(".student_name_msg").text($("#student_name").val());
		if($("#action_token").val()=="Rejected"){
			$(".success-para").hide();
			$(".rejected-para").show();
		}
		else{
			$(".success-para").show();
			$(".rejected-para").hide();
		}
		$(".green_color").text($("#action_token").val());
		$("#verifySubmit").attr('href','#ModalForVerify');
	}
}

function verifynadidstudsubmit()
{
	$(".field-error").html("");
	$("#verifySubmit").attr('href','javascript:void(0)');
	if($("#action_token").val()=="Rejected" && $("#remark").val()==""){
		$("#remark_error").html("Remark is not filled.").show();
	}
	else{
		//ModalForVerify
		if($("#filterBy1").val()=="nadid")
			$(".nad_id_msg").text($("#nad_id").val().toUpperCase());
		else
			$(".nad_id_msg").text($("#adhaar_id").val());
		$(".student_name_msg").text($("#student_name").val());
		if($("#action_token").val()=="Rejected"){
			$(".success-para").hide();
			$(".rejected-para").show();
			$(".Rejected-content").show();
			$(".Success-content").hide();
		}
		else{
			$(".success-para").show();
			$(".rejected-para").hide();
			$(".Success-content").show();
			$(".Rejected-content").hide();
		}
		//$(".green_color").text($("#action_token").val());
		$("#verifySubmit").attr('href','#ModalForVerify');
	}
}

function seeding_nad_id(){
	$(".hideSubmitForm").hide();
	$("#viewOnlineVerirficationData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	var regx1 = /^[0-9]+$/;
	if($("#filterBy").val()=="nad_cert_id")
	{
		if($("#nad_certificate_id").val()==""){
			flag=0;
			$("#nad_certificate_id_error").html("NAD Certificate ID is not filled.").css("display","block");
			scroll.push('nad_certificate_id');
		}
		else if($("#nad_certificate_id").val().length!=15){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").css("display","block");
			scroll.push('nad_certificate_id');
		}
		else if(!regx.test($("#nad_certificate_id").val())){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").show();
			scroll.push('nad_certificate_id');
		}
	}
	else if($("#filterBy").val()=="fileUploadReference")
	{
		if($("#course").val()==""){
			flag=0;
			$("#course_error").html("Any of the specified value need to be selected.").css("display","block");
			scroll.push('course');
		}
	}
	else if($("#filterBy").val()=="fileUploadReference1")
	{
		if($("#award_type").val()==""){
			flag=0;
			$("#award_type_error").html("Any of the specified value need to be selected.").css("display","block");
			scroll.push('award_type');
		}
	}
	else if($("#filterBy").val()=="fileUploadReference2")
	{
		if($("#academic_year").val()==""){
			flag=0;
			$("#academic_year_error").html("Any of the specified value need to be selected.").css("display","block");
			scroll.push('academic_year');
		}
	}
	else if($("#filterBy").val()=="fileUploadReference3")
	{
		if($("#academic_month").val()==""){
			flag=0;
			$("#academic_month_error").html("Any of the specified value need to be selected.").css("display","block");
			scroll.push('academic_month');
		}
	}
	else if($("#filterBy").val()=="fileUploadReference4")
	{
		if($("#ref_no").val()==""){
			flag=0;
			$("#ref_no_error").html("Unique Reference No. is not filled.").css("display","block");
			scroll.push('ref_no');
		}
		else{
			var letters = /^[a-zA-Z0-9]+$/;
			if(!(letters.test($("#ref_no").val()))){
				$("#ref_no_error").html("Please enter valid Unique Reference No.");
				flag = false;
				scroll.push("ref_no");
			}
		}
	}

	scroll_error(scroll);
	if(flag==1){
		$("#viewOnlineVerirficationData").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
		}, 1200);
	}
}
function verifynadidupdation(){
	$(".hideSubmitForm").hide();
	$("#viewOnlineVerirficationData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
	var scroll = [];
	if($("#nad_id").val()==""){
		flag = 0;
		$("#nad_id_error").html("NAD ID information is not filled.").show();
		scroll.push("nad_id");
	}
	else if($("#nad_id").val().length!="14"){
		flag = 0;
		$("#nad_id_error").html("Please enter valid NAD ID.").show();
		scroll.push("nad_id");
	}
	else{
		var str = $("#nad_id").val().replace(/\s/g, '') ;
		var fisrtChar = str.charAt(0);
		if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
			var subStr = str.substring(1);
			if(!$.isNumeric(subStr)){
				flag = 0;
				$("#nad_id_error").html("Please enter valid NAD ID1.").show();
				scroll.push("nad_id");
			}
		}
		else{
			flag = 0;
			$("#nad_id_error").html("Please enter valid NAD ID2.").show();
			scroll.push("nad_id");
		}
	}
	scroll_error(scroll);

	if(flag==1){
		$(".hideSubmitForm").show();
		$("#viewOnlineVerirficationData").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
		}, 1200);
	}
}
function verifynadid()
{
	$(".hideSubmitForm").hide();
	$("#viewOnlineVerirficationData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
	var scroll = [];
	if($("#acknowledgement_no").val()==""){
		flag=0;
		$("#acknowledgement_no_error").html("Acknowledgement No is not filled.").css("display","block");
		scroll.push('acknowledgement_no');
	}
	scroll_error(scroll);

	if(flag==1){
		$(".hideSubmitForm").show();
		$("#viewOnlineVerirficationData").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
		}, 1200);
	}
}

function view_acnt_details1()
{
	$(".hideSubmitForm").hide();
	$(".field-error").html("");
	var flag=1;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	var regx1 = /^[0-9]+$/;
	if($("#filterBy1").val()=="Aadhaar_NO"){
		if($("#adhaar_id").val()==""){
			flag=0;
			$("#adhaar_id_error").html("Adhaar ID is not filled.").css("display","block");
			scroll.push('adhaar_id');
		}
		else if($("#adhaar_id").val().length!="12"){
			flag=0;
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").show();
			scroll.push('adhaar_id');
		}
		else if(!regx1.test($("#adhaar_id").val())){
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("adhaar_id");
		}
	}
	else if($("#filterBy1").val()=="nadid"){
		if($("#nad_id").val()==""){
			flag = 0;
			$("#nad_id_error").html("NAD ID information is not filled.").show();
			scroll.push("nad_id");
		}
		else if($("#nad_id").val().length!="14"){
			flag = 0;
			$("#nad_id_error").html("Please enter valid NAD ID.").show();
			scroll.push("nad_id");
		}
		else{
			var str = $("#nad_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag = 0;
					$("#nad_id_error").html("Please enter valid NAD ID1.").show();
					scroll.push("nad_id");
				}
			}
			else{
				flag = 0;
				$("#nad_id_error").html("Please enter valid NAD ID2.").show();
				scroll.push("nad_id");
			}
		}
	}
	scroll_error(scroll);

	if(flag==1){
		$(".hideSubmitForm").show();
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		/*$('html, body').animate({
			scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
		}, 1200);*/
	}
}

function view_acnt_details()
{
	$("#viewOnlineVerirficationData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	var regx1 = /^[0-9]+$/;
	if($("#filterBy").val()=="Aadhaar_NO"){
		if($("#adhaar_id").val()==""){
			flag=0;
			$("#adhaar_id_error").html("Adhaar ID is not filled.").css("display","block");
			scroll.push('adhaar_id');
		}
		else if($("#adhaar_id").val().length!="12"){
			flag=0;
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").show();
			scroll.push('adhaar_id');
		}
		else if(!regx1.test($("#adhaar_id").val())){
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("adhaar_id");
		}
	}
	else if($("#filterBy").val()=="nadid"){
		if($("#nad_id").val()==""){
			flag = 0;
			$("#nad_id_error").html("NAD ID information is not filled.").show();
			scroll.push("nad_id");
		}
		else if($("#nad_id").val().length!="14"){
			flag = 0;
			$("#nad_id_error").html("Please enter valid NAD ID.").show();
			scroll.push("nad_id");
		}
		else{
			var str = $("#nad_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag = 0;
					$("#nad_id_error").html("Please enter valid NAD ID1.").show();
					scroll.push("nad_id");
				}
			}
			else{
				flag = 0;
				$("#nad_id_error").html("Please enter valid NAD ID2.").show();
				scroll.push("nad_id");
			}
		}
	}

	scroll_error(scroll);

	if(flag==1){
		$("#viewOnlineVerirficationData").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
		}, 1200);
	}
}

function verifier_my_profile(text)
{

	$("#viewOnlineVerirficationData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;
	var regx1 = /^[0-9]+$/;
	if($("#filterBy").val()=="nad_cert_id")
	{
		if($("#nad_certificate_id").val()==""){
			flag=0;
			$("#nad_certificate_id_error").html("NAD Certificate ID is not filled.").css("display","block");
			scroll.push('nad_certificate_id');
		}
		else if(!regx.test($("#nad_certificate_id").val())){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").show();
			scroll.push('nad_certificate_id');
		}
		else if($("#nad_certificate_id").val().length!="15"){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").show();
			scroll.push('nad_certificate_id');
		}
	}
	else if($("#filterBy").val()=="Aadhaar_NO"){
		if($("#adhaar_id").val()==""){
			flag=0;
			$("#adhaar_id_error").html("AADHAAR No. cannot be blank.").css("display","block");
			scroll.push('adhaar_id');
		}
		else if($("#adhaar_id").val().length!="12"){
			flag=0;
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").show();
			scroll.push('adhaar_id');
		}
		else if(!regx1.test($("#adhaar_id").val())){
			$("#adhaar_id_error").html("Please enter valid Adhaar ID.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("adhaar_id");
		}
	}
	else if($("#filterBy").val()=="nadid"){
		if($("#nad_id").val()==""){
			flag = 0;
			$("#nad_id_error").html("NAD ID information is not filled.").show();
			scroll.push("nad_id");
		}
		else if($("#nad_id").val().length!="14"){
			flag = 0;
			$("#nad_id_error").html("Please enter valid NAD ID.").show();
			scroll.push("nad_id");
		}
		else{
			var str = $("#nad_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag = 0;
					$("#nad_id_error").html("Please enter valid NAD ID1.").show();
					scroll.push("nad_id");
				}
			}
			else{
				flag = 0;
				$("#nad_id_error").html("Please enter valid NAD ID2.").show();
				scroll.push("nad_id");
			}
		}
	}
	else if($("#filterBy").val()=="academic_details"){
		if($("#unique_ref_no").val()==""){
			$("#unique_ref_no_error").html("Please enter a Unique Ref No.").show();
			flag = 0;
			scroll.push('unique_ref_no');
		}
	}
	scroll_error(scroll);
	if(flag==1){
		$("#viewOnlineVerirficationData").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
		}, 1200);
	}
}

function onlineVerificationData()
{
	$("#viewOnlineVerirficationData").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=1;
	var scroll = [];
	var regx = /^[A-Za-z0-9]+$/;

	if($("#filterBy").val()=="CertificateType")
	{
		if($("#university").val().trim()==""){
			flag=0;
			$("#university_error").html("Any of the specified value need to be selected").css("display","block");
			scroll.push('university');
		}

		if($("#course").val().trim()==""){
			flag=0;
			$("#course_error").html("Any of the specified value need to be selected").css("display","block");
			scroll.push('course');
		}

		if($("#certificate_type").val().trim()==""){
			flag=0;
			$("#certificate_type_error").html("Any of the specified value need to be selected").css("display","block");
			scroll.push('certificate_type');
		}

		if($("#academic_year").val().trim()==""){
			flag=0;
			$("#academic_year_error").html("Any of the specified value need to be selected").css("display","block");
			scroll.push('academic_year');
		}

		if($("#rollNo").val().trim()==""){
			flag=0;
			$("#rollNo_error").html("Roll No is not filled.").css("display","block");
			scroll.push('rollNo');
		}
	}
	else if($("#filterBy").val()=="fileUploadReference"){
		if($("#nad_certificate_id").val()==""){
			flag=0;
			$("#nad_certificate_id_error").html("NAD Certificate ID is not filled.").css("display","block");
			scroll.push('nad_certificate_id');
		}
		else if(!regx.test($("#nad_certificate_id").val())){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
			scroll.push('nad_certificate_id');
		}
		/*else if($("#nad_certificate_id").val().length!="12"){
			flag=0;
			$("#nad_certificate_id_error").html("Please enter valid NAD Certificate ID.").show();
			scroll.push('nad_certificate_id');
		}
		else{
			var str = $("#nad_certificate_id").val().replace(/\s/g, '') ;
			var fisrtChar = str.charAt(0);
			if(fisrtChar=="c" || fisrtChar=="n" || fisrtChar=="N" || fisrtChar=="C"){
				var subStr = str.substring(1);
				if(!$.isNumeric(subStr)){
					flag=0;
					$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
					scroll.push('nad_certificate_id');
				}
			}
			else{
				flag=0;
				$("#nad_certificate_id_error").html("Please enter valid NAD ID.").show();
				scroll.push('nad_certificate_id');
			}
		}*/
	}
	scroll_error(scroll);
	if(flag==1){
		$("#viewOnlineVerirficationData").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewOnlineVerirficationData").offset().top-$("nav").height()
		}, 1200);
	}
}

/* View UPLOAD STATUS */

function previous_transaction()
{
	$("#previous_transaction").addClass("showHideMasterData");
	$(".field-error").html("");
	var scroll = [];
	var flag=1;
	var d=new Date();
	var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
	var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();

	if($("#transaction_type").val()==""){
		$("#transaction_type_error").html("Any of the specified value need to be selected.").fadeIn().css("display","block");
		scroll.push("transaction_type");
		flag=0;
	}

	if($("#to_date").val()!="" && $("#from_date").val()!="")
	{
		/*if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
			$("#to_date_error").html("To Date should not be greater than current date.").fadeIn().css("display","block");
			scroll.push("to_date");
			flag=0;
		}*/
		if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
			$("#from_date_error").html("From date should not be greater than to date.").fadeIn().css("display","block");
			flag=0;
		}
		/*else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
			$("#from_date_error").html("From date should not be more than 90 day old.").fadeIn().css("display","block");
			scroll.push("from_date");
			flag=0;
		}
		else if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
			$("#from_date_error").html("From Date should not be greater than current date.").fadeIn().css("display","block");
			scroll.push("from_date");
			flag=0;
		}*/

	}
	else{
		if($("#from_date").val()==""){
			$("#from_date_error").html("Please select From Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("from_date");
		}
		if($("#to_date").val()==""){
			$("#to_date_error").html("Please select To Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("to_date");
		}
	}
	scroll_error(scroll);
	if(flag==1)
	{
		$("#previous_transaction").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#previous_transaction").offset().top-$("nav").height()
		}, 1200);
	}
}

function download_invoice()
{
	$("#viewdownloadinvoice").addClass("showHideMasterData");
	$(".field-error").html("");
	var scroll = [];
	var flag=1;
	var d=new Date();
	var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
	var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();

	if($("#invoice_for").val()==""){
		$("#invoice_for_error").html("Please select Invoice For.").fadeIn().css("display","block");
		scroll.push("invoice_for");
		flag=0;
	}

	if($("#clinet_name").val()==""){
		$("#clinet_name_error").html("Please select Invoice For.").fadeIn().css("display","block");
		scroll.push("clinet_name");
		flag=0;
	}

	if($("#to_date").val()!="" && $("#from_date").val()!="")
	{
		if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
			$("#to_date_error").html("To Date should not be greater than current date.").fadeIn().css("display","block");
			scroll.push("to_date");
			flag=0;
		}
		if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
			$("#from_date_error").html("From date should not be greater than to date.").fadeIn().css("display","block");
			flag=0;
			}else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
			$("#from_date_error").html("From date should not be more than 90 day old.").fadeIn().css("display","block");
			scroll.push("from_date");
			flag=0;
		}
		else if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
			$("#from_date_error").html("From Date should not be greater than current date.").fadeIn().css("display","block");
			scroll.push("from_date");
			flag=0;
		}

	}
	else{
		if($("#from_date").val()==""){
			$("#from_date_error").html("Please select From Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("from_date");
		}
		if($("#to_date").val()==""){
			$("#to_date_error").html("Please select To Date.").fadeIn().css("display","block");
			flag=0;
			scroll.push("to_date");
		}
	}
	scroll_error(scroll);
	if(flag==1)
	{
		$("#viewdownloadinvoice").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewdownloadinvoice").offset().top-$("nav").height()
		}, 1200);
	}
}

function searchUploadStatusData()
{
	$(".search-text-toggle").fadeOut();
	var scroll = [];
	$("#viewuploadstatus-data-form").addClass("showHideMasterData");
	$(".field-error").html("");
	var regx = /^[A-Za-z0-9]+$/;
	var flag=1;
	if($("#filterBy").val()=="awardType")
	{
		var d=new Date();
		var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();

		if($("#to_date").val()!="" && $("#from_date").val()!="")
		{
			if (processDateFormat($("#to_date").val()) > processDateFormat(Today)) {
				$("#to_date_error").html("To Date should not be greater than current date").fadeIn().css("display","block");
				scroll.push("to_date");
				flag=0;
			}
			if (processDateFormat($("#from_date").val()) > processDateFormat($("#to_date").val())) {
				$("#from_date_error").html("From date should not be greater than to date").fadeIn().css("display","block");
				flag=0;
				}else if(processDateFormat($("#from_date").val())<processDateFormat(Then)){
				$("#from_date_error").html("From date should not be more than 90 day old").fadeIn().css("display","block");
				scroll.push("from_date");
				flag=0;
			}
			else if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
				$("#from_date_error").html("From Date should not be greater than current date").fadeIn().css("display","block");
				scroll.push("from_date");
				flag=0;
			}

		}
		else{
			if($("#from_date").val()==""){
				$("#from_date_error").html("Please select From Date.").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
			if($("#to_date").val()==""){
				$("#to_date_error").html("Please select To Date.").fadeIn().css("display","block");
				flag=0;
				scroll.push("to_date");
			}
		}
	}
	else if($("#filterBy").val()=="fileUploadReference")
	{
		if($("#reference_id").val()==""){
			$("#reference_id_error").html("File Upload Reference No information is not filled.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("reference_id");
		}
		else($("#reference_id").val()!="")
		{
			if(!regx.test($("#reference_id").val())){
				$("#reference_id_error").html("File Upload Reference No should be alphanumeric.").fadeIn().css("display","block");
				flag = 0;
				scroll.push("reference_id");
			}
		}
	}
	else if($("#filterBy").val()=="uploadedBy")
	{
		if($("#uploadedBy").val()==""){
			$("#uploadedBy_error").html("Uploaded By information is not filled.").fadeIn().css("display","block");
			flag = 0;
			scroll.push("uploadedBy");
		}
	}
	scroll_error(scroll);
	/*24/12/2016*/
	if(flag==1)
	{
		//$(".search-text-toggle").fadeIn();
		$("#viewuploadstatus-data-form").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#viewuploadstatus-data-form").offset().top-$("nav").height()
		}, 1200);
	}


}
/*31/01/20 mandar************/
$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

	$('a[data-modal-id]').click(function(e) {
		e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
		var modalBox = $(this).attr('data-modal-id');
		$('#'+modalBox).fadeIn($(this).data());
	});


$(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
    });


});

$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();

});
$(".masterDataUpdateLink-main").on("click",function(e){
		$('#yesUpdate, #noUpdate').modal('hide');
	});
	$(".masterDataUpdateLink-yes").on("click",function(e){
		flag_pop = 1;
		$('#myModal, #noUpdate').modal('hide');
		$(this).hasClass("active");
	});
	$(".masterDataUpdateLink-no").on("click",function(e){
		flag_pop = 1;
		$('#myModal, #yesUpdate').modal('hide');
		$(this).hasClass("active");
		$("body").css("overflow-y","scroll");
	});

/***********mandar js***************/
          $(document).ready(function() {
				$('#example-getting-started').multiselect;
			});

			$(document).on('change', '#upload_type', function() {
				$(".hideSubDiv").show();
				$("#from_date").removeAttr('readonly');
				$("#count_file").removeAttr('readonly');
				$("#from_date").removeClass('non-editable');
				$("#count_file").removeClass('non-editable');
				if($("#upload_type").val()=="Pending for Upload"){
					$(".hideSubDiv").hide();
					$("#from_date").val("");
					$("#count_file").val("");
					$("#from_date").addClass("non-editable");
					$("#count_file").addClass("non-editable");
					$('#from_date').attr('readonly', true);
					$('#count_file').attr('readonly', true);
				}
			});

			$(document).on('change', '.dropsub', function() {
			  var target = $(this).data('target');
			  var show = $("option:selected", this).data('show');
			  $(target).children().addClass('hide');
			  $(show).removeClass('hide');
			});
			$(document).ready(function(){
				$('.dropsub').trigger('change');
			});


function uploadCertificate()
{
	$(".certificate_upload_data").hide();
	$("#certificate_upload_data").addClass("showHideMasterData");
	$(".field-error").html("");
	var flag=true;
	var scroll = [];
	$("#from_date_error").html('');
	/*if($("#filterBy").val()=="awardType" && $("#state").val()=="")
	{
		if($("#from_date").val()!="")
		{
			today = moment().format('MM/DD/YYYY')
			from_date = $("#from_date").val();

			var date1 = new Date(today);
			var date2 = new Date(from_date);
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
			if(diffDays>30){
				flag=false;
				$("#from_date_error").html("Date can not be more than 30 days older than current date");
				scroll.push("from_date");
			}
		}
	}*/
	if($("#type_of_upload").val()=="XML Data with AI Certificate Images" && $("#upload_type").val()=="Already Uploaded"){
		var d=new Date();
		var Today = (d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
		var Then= (d.getMonth()-2) + "/" + d.getDate() + "/" + d.getFullYear();
		if($("#from_date").val()==""){
			$("#uploaded_date_error").html("Date of Upload of Image Files information is not filled.").show();
			scroll.push("from_date");
			flag=false;
		}
		else if($("#from_date").val()!="")
		{
			today = moment().format('MM/DD/YYYY')
			from_date = $("#from_date").val();

			var date1 = new Date(today);
			var date2 = new Date(from_date);
			var timeDiff = Math.abs(date2.getTime() - date1.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
			if (processDateFormat($("#from_date").val()) > processDateFormat(Today)) {
				$("#uploaded_date_error").html("Date should not be greater than current date").fadeIn().css("display","block");
				flag=0;
				scroll.push("from_date");
			}
			else if(diffDays>30){
				flag=false;
				$("#uploaded_date_error").html("Date can not be more than 30 days older than current date");
				scroll.push("from_date");
			}
		}
		if($("#count_file").val()==""){
			$("#count_file_error").html("Count of Image Files Uploaded information is not filled.").show();
			scroll.push("count_file");
			flag=false;
		}
	}
	if($("#xml_file").val()=="")
	{
		$("#xml_file_error").html("Please upload .xml file").show();
		scroll.push("xmlFileDiv");
		flag=false;
	}
	scroll_error(scroll);
	if(flag==true)
	{
		$(".certificate_upload_data").show();
		$("#certificate_upload_data").removeClass("showHideMasterData");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $(".certificate_upload_data").offset().top-$("nav").height()
		}, 1200);
	}



}


	$(window).resize(function() {
    $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
});

$(window).resize();



$(".masterDataUpdateLink-reject").on("click",function(e){
		$('#reson, #noresone').modal('hide');
	});
	$(".masterDataUpdateLink-reson").on("click",function(e){
		flag_pop = 1;
		$('#reson, #noresone').modal('hide');
		$(this).hasClass("active");
	});
	$(".masterDataUpdateLink-noreject").on("click",function(e){
		flag_pop = 1;
		$('#reson').modal('hide');
		$(this).hasClass("active");
		$("body").css("overflow-y","scroll");
	});

function master_data3()
{
	$("#download-btn").removeClass('disabled-btn');
	$("#error-msg").hide();
	$("#succes-msg").show();
  /*if($("#typeOfMaster").val()=="Student Enrolment"){
      $("#error-msg").hide();
      $("#download-btn").removeClass('disabled-btn');
  }
  else{
      $("#error-msg").show();
      $("#download-btn").addClass('disabled-btn');
  }*/
}

function submit_userCreation()
{
	$(".field-error").hide();
	var flag = true;
	var regx = /^[A-Za-z]+$/;
	var mobile_regx=/[0-9]/;
	var scroll = [];
	if($("#affiliated_institute").val()=="Yes" && $("#college_name").val()==""){
		$("#college_name_error").show().css("display","block").html("Please select Name of the college.");
		scroll.push("college_name");
		flag = false;
	}
	if($("#nad_certificate_id").val()=="")
	{
		$("#verifier_name_error").show().css("display","block").html("Verifier Name information is not filled.");
		scroll.push("nad_certificate_id");
		flag = false;
	}
	if($("#emp_name").val()==""){
		$("#emp_name_error").show().css("display","block").html("Name of the Employee information is not filled.");
		scroll.push("emp_name");
		flag = false;
	}
	if($("#designation").val()==""){
		$("#designation_error").show().css("display","block").html("Please enter the Designation.");
		scroll.push("designation");
		flag = false;
	}
	if($("#emp_id").val()==""){
		$("#emp_id_error").show().css("display","block").html("Please enter the Employee ID.");
		scroll.push("emp_id");
		flag = false;
	}
	if($("#from_date").val()==""){
		$("#from_date_error").show().css("display","block").html("Date OF Birth information is not filled.");
		scroll.push("from_date");
		flag = false;
	}
	else{
		dob = new Date($("#from_date").val());
		var today = new Date();
	 	var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
	 	if(age<18){
	 		$("#from_date_error").show().css("display","block").html("Age can not be less than 18 years.");
	 		scroll.push("from_date");
	 		flag = false;
		}
	 }
	if($("#mobile").val()==""){
		$("#mobile_error").show().css("display","block").html("Please enter the Mobile No.");
		scroll.push("mobile");
		flag = false;
	}else if($("#mobile").val().length<10){
		$("#mobile_error").show().css("display","block").html("Invalid Mobile Number.");
		scroll.push("mobile");
		flag = false;
	}
	if($("#user_email_id").val()==""){
		$("#user_email_id_error").show().css("display","block").html("Please enter the Email ID.");
		scroll.push("user_email_id");
		flag = false;
	}else if(!checkEmail($("#user_email_id").val())){
		$("#user_email_id_error").show().css("display","block").html("Invalid Email ID.");
		scroll.push("user_email_id");
		flag = false;
	}
	if($("#new_user_id").val()=="")
	{
		$("#user_id_error").show().css("display","block").html("User ID information is not filled.");
		scroll.push("new_user_id");
		flag = false;
	}
	else if($("#new_user_id").val().length>15){
		$("#user_id_error").show().css("display","block").html("Invalid User ID.");
		scroll.push("new_user_id");
		flag = false;
	}
	if($("#type_of_user").val()=="functional" && $("#type_of_access").val()==""){
		$("#type_of_access_error").show().css("display","block").html("Please select Role.");
		scroll.push("type_of_access");
		flag = false;
	}
	if($("#dsc_no").val()!="" || $("#to_date").val()!="" || $("#authority").val()!=""){
		if($("#dsc_no").val()==""){
			$("#dsc_no_error").show().css("display","block").html("Please enter DSC Serial Number.");
			scroll.push("dsc_no");
			flag = false;
		}
		if($("#to_date").val()==""){
			$("#to_date_error").show().css("display","block").html("Please enter DSC Expiry Date.");
			scroll.push("to_date");
			flag = false;
		}
		if($("#authority").val()==""){
			$("#authority_error").show().css("display","block").html("Please select valid Certifying Authority.");
			scroll.push("authority");
			flag = false;
		}
	}
	scroll_error(scroll);
	var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
	if(flag==true){
		$("#submit_show_btn").addClass("hide_verifier_btn");
		$("#submit_confirm_btn").removeClass("hide_verifier_btn");
		$(".confirm").attr("readonly",true);
		$("#from_date").attr('disabled',true);
		$('#approveCertificateForm select').attr('disabled',true);
		$('.selectpicker').selectpicker('refresh');
	}
}

function edit_userCreation(){
	$("#submit_show_btn").removeClass("hide_verifier_btn");
	$("#submit_confirm_btn").addClass("hide_verifier_btn");
	$(".confirm").removeAttr("readonly");
	$("#from_date").removeAttr('disabled');
	$('#approveCertificateForm select').removeAttr('disabled');
	$('.selectpicker').selectpicker('refresh');
}

$("#type_of_user").change(function(){
	if($("#type_of_user").val()=="Functional")
		$("#role_div").show();
	else
		$("#role_div").hide();
	var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
});

$("#type_of_user").change(function(){
	if($("#type_of_user").val()=="Functional")
		$("#digital").show();
	else
		$("#digital").hide();
	var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
});

function inbox_summary(){
	$(".succes_msgs").show();
	$(".remove_data").hide();
}

function checkEmail(email){
	var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return valid = emailReg.test(email);
}

$(window).load(function(){

});


	$(document).ready(function(e){
							function animateElements() {
								$('.progressbar').each(function () {
									var elementPos = $(this).offset().top;
									var topOfWindow = $(window).scrollTop();
									var percent = $(this).find('.circle').attr('data-percent');
									var percentage = parseInt(percent, 10) / parseInt(100, 10);
									var animate = $(this).data('animate');
									if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
										$(this).data('animate', true);
										$(this).find('.circle').circleProgress({
											startAngle: -Math.PI / 2,
											value: percentage,
											thickness: 35,
											fill: {
												color: '#f07f01'
											},
											emptyFill: 'rgba(231, 231, 231, 1)'

											}).on('circle-animation-progress', function (event, progress, stepValue) {
											$(this).find('div').text(String(stepValue.toFixed(2)).substr(2) + '%');
										});

									}

								});
							}

							$(window).on("scroll",function(e){
								animateElements();
							});

						var navListItems = $('ul.setup-panel li a'),
					        allWells = $('.setup-content');

					    allWells.hide();

					    navListItems.click(function(e)
					    {
					        e.preventDefault();
					        var $target = $($(this).attr('href')),
					            $item = $(this).closest('li');


					        if (!$item.hasClass('enables')) {
					            navListItems.closest('li').removeClass('active');
					            $item.addClass('active');
					            allWells.hide();
					            $target.show();
					        }
					    });

					    $('ul.setup-panel li.active a').trigger('click');


					    var  current_active = $("ul.setup-panel li.active");

					    $('#activate-step').on('click', function(e) {
					        current_active = $("ul.setup-panel li.active");
					        current_active_id=current_active.data("id");

					        $('ul.setup-panel li:eq('+current_active_id+')').removeClass('disabled');
					        $('ul.setup-panel li a[href="#step-'+(current_active_id+1)+'"]').trigger('click');

					    })
					        $('#back-step').on('click', function(e) {
					            current_active = $("ul.setup-panel li.active");
					            current_active_id=current_active.data("id");

					        $('ul.setup-panel li a[href="#step-'+(current_active_id-1)+'"]').trigger('click');

					    })


						$("html, body").animate({ scrollTop: 0 }, "300");
					});

	setTimeout( function () {
		$.each( $('.verifier-document-select').find('.filter-option'), function () {
			var a = $(this).text();
			$(this).text(' ');
			$(this).html(a +' <span class="mandatory">*</span>' );
		})

		$.each( $('.state-block').find('.filter-option'), function () {
			var a = $(this).text();
			$(this).text(' ');
			$(this).html(a +' <span class="mandatory">*</span>' );
		})

	}, 1000)


$(".view-details").click(function(){
	var scroll = [];
	$(".field-error").html("");
	var flag=1;
	var atLeastOneIsChecked = false;
	$('input:radio').each(function () {
		if ($(this).is(':checked')) {
			atLeastOneIsChecked = true;
			return false;
		}
	});
	if(atLeastOneIsChecked==false)
	{
		var flag=0;
		$("#masterFile_error").html("Please select a file").fadeIn().css("display","block");
		scroll.push("certificate_checkbox_table");
	}
	scroll_error(scroll);

	if(flag==1)
	{
		$("#fileNo").html($("#file_no").val());
		$(this).hide();
		// $(".table-1").hide();
		// $(".table-2").hide();
		$(".table-3").show();
		$("html, body").animate({ scrollTop: $("#certificate_checkbox_table").offset().top }, "2000");
		var dashboardContentHeight = $(".dashboard-content").height();
		$(".dashboard-side-menu-wrap").css("height",dashboardContentHeight);
		$(".dashboard_side_menu").css("height",dashboardContentHeight + 200);
		$('html, body').animate({
			scrollTop: $("#certificate_checkbox_table").offset().top
		}, 1200);
	}
});

( function () {
    console.log('function');




 	// $.each( $('.a14 input, .a13 input, .v5_0 input') , function() {
 	// 	var value = $(this).attr('value');
  //   $(this).attr('placeholder', value);
 	// });

 	if ( $(window).width() < 767 ) {
 		$('.inner-point').removeClass('no-timeline-circle no-timeline');
 		$('.desk-last-inner').removeClass('lastinnerpointer');
 		$('.desk-remove-inner').removeClass('inner-point');
 		$('.resp-last-inner').addClass('lastinnerpointer');

 		$('.a15 .right-side > .form-group').addClass('inner-point');
 		$('.resp-big-info').addClass('big-info-cat')
 	}

 	$('.ring').on('click', function() {
 		$(this).find('.inner').toggleClass('border-white');
 	});

 	$("input.otp-field").mobilePassword();

 	$('.v5_0 .edit-icon').on('click', function() {
    console.log("edit-icon click");
 	});

})();

$("#mob_edit_icon").click(function(){
	$("#profile_mob").attr('readonly',false);
	$("#profile_mob").removeAttr("placeholder");
	$("#profile_mob").val("");
});

$("#email_edit_icon").click(function(){
	$('#profile_email').attr('readonly', false);
	$("#profile_email").removeAttr("placeholder");
	$("#profile_email").val("");
});

$("#sample_template").on('change', function(e) {
   console.log("hi");
   console.log($('#sample_template').val()); // is empty
});

$(".update-aadhaar-number").click(function(e){
		e.preventDefault();
		$(".adhar-number-validation").hide();
		$(".field-error").hide();
		var mobile_regx=/[0-9]/;
		var regx = /^[A-Za-z0-9]+$/;
		var sum = 0;
		$( ".inputs" ).each(function( index ) {
			sum = sum + parseInt($( this ).val().length);
		});
		if(($("#adharNumber1").val()!="" && !mobile_regx.test($("#adharNumber1").val())) || ($("#adharNumber2").val()!="" && !mobile_regx.test($("#adharNumber2").val())) || ($("#adharNumber3").val()!="" && !mobile_regx.test($("#adharNumber3").val())))
		{
			$(".adhar-number-validation").html("Please enter valid Aadhaar Number.").fadeIn().css("display","block");
		}
		if(sum=='12' && document.getElementById('checkboxInput').checked){
			$(this).hide();
			$(".aadhaar_accounts_details_otp").show();
			$("#checkboxInput").attr('disabled', true);
			$("#otp-section").fadeIn();
			$(".inputs").addClass('non-editable');
		}
		else{
			if($("#captch_code").val()=="")
				$("#captch_code_error").html("Please enter captcha code.").fadeIn().css("display","block");
			if(!document.getElementById('checkboxInput').checked)
				$("#checkboxInput_error").html("Please check checkbox for authentication.").fadeIn().css("display","block");
			if(sum=='0')
				$(".adhar-number-validation:eq(0)").fadeIn();
			else if(sum!='12')
				$(".adhar-number-validation:eq(1)").fadeIn();
		}
		return false;
	});

$("#resend").click(function(){
	$(".resend-msg").show();
})

function nadCertificateIDValidation(event) {
	var str = $('#nad_certificate_id').val();
	var keyCode = event.keyCode || event.which
	//console.log(keyCode)
	if(str.length == 0 && keyCode>=48 && keyCode<=57 ){
		return false;
	}
	if(keyCode == 46 || keyCode == 39){
		return false;
	}
	if (keyCode!="78" && keyCode!="67" && event.shiftKey)
	return false;
	// Don't validate the input if below arrow, delete and backspace keys were pressed
	if (keyCode == 8 || keyCode==229 || keyCode==46 || (keyCode >= 35 && keyCode <= 40) || keyCode==99 || keyCode==110 || keyCode==78 || keyCode==67) { // Left / Up / Right / Down Arrow, Backspace, Delete keys
		if(str.length>0 && (keyCode==99 || keyCode==110 || keyCode==78 || keyCode==67)){
			return false;
		}
		return;
	}

	var regex = new RegExp("^[NC0-9]+$");
	var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	if (!regex.test(key) || keyCode==0) {
		event.preventDefault();
		return false;
	}
}

function getAlphabates(key){
	for (var i = 97; i <= 122; i++) {
		if(key==i){
			return String.fromCharCode(i);
		}
	}
}


// font change A A+

// $(document).ready(function(){
// 	$("ul.loginmenu > li > span > a").attr( "href", "#" );
// 	$("ul.loginmenu > li > span:first-child > a").addClass("font-minus-resizer");
// 	$("ul.loginmenu > li > span:last-child > a").addClass("font-plus-resizer");
// 	$( ".font-plus-resizer" ).click(function() {
// 		var fontSize = parseInt($("ul,h1,p,a,span").css("font-size"));
// 		fontSize = fontSize * 1.1 + "px";
// 		$("ul,h1,p,a,span").css({'font-size':fontSize});
// 	});
// 	$( ".font-minus-resizer" ).click(function() {
// 		var fontSize = parseInt($("ul,h1,p,a,span").css("font-size"));
// 		fontSize = fontSize - 1 + "px";
// 		$("ul,h1,p,a,span").css({'font-size':fontSize});
// 	});

// });

// var newFontSize = 100;
// $(document).ready(function(){
	// $("ul.loginmenu > li > span > a").attr( "href", "#" );
	// $("ul.loginmenu > li > span:first-child > a").addClass("font-minus-resizer");
	// $("ul.loginmenu > li > span:last-child > a").addClass("font-plus-resizer");

	// $('.font-plus-resizer, .font-minus-resizer').on('click', function(e) {
	// 	  e.preventDefault();
	// 	});

	// Reset Font Size
	// var originalFontSize = $('ul,h1,p,a,span').css('font-size');
    //console.log(originalFontSize);

    // $(".font-minus-resizer").click(function(){
    // 	$('ul,h1,p,a,span').css('font-size', originalFontSize);
    //  });

  	// Increase Font Size
	  // $( ".font-plus-resizer" ).click(function() {
	  // 	var fontSize = parseInt($("ul,h1,p,a,span").css("font-size"));
	  // 	fontSize = fontSize * 1.1 + "px";
	  // 	$("ul,h1,p,a,span").css({'font-size':fontSize});
	  // });



	// $(".font-minus-resizer").click(function(){
		// 	newFontSize = 100;
		// 	var allElems = $('*');
		// 	for(var counter = 0; counter < allElems.length; counter++) {
		// 		$(allElems[counter]).css('font-size', '100%');
		// 	}
		// });

	 // Increase Font Size

	// $(".font-plus-resizer").click(function(){
	  //   newFontSize = newFontSize*1.2;
	  //   var allElems = $('*');
	  //   for(var counter = 0; counter < allElems.length; counter++) {
	  //       $(allElems[counter]).css('font-size', newFontSize + '%');
	  //   }
	  //   return false;
	  // });
// });


// Font Plus Minus On NAD Website
$(window).on('load', function(event) {

	$("ul.loginmenu > li > span:first-child > a").addClass("font-minus-resizer");
	$("ul.loginmenu > li > span:last-child > a").addClass("font-plus-resizer");

	if ( $(window).width() < 767 ) {
		$("ul.loginmenu > li > span:first-child").addClass("font-minus-resizer");
		$("ul.loginmenu > li > span:last-child").addClass("font-plus-resizer");
	}

	var noOfClicks = 0;

	$('ul.loginmenu .font-plus-resizer').on('click', function(event) {
		event.preventDefault();
		noOfClicks++;

		if ( noOfClicks <= 5 ) {
			$.each( $('*') , function(index, val) {
				var fontSize = $(this).attr('new-font-size');
				var newFont = parseInt(fontSize);

				$(this).css('font-size', newFont + 1 + "px");
				$(this).attr('new-font-size', newFont + 1);
			});
		}

	});

	$('ul.loginmenu .font-minus-resizer').on('click', function(event) {
		event.preventDefault();
		noOfClicks = 0;
		$.each( $('*') , function(index, val) {
			var actualFont = $(this).attr('data-actual-font-size');
			$(this).css('font-size', actualFont);
			$(this).attr('new-font-size', actualFont);
		});

	});

	$.each( $('*') , function(index, val) {
		var fz = $(this).css('font-size');
		$(this).attr('data-actual-font-size', fz);
		$(this).attr('new-font-size', fz);
	});

	// $('.close-button-for-menu').on('click', function() {
	// 	$('.nad_main_menu_wrap').removeClass('openMe');
	// });


});


function verifier_my_profile_3_1() {
	var optionSelected = $('#typeOfMasterData').val();
	console.log("optionSelected", optionSelected);
	$('.profile-responsive-table').addClass('showHideMasterData');
	$('.profile-responsive-table').addClass('showHideMasterData');
	$('#'+ optionSelected).removeClass('showHideMasterData');
	var dashboardContentHeight = $(".dashboard-content").outerHeight();
	$(".dashboard_side_menu").css("height", dashboardContentHeight);
}

$('#typeOfMasterData').on('change', function() {
	var optionSelected = $(this).val();
	console.log("optionSelected", optionSelected);
	$('.sub-drop-down').addClass('hidden-filled');
	$('.'+ optionSelected).removeClass('hidden-filled');
	$('.profile-responsive-table').addClass('showHideMasterData');
});

$('#ResetFormData').on('click', function() {
    console.log("ResetFormData");
    $('.profile-responsive-table').addClass('showHideMasterData');
    $('.profile-responsive-table').addClass('showHideMasterData');
    $('#typeOfMasterData').val("Course");
    $('#typeOfMasterData').trigger('change');
    var dashboardContentHeight = $(".dashboard-content").outerHeight();
    $(".dashboard_side_menu").css("height", 800);
});